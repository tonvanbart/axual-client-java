# Axual Discovery Client

Contains logic to interact with the `DiscoveryAPI`.
Clients that register themselves by to the `DiscoveryAPI`, receive a callback call whenever there
is a change on the cluster availability.

## DiscoveryAPI
In order for the client to achieve multi cluster switching it is required to utilize a service 
discovery API which points clients to an available cluster.

DiscoveryAPI is responsible for scanning for cluster availability.
The implementation for cluster availability is not in scope for this document.

The client uses the 
[`DiscoveryFetcher`](src/main/java/io/axual/discovery/client/fetcher/DiscoveryFetcher.java) 
in order to hit the DiscoveryAPI url with a `GET` request to obtain as
[`DiscoveryResult`](src/main/java/io/axual/discovery/client/DiscoveryResult.java) object which 
encapsulates the information that the requesting client needs in order to connect to the available
cluster returned.

The flow follows:
```plantuml
Actor AxualClient

client -> DiscoveryAPI ++: getAvailableCluster

DiscoveryAPI -> ClusterAPI ++: isClusterAvailable
ClusterAPI --> DiscoveryAPI --: isClusterAvailableResult

DiscoveryAPI --> client --: DiscoveryResult

client -> AxualCluster ++: ConsumeRequest
AxualCluster --> client --: List<ConsumedMessage>
```

### The endpoint:

The parameters needed to make the `GET` call are the following:
 * `applicationId` applicationId of the requester. 
                    Used to route all same applications to the same cluster
 * `lastCluster` the cluster retrieved in the last call used (for stickyness)

The information obtained from the DiscoveryAPI endpoint is json formatted and is shown in the 
following example:

```json
{
  instance: "constellation",
  cluster: "sirius",
  bootstrap.servers: "sirius-worker-1.axual.io:9093",
  schema.registry.url: "https://sirius-worker-2:24000",
  distributor.timeout: "60000",
  distributor.distance: "1",
  ttl: "600000",
  enable.value.headers: "false",
  group.id.resolver: "io.axual.common.resolver.GroupPatternResolver",
  group.id.pattern: "{tenant}-{instance}-{environment}-{group}",
  topic.resolver: "io.axual.common.resolver.TopicPatternResolver",
  topic.pattern: "{tenant}-{instance}-{environment}-{topic}",
  transactional.id.resolver: "io.axual.common.resolver.TransactionalIdPatternResolver",
  transactional.id.pattern: "{tenant}-{instance}-{environment}-{transactionalId}",
  acl.principal.builder: "io.axual.security.principal.AdvancedAclPrincipalBuilder"
}
```
| Name                      |   Type  | Description                                                                                                 |
|---------------------------|:-------:|-------------------------------------------------------------------------------------------------------------|
| instance                  |  string | instance used                                                                                               |
| cluster                   |  string | name of the active Axual cluster                                                                            |
| bootstrap.servers         |  string | list of host/port pairs to use for establishing the initial connection to the active Axual cluster          |
| schema.registry.url       |  string | SchemaRegistry url in the active Axual cluster (relevant for AVRO streams)                                  |
| distributor.timeout       |   int   | timeout for distributor (relevant for multicluster setups)                                                  |
| distributor.distance      |   int   | used to calculate the distributor delay messages                                                            |
| ttl                       |   int   | timeout for the validity of the `DiscoveryResult`, once exceeded the DiscoveryAPI needs to be queried again |
| enable.value.headers      | boolean | true if the value headers of target cluster are enabled                                                     |
| group.id.resolver         |  string | `GroupPatternResolver` class, used to resolve `groupId`                                                     |
| group.id.pattern          |  string | pattern used to configure the `GroupPatternResolver` object                                                 |
| topic.resolver            |  string | `TopicPatternResolver` class, used to resolve topic name                                                    |
| topic.pattern             |  string | pattern used to configure the `TopicPatternResolver` object                                                 | 
| transaction.id.resolver   |  string | `TransactionIdPatternResolver` class, used to resolve transaction id                                        |
| transaction.id.pattern    |  string | pattern used to configure the `TransactionIdPatternResolver` object                                         |
| acl.principal.builder     |  string | one of `AdvancedAclPrincipalBuilder` or `BasicAclPrincipalBuilder` depending on the cluster settings        | 
