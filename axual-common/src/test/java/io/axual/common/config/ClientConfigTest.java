package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class ClientConfigTest {
    private static final String APPLICATION_ID = "APPID";
    private static final String APPLICATION_VERSION = "APPVERSION";
    private static final String ENDPOINT = "ENDPOINT";
    private static final String ENVIRONMENT = "ENVIRONMENT";
    private static final String TENANT = "TENANT_VALID";

    private static final PasswordConfig SSL_KEY_PASSWORD = new PasswordConfig("SSLKEYPASSWORD");
    private static final String SSL_KEYSTORE_LOCATION = "SSLKEYSTORELOCATION";
    private static final PasswordConfig SSL_KEYSTORE_PASSWORD = new PasswordConfig("SSLKEYSTOREPASSWORD");
    private static final String SSL_TRUSTSTORE_LOCATION = "SSLTRUSTSTORELOCATION";
    private static final PasswordConfig SSL_TRUSTSTORE_PASSWORD = new PasswordConfig("SSLTRUSTSTOREPASSWORD");

    private ClientConfig obj1, obj2;

    @Before
    public void setUp() {
        obj1 = ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(ENDPOINT)
                .setEnvironment(ENVIRONMENT)
                .setTenant(TENANT)
                .setSslConfig(SslConfig.newBuilder()
                        .setKeyPassword(SSL_KEY_PASSWORD)
                        .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                        .setKeystorePassword(SSL_KEYSTORE_PASSWORD)
                        .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                        .setTruststorePassword(SSL_TRUSTSTORE_PASSWORD)
                        .build())
                .build();

        obj2 = ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(ENDPOINT)
                .setEnvironment(ENVIRONMENT)
                .setTenant(TENANT)
                .setSslConfig(SslConfig.newBuilder()
                        .setKeyPassword(SSL_KEY_PASSWORD)
                        .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                        .setKeystorePassword(SSL_KEYSTORE_PASSWORD)
                        .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                        .setTruststorePassword(SSL_TRUSTSTORE_PASSWORD + "a") // different
                        .build())
                .build();
    }

    @Test
    public void construction_complete() {
        ClientConfig config = ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(ENDPOINT)
                .setEnvironment(ENVIRONMENT)
                .setTenant(TENANT)
                .setSslConfig(SslConfig.newBuilder()
                        .setKeyPassword(SSL_KEY_PASSWORD)
                        .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                        .setKeystorePassword(SSL_KEYSTORE_PASSWORD)
                        .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                        .setTruststorePassword(SSL_TRUSTSTORE_PASSWORD)
                        .build())
                .build();

        assertEquals(APPLICATION_ID, config.getApplicationId());
        assertEquals(APPLICATION_VERSION, config.getApplicationVersion());
        assertEquals(ENDPOINT, config.getEndpoint());
        assertEquals(ENVIRONMENT, config.getEnvironment());
        assertEquals(TENANT, config.getTenant());
        assertEquals(SSL_KEY_PASSWORD, config.getSslConfig().getKeyPassword());
        assertEquals(SSL_KEYSTORE_LOCATION, config.getSslConfig().getKeystoreLocation());
        assertEquals(SSL_KEYSTORE_PASSWORD, config.getSslConfig().getKeystorePassword());
        assertEquals(SSL_TRUSTSTORE_LOCATION, config.getSslConfig().getTruststoreLocation());
        assertEquals(SSL_TRUSTSTORE_PASSWORD, config.getSslConfig().getTruststorePassword());
    }

    @Test
    public void test_Equality() {
        assertEquals("Objects should be equal", obj1, obj1);       // same object
        assertNotEquals("Objects should not be equal", obj1, null);    // null check
        assertNotEquals("Objects should not be equal", obj1, obj2);
    }

    @Test
    public void test_Hashcode() {
        assertNotEquals("Hashcode should not be equal", obj1.hashCode(), obj2.hashCode());
    }

    @Test
    public void test_toString() {
        assertNotNull("String should not be null", obj1.toString());
    }
}
