package io.axual.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.KafkaException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class NoExistingSchemaExceptionTest {
    private static final String DUMMY_SR_URLS = "http://dummy.url";
    private static final String STREAM_VALUE = "MOCK_STREAM";

    @Test(expected = NoExistingSchemaException.class)
    public void handlePossibleException_unknownMaster_ExectionThrown() {
        RestClientException clientException = new RestClientException(null, 0, NoExistingSchemaException.UNKNOWN_MASTER_ERROR_CODE);
        Exception mainException = new Exception(clientException);
        NoExistingSchemaException.handleException(mainException, STREAM_VALUE, DUMMY_SR_URLS);
    }

    @Test(expected = NoExistingSchemaException.class)
    public void handlePossibleException_requestForwardingFailed_ExectionThrown() {
        RestClientException clientException = new RestClientException(null, 0, NoExistingSchemaException.REQUEST_FORWARDING_FAILED_ERROR_CODE);
        Exception mainException = new Exception(clientException);
        NoExistingSchemaException.handleException(mainException, STREAM_VALUE, DUMMY_SR_URLS);
    }

    @Test
    public void handlePossibleException_otherErrorCode_noExceptionThrown() {
        RestClientException clientException = new RestClientException(null, 0, 404);
        Exception mainException = new Exception(clientException);
        NoExistingSchemaException.handleException(mainException, STREAM_VALUE, DUMMY_SR_URLS);
        //reached the end means success, as no exception is thrown

        // Dummy assert to satisfy Sonar
        assertNotNull(mainException);
    }

    @Test
    public void handlePossibleException_multipleUrls_oneUrlInMessage() {
        RestClientException clientException = new RestClientException(null, 0, NoExistingSchemaException.UNKNOWN_MASTER_ERROR_CODE);
        Exception mainException = new Exception(clientException);
        NoExistingSchemaException noExistingSchemaException = null;
        try {
            NoExistingSchemaException.handleException(mainException, STREAM_VALUE, DUMMY_SR_URLS + "," + DUMMY_SR_URLS);
        } catch (NoExistingSchemaException e) {
            noExistingSchemaException = e;
        }
        assertNotNull(noExistingSchemaException);
        String message = noExistingSchemaException.getMessage();
        assertTrue(message.indexOf(STREAM_VALUE) > 0);
        assertTrue(message.indexOf(DUMMY_SR_URLS) > 0);
        //url should only be present at least twice
        Assert.assertNotEquals(message.indexOf(DUMMY_SR_URLS), message.lastIndexOf(DUMMY_SR_URLS));
    }

    @Test
    public void handlePossibleException_noSchemaUrl_ExceptionThrown() {
        RestClientException clientException = new RestClientException(null, 0, NoExistingSchemaException.UNKNOWN_MASTER_ERROR_CODE);
        Exception mainException = new Exception(clientException);
        NoExistingSchemaException noExistingSchemaException = null;
        try {
            NoExistingSchemaException.handleException(mainException, STREAM_VALUE, "");
        } catch (NoExistingSchemaException e) {
            noExistingSchemaException = e;
        }
        assertNotNull(noExistingSchemaException);
        String message = noExistingSchemaException.getMessage();
        assertTrue(message.contains(STREAM_VALUE));
        assertFalse(message.contains("null"));
    }

    @Test
    public void handlePossibleException_noRestClientException_noExceptionThrown() {
        Exception kafkaException = new KafkaException("some exception");
        Exception mainException = new Exception(kafkaException);
        NoExistingSchemaException noExistingSchemaException = null;
        try {
            NoExistingSchemaException.handleException(mainException, STREAM_VALUE, DUMMY_SR_URLS);
        } catch (NoExistingSchemaException e) {
            noExistingSchemaException = e;
        }
        assertNull(noExistingSchemaException);
    }

}
