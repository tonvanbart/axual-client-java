package io.axual.client.janitor;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TemporaryFile extends Janitor.ManagedCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(TemporaryFile.class);
    private final String filename;

    public TemporaryFile(String filename) {
        this.filename = filename;
    }

    @Override
    public void close() {
        LOG.debug("Cleaning up temporary file: {}", filename);
        try {
            Files.deleteIfExists(Paths.get(filename));
        } catch (IOException e) {
            LOG.warn("Could not delete temporary file: {}", filename);
        }
    }

    public String getFilename() {
        return filename;
    }
}
