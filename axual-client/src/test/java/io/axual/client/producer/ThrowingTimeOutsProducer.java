package io.axual.client.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.MockProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.errors.TimeoutException;

import java.util.concurrent.Future;

import io.confluent.kafka.serializers.KafkaAvroSerializer;

/**
 * Mock class to generate errors on produce
 */
public class ThrowingTimeOutsProducer extends MockProducer<Object, Object> {
    final private int maxErrors;
    private int errorCounter = 0;

    public ThrowingTimeOutsProducer() {
        this(Integer.MAX_VALUE);
    }

    public ThrowingTimeOutsProducer(int maxErrors) {
        super(false, new KafkaAvroSerializer(), new KafkaAvroSerializer());
        this.maxErrors = maxErrors;
    }

    /**
     * Adds the record to the list of sent records.
     *
     * @see #history()
     */
    @Override
    public synchronized Future<RecordMetadata> send(ProducerRecord<Object, Object> record, Callback callback) {
        Future<RecordMetadata> future = super.send(record, callback);
        if (maxErrors > errorCounter) {
            errorCounter = errorCounter + 1;
            super.errorNext(new TimeoutException());
        } else {
            super.completeNext();
        }
        return future;
    }

    public int getErrorCounter() {
        return errorCounter;
    }
}
