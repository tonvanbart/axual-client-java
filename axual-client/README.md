# Axual Client

This module contains the code in order to make use of the Axual Client.
It can be used by applications to provide an easy way to produce or consume events from the Axual 
Platform.

## How to use
Refer to the [Axual Client Documentation](https://docs.cloud.axual.io/client/5.4.4/client/index.html).
