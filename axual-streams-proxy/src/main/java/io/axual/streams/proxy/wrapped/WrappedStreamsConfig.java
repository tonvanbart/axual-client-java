package io.axual.streams.proxy.wrapped;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.streams.KafkaClientSupplier;
import org.apache.kafka.streams.processor.internals.DefaultKafkaClientSupplier;

import java.util.Map;

import io.axual.client.proxy.wrapped.generic.WrappedClientProxyConfig;
import io.axual.common.annotation.InterfaceStability;
import io.axual.common.exception.PropertyNotSetException;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.proxy.generic.factory.UncaughtExceptionHandlerFactory;
import io.axual.streams.proxy.generic.proxy.StreamsProxy;

@InterfaceStability.Evolving
public class WrappedStreamsConfig extends WrappedClientProxyConfig<StreamsProxy> {
    public static final String TOPOLOGY_FACTORY_CONFIG = "streamsproxy.topology.factory";
    public static final String UNCAUGHT_EXCEPTION_HANDLER_FACTORY_CONFIG = "uncaught.exception.handler.factory";
    public static final String KAFKA_CLIENT_SUPPLIER_CONFIG = "kafka.client.supplier";

    private final KafkaClientSupplier clientSupplier;
    private final TopologyFactory topologyFactory;
    private final UncaughtExceptionHandlerFactory uehf;

    public WrappedStreamsConfig(Map<String, Object> configs) {
        super(configs);
        filterDownstream(TOPOLOGY_FACTORY_CONFIG, UNCAUGHT_EXCEPTION_HANDLER_FACTORY_CONFIG, KAFKA_CLIENT_SUPPLIER_CONFIG);

        Object clientSupplierConfig = configs.get(KAFKA_CLIENT_SUPPLIER_CONFIG);
        clientSupplier = clientSupplierConfig instanceof KafkaClientSupplier ? (KafkaClientSupplier) clientSupplierConfig : new DefaultKafkaClientSupplier();

        // Parse topology factory
        Object topologyFactoryConfig = configs.get(TOPOLOGY_FACTORY_CONFIG);
        if (!(topologyFactoryConfig instanceof TopologyFactory)) {
            throw new PropertyNotSetException(TOPOLOGY_FACTORY_CONFIG);
        }
        topologyFactory = (TopologyFactory) topologyFactoryConfig;

        // Parse uncaught exception handler
        Object uehfConfig = configs.get(UNCAUGHT_EXCEPTION_HANDLER_FACTORY_CONFIG);
        uehf = uehfConfig instanceof UncaughtExceptionHandlerFactory ? (UncaughtExceptionHandlerFactory) uehfConfig : null;
    }

    public KafkaClientSupplier getClientSupplier() {
        return clientSupplier;
    }

    public TopologyFactory getTopologyFactory() {
        return topologyFactory;
    }

    public UncaughtExceptionHandlerFactory getUncaughtExceptionHandlerFactory() {
        return uehf;
    }
}
