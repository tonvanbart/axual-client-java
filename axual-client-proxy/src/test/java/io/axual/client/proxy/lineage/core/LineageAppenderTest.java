package io.axual.client.proxy.lineage.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static io.axual.client.proxy.lineage.LineageHeaders.*;
import static io.axual.serde.utils.HeaderUtils.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class LineageAppenderTest {
    private static final String SYSTEM = "system";
    private static final String TENANT = "tenant";
    private static final String INSTANCE = "instance";
    private static final String ENVIRONMENT = "environment";
    private static final String CLUSTER = "cluster";

    private static final String APPLICATION_ID = "application.id";
    private static final String APPLICATION_VERSION = "application.version";
    private static final boolean ENABLE_SYSTEM_PRODUCE = true;
    private static final boolean DISABLE_SYSTEM_PRODUCE = false;

    private static final Long PRESET_SERIALIZATION_TIME = -1L;
    private static final Long PRESET_DESERIALIZATION_TIME = -1L;
    private static final String PRESET_SYSTEM = "system.preset";
    private static final String PRESET_TENANT = "tenant.preset";
    private static final String PRESET_INSTANCE = "instance.preset";
    private static final String PRESET_ENVIRONMENT = "environment.preset";
    private static final String PRESET_CLUSTER = "cluster.preset";
    private static final String PRESET_INTERMEDIATE_ID = "intermediate.id.preset";
    private static final String PRESET_INTERMEDIATE_VERSION = "intermediate.version.preset";
    private static final String PRESET_APPLICATION_ID = "application.id.preset";
    private static final String PRESET_APPLICATION_VERSION = "application.version.preset";
    private static final int PRESET_COPY_FLAG = 0b00001000;
    private static final UUID PRESET_MESSAGE_ID = new UUID(12L, 12L);


    private static final int EXPECTED_SINGLE_HEADER_COUNT = 1;
    private static final int EXPECTED_DESERIALIZATION_HEADER_COUNT = 8;
    private static final int EXPECTED_SERIALIZATION_HEADER_COUNT_WITHOUT_COPYFLAG = 11;
    private static final int EXPECTED_SERIALIZATION_HEADER_COUNT_WITH_COPYFLAG = EXPECTED_SERIALIZATION_HEADER_COUNT_WITHOUT_COPYFLAG + 1;

    private LineageConfig config = mock(LineageConfig.class);

    @Before
    public void prepareTest() {
        when(config.getSystem()).thenReturn(SYSTEM);
        when(config.getTenant()).thenReturn(TENANT);
        when(config.getInstance()).thenReturn(INSTANCE);
        when(config.getEnvironment()).thenReturn(ENVIRONMENT);
        when(config.getCluster()).thenReturn(CLUSTER);

        when(config.getApplicationId()).thenReturn(APPLICATION_ID);
        when(config.getApplicationVersion()).thenReturn(APPLICATION_VERSION);
        when(config.isSystemProduceEnabled()).thenReturn(DISABLE_SYSTEM_PRODUCE);
    }

    @Test
    public void clearLineageHeadersWithoutOtherHeaders() {
        RecordHeaders headers = new RecordHeaders();
        headers.add(MESSAGE_ID_HEADER, new byte[0]);
        headers.add(SERIALIZATION_TIME_HEADER, new byte[0]);
        headers.add(DESERIALIZATION_TIME_HEADER, new byte[0]);
        headers.add(COPY_FLAGS_HEADER, new byte[0]);
        headers.add(PRODUCER_ID_HEADER, new byte[0]);
        headers.add(PRODUCER_VERSION_HEADER, new byte[0]);
        headers.add(INTERMEDIATE_ID_HEADER, new byte[0]);
        headers.add(INTERMEDIATE_VERSION_HEADER, new byte[0]);
        headers.add(SYSTEM_HEADER, new byte[0]);
        headers.add(INSTANCE_HEADER, new byte[0]);
        headers.add(CLUSTER_HEADER, new byte[0]);
        headers.add(TENANT_HEADER, new byte[0]);
        headers.add(ENVIRONMENT_HEADER, new byte[0]);

        Headers cleared = LineageAppender.clearLineageHeaders(headers);
        assertEquals(0, cleared.toArray().length);
    }

    @Test
    public void clearLineageHeadersWitOtherHeaders() {
        final String extraHeaderKey1 = "Key 1";
        final String extraHeaderKey2 = "Key 2";
        RecordHeaders headers = new RecordHeaders();
        headers.add(MESSAGE_ID_HEADER, new byte[0]);
        headers.add(SERIALIZATION_TIME_HEADER, new byte[0]);
        headers.add(DESERIALIZATION_TIME_HEADER, new byte[0]);
        headers.add(COPY_FLAGS_HEADER, new byte[0]);
        headers.add(PRODUCER_ID_HEADER, new byte[0]);
        headers.add(PRODUCER_VERSION_HEADER, new byte[0]);
        headers.add(INTERMEDIATE_ID_HEADER, new byte[0]);
        headers.add(INTERMEDIATE_VERSION_HEADER, new byte[0]);
        headers.add(SYSTEM_HEADER, new byte[0]);
        headers.add(INSTANCE_HEADER, new byte[0]);
        headers.add(CLUSTER_HEADER, new byte[0]);
        headers.add(TENANT_HEADER, new byte[0]);
        headers.add(ENVIRONMENT_HEADER, new byte[0]);
        headers.add(extraHeaderKey1, new byte[0]);
        headers.add(extraHeaderKey2, new byte[0]);

        Headers cleared = LineageAppender.clearLineageHeaders(headers);
        assertEquals(2, cleared.toArray().length);
        verifySingleHeader(cleared, extraHeaderKey1);
        verifySingleHeader(cleared, extraHeaderKey2);
    }

    @Test
    public void appendLineageForDeserializationNoPresetHeaders() {
        RecordHeaders headers = new RecordHeaders();

        // apppend
        LineageAppender.appendLineageForDeserialization(headers, config);

        // declare expectations
        verifyDeserializationHeaders(headers, false);
    }

    @Test
    public void appendLineageForDeserializationWithPresetHeaders() {
        RecordHeaders headers = new RecordHeaders();
        addLongHeader(headers, DESERIALIZATION_TIME_HEADER, PRESET_DESERIALIZATION_TIME);
        addStringHeader(headers, TENANT_HEADER, PRESET_TENANT);
        addStringHeader(headers, ENVIRONMENT_HEADER, PRESET_ENVIRONMENT);
        addStringHeader(headers, INSTANCE_HEADER, PRESET_INSTANCE);
        addStringHeader(headers, CLUSTER_HEADER, PRESET_CLUSTER);
        addStringHeader(headers, SYSTEM_HEADER, PRESET_SYSTEM);
        addStringHeader(headers, INTERMEDIATE_ID_HEADER, PRESET_INTERMEDIATE_ID);
        addStringHeader(headers, INTERMEDIATE_VERSION_HEADER, PRESET_INTERMEDIATE_VERSION);

        // apppend
        LineageAppender.appendLineageForDeserialization(headers, config);
        verifyDeserializationHeaders(headers, true);
    }


    private void verifyDeserializationHeaders(Headers headers, boolean preset) {
        // Check deserialization time header
        Header contentHeader = verifySingleHeader(headers, DESERIALIZATION_TIME_HEADER);
        assertNotNull(contentHeader);
        assertNotEquals(PRESET_DESERIALIZATION_TIME, decodeLongHeader(contentHeader));

        List<Header> headerCheckList = new ArrayList<>();
        headers.headers(DESERIALIZATION_TIME_HEADER).forEach(headerCheckList::add);
        assertThat(headerCheckList.size(), is(EXPECTED_SINGLE_HEADER_COUNT));

        // Check tenant header
        verifySingleStringHeader(headers, TENANT_HEADER, preset ? PRESET_TENANT : TENANT);

        // Check environment header
        verifySingleStringHeader(headers, ENVIRONMENT_HEADER, preset ? PRESET_ENVIRONMENT : ENVIRONMENT);

        // Check instance header
        verifySingleStringHeader(headers, INSTANCE_HEADER, preset ? PRESET_INSTANCE : INSTANCE);

        // Check cluster header
        verifySingleStringHeader(headers, CLUSTER_HEADER, preset ? PRESET_CLUSTER : CLUSTER);

        // Check system header
        verifySingleStringHeader(headers, SYSTEM_HEADER, preset ? PRESET_SYSTEM : SYSTEM);

        // Check intermediate application id header
        verifySingleStringHeader(headers, INTERMEDIATE_ID_HEADER, APPLICATION_ID);

        // Check intermediate application id header
        verifySingleStringHeader(headers, INTERMEDIATE_VERSION_HEADER, APPLICATION_VERSION);

        assertEquals(EXPECTED_DESERIALIZATION_HEADER_COUNT, headers.toArray().length);
    }

    private void verifySingleStringHeader(Headers headers, String key, String expectedValue) {
        Header contentHeader = verifySingleHeader(headers, key);
        assertNotNull(contentHeader);
        assertThat(decodeStringHeader(contentHeader), equalTo(expectedValue));
    }

    private Header verifySingleHeader(Headers headers, String key) {
        List<Header> headerCheckList = new ArrayList<>();
        headers.headers(key).forEach(headerCheckList::add);
        assertThat(headerCheckList.size(), is(EXPECTED_SINGLE_HEADER_COUNT));
        return headerCheckList.get(0);
    }

    @Test
    public void appendLineageForSerializationWithoutPresetsAsClientProducer() {
        when(config.isSystemProduceEnabled()).thenReturn(DISABLE_SYSTEM_PRODUCE);
        RecordHeaders headers = new RecordHeaders();
        LineageAppender.appendLineageForSerialization(headers, config);
        verifySerializationHeaders(headers, false, false);
    }

    @Test
    public void appendLineageForSerializationWithoutPresetsAsSystemProducer() {
        when(config.isSystemProduceEnabled()).thenReturn(ENABLE_SYSTEM_PRODUCE);
        RecordHeaders headers = new RecordHeaders();
        RecordHeaders headers2 = new RecordHeaders();
        LineageAppender.appendLineageForSerialization(headers, config);
        verifySerializationHeaders(headers, false, true);
    }

    @Test
    public void appendLineageForSerializationAsClientProducer() {
        // Reset mock logic
        when(config.getApplicationId()).thenReturn(APPLICATION_ID);
        when(config.getApplicationVersion()).thenReturn(APPLICATION_VERSION);
        when(config.isSystemProduceEnabled()).thenReturn(DISABLE_SYSTEM_PRODUCE);

        RecordHeaders headers = new RecordHeaders();
        setSerializationPresetHeaders(headers);

        LineageAppender.appendLineageForSerialization(headers, config);
        verifySerializationHeaders(headers, true, false);
    }

    @Test
    public void appendLineageForSerializationAsSystemProducer() {
        // Reset mock logic
        when(config.getApplicationId()).thenReturn(APPLICATION_ID);
        when(config.getApplicationVersion()).thenReturn(APPLICATION_VERSION);
        when(config.isSystemProduceEnabled()).thenReturn(ENABLE_SYSTEM_PRODUCE);

        RecordHeaders headers = new RecordHeaders();
        setSerializationPresetHeaders(headers);

        LineageAppender.appendLineageForSerialization(headers, config);
        verifySerializationHeaders(headers, true, true);
    }

    private void setSerializationPresetHeaders(Headers headers) {
        addIntegerHeader(headers, COPY_FLAGS_HEADER, PRESET_COPY_FLAG); // should be removed
        addUuidHeader(headers, MESSAGE_ID_HEADER, PRESET_MESSAGE_ID); // should be removed
        addLongHeader(headers, DESERIALIZATION_TIME_HEADER, PRESET_DESERIALIZATION_TIME); // Should be removed
        addLongHeader(headers, SERIALIZATION_TIME_HEADER, PRESET_SERIALIZATION_TIME); // Should be removed and set to current time
        addStringHeader(headers, TENANT_HEADER, PRESET_TENANT);
        addStringHeader(headers, ENVIRONMENT_HEADER, PRESET_ENVIRONMENT);
        addStringHeader(headers, INSTANCE_HEADER, PRESET_INSTANCE);
        addStringHeader(headers, CLUSTER_HEADER, PRESET_CLUSTER);
        addStringHeader(headers, SYSTEM_HEADER, PRESET_SYSTEM);
        addStringHeader(headers, INTERMEDIATE_ID_HEADER, PRESET_INTERMEDIATE_ID); // Reset with application id
        addStringHeader(headers, INTERMEDIATE_VERSION_HEADER, PRESET_INTERMEDIATE_VERSION); // Reset with application version
        addStringHeader(headers, PRODUCER_ID_HEADER, PRESET_APPLICATION_ID);
        addStringHeader(headers, PRODUCER_VERSION_HEADER, PRESET_APPLICATION_VERSION);
    }

    private void verifySerializationHeaders(Headers headers, boolean preset, boolean isSystemProducer) {
        // Check deserialization time header
        Header contentHeader = verifySingleHeader(headers, SERIALIZATION_TIME_HEADER);
        assertNotNull(contentHeader);
        assertNotEquals(PRESET_SERIALIZATION_TIME, decodeLongHeader(contentHeader));

        Header messageIdHeader = verifySingleHeader(headers, MESSAGE_ID_HEADER);
        assertNotNull(messageIdHeader);

        verifySingleStringHeader(headers, INTERMEDIATE_ID_HEADER, APPLICATION_ID);
        verifySingleStringHeader(headers, INTERMEDIATE_VERSION_HEADER, APPLICATION_VERSION);
        verifySingleStringHeader(headers, TENANT_HEADER, TENANT);
        verifySingleStringHeader(headers, ENVIRONMENT_HEADER, ENVIRONMENT);
        verifySingleStringHeader(headers, SYSTEM_HEADER, SYSTEM);
        verifySingleStringHeader(headers, INSTANCE_HEADER, INSTANCE);
        verifySingleStringHeader(headers, CLUSTER_HEADER, CLUSTER);


        if (preset) {
            verifySingleStringHeader(headers, PRODUCER_ID_HEADER, PRESET_APPLICATION_ID);
            verifySingleStringHeader(headers, PRODUCER_VERSION_HEADER, PRESET_APPLICATION_VERSION);

            Header copyFlagsHeader = headers.lastHeader(COPY_FLAGS_HEADER);

            if (isSystemProducer) {
                assertEquals(PRESET_MESSAGE_ID, decodeUuidHeader(messageIdHeader));

                assertNotNull(copyFlagsHeader);
                assertEquals(PRESET_COPY_FLAG, decodeIntegerHeader(copyFlagsHeader).intValue());
            } else {
                assertNotEquals(PRESET_MESSAGE_ID, decodeUuidHeader(messageIdHeader));
                assertNull(copyFlagsHeader);
            }

        } else {
            verifySingleStringHeader(headers, PRODUCER_ID_HEADER, APPLICATION_ID);
            verifySingleStringHeader(headers, PRODUCER_VERSION_HEADER, APPLICATION_VERSION);
        }
    }
}
