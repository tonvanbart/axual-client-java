package io.axual.client.proxy.resolving.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import io.axual.client.proxy.resolving.consumer.ResolvingConsumerConfig;
import io.axual.client.proxy.test.TestProducerProxy;
import io.axual.client.proxy.test.TestProducerProxyFactory;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.exception.ConfigurationException;
import io.axual.common.resolver.GroupPatternResolver;
import io.axual.common.resolver.GroupResolver;
import io.axual.common.resolver.TopicPatternResolver;
import io.axual.common.resolver.TopicResolver;
import io.axual.common.resolver.TransactionalIdPatternResolver;
import io.axual.common.tools.KafkaUtil;
import io.axual.discovery.client.DiscoveryClientRegistry;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ResolvingProducer.class, TestProducerProxyFactory.class, TestProducerProxy.class,
    DiscoveryClientRegistry.class})
@PowerMockIgnore({"javax.net.ssl.*","jdk.internal.reflect.*"})
public class ResolvingProducerTest {

  private static final String STREAM = "general-applicationlog";
  private static final String APPLICATION_ID = "io.axual.test";
  private static final String GROUP = APPLICATION_ID;
  private static final String APPLICATION_VERSION = "1.0";
  private static final String TENANT_PROPERTY = "tnt";
  private static final String ENVIRONMENT_PROPERTY = "envt";
  private static final String TOPIC_PATTERN =
      "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{topic}";
  private static final String GROUP_PATTERN =
      "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{group}";
  private static final String ENVIRONMENT = "unit";
  private static final String TENANT = "axual";
  public static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
  public static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
  public static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");

  private final TopicResolver topicResolver;
  private final GroupResolver groupResolver;

  @Mock
  TestProducerProxy<String, String> mockedProducer1, mockedProducer2;

  public ResolvingProducerTest() {
    Map<String, Object> properties = new HashMap<>();
    properties.put(TENANT_PROPERTY, TENANT);
    properties.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);
    properties
        .put(ResolvingProducerConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
    properties.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, TOPIC_PATTERN);
    properties
        .put(ResolvingConsumerConfig.GROUP_ID_RESOLVER_CONFIG, GroupPatternResolver.class.getName());
    properties.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, GROUP_PATTERN);

    topicResolver = new TopicPatternResolver();
    topicResolver.configure(properties);
    groupResolver = new GroupPatternResolver();
    groupResolver.configure(properties);
  }

  protected Properties getValidProperties() {
    final Properties producerProps = new Properties();
    producerProps.putAll(getValidMap());
    return producerProps;
  }

  protected Map<String, Object> getValidMap() {
    final Map<String, Object> producerMap = new HashMap<>();

    producerMap.put(ResolvingProducerConfig.BACKING_FACTORY_CONFIG,
        TestProducerProxyFactory.class.getName());
    producerMap.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
    producerMap.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);

    producerMap.put(ResolvingConsumerConfig.GROUP_ID_RESOLVER_CONFIG,
        GroupPatternResolver.class.getName());
    producerMap.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, GROUP_PATTERN);
    producerMap
        .put(ResolvingProducerConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
    producerMap.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, TOPIC_PATTERN);
    producerMap.put(TENANT_PROPERTY, TENANT);
    producerMap.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);

    producerMap.put(ProducerConfig.ACKS_CONFIG, "-1");
    producerMap.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");
    producerMap.put(ProducerConfig.RETRIES_CONFIG, "" + Integer.MAX_VALUE);
    producerMap.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    producerMap.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringDeserializer.class);

    SslConfig sslConfig = SslConfig.newBuilder()
        .setKeyPassword(SSL_PASSWORD)
        .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
        .setKeystorePassword(SSL_PASSWORD)
        .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
        .setTruststorePassword(SSL_PASSWORD)
        .setEnableHostnameVerification(false)
        .build();
    KafkaUtil.getKafkaConfigs(sslConfig, producerMap);

    return producerMap;
  }

  protected void validConstructorTest(final ResolvingProducer producer) throws Exception {
    final ResolvingProducerConfig config = (ResolvingProducerConfig) Whitebox
        .getInternalState(producer, "config");

    Assert.assertEquals(SSL_KEYSTORE_LOCATION, config.getSslConfig().getKeystoreLocation());
    Assert.assertEquals(SSL_TRUSTSTORE_LOCATION, config.getSslConfig().getTruststoreLocation());
    Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeystorePassword());
    Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getTruststorePassword());
    Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeyPassword());

    // Verify initialisation of Apache KafkaProducer with mocked properties from the mocked provider
    verifyNew(TestProducerProxy.class, Mockito.times(1));
  }

  @Before
  public void setup() throws Exception {
    whenNew(TestProducerProxy.class).withAnyArguments()
        .thenReturn(mockedProducer1, mockedProducer2);
  }

  @Test
  public void construction_ValidProperties() throws Exception {
    final ResolvingProducer kafkaProducer = new ResolvingProducer(getValidProperties());
    validConstructorTest(kafkaProducer);
  }

  @Test
  public void construction_ValidMap() throws Exception {
    final ResolvingProducer kafkaProducer = new ResolvingProducer(getValidMap());
    validConstructorTest(kafkaProducer);
  }

  @Test
  @Ignore("property now in downstream config?")
  public void transactionId_isResolved() throws Exception {
    // given a configuration with transactional.id and transactional.id.resolver configured
    final Map<String, Object> config = getValidMap();
    config.put(ResolvingProducerConfig.TRANSACTIONAL_ID_RESOLVER_CONFIG, TransactionalIdPatternResolver.class.getName());
    config.put(TransactionalIdPatternResolver.TRANSACTIONAL_ID_PATTERN_CONFIG, "{tnt}-{envt}-{transactional.id}");
    config.put("transactional.id", "trns_test");

    // when a producer is constructed
    final ResolvingProducer resolvingProducer = new ResolvingProducer(config);

    // the internal config contains a resolved transactional.id
    final ResolvingProducerConfig internalState = (ResolvingProducerConfig) Whitebox
            .getInternalState(resolvingProducer, "config");
    assertEquals("axual-unit-trns_test", config.get("transactional.id"));
  }

  @Test(expected = ConfigurationException.class)
  public void transactionIdPresent_noResolver_throwsConfigurationException() throws Exception {
    // given a configuration with transactional.id but no transactional.id.resolver configured
    final Map<String, Object> config = getValidMap();
    config.put("transactional.id", "trns_test");

    // when a producer is constructed we should get ConfigurationException
    final ResolvingProducer resolvingProducer = new ResolvingProducer(config);
  }

  @Test
  public void initTransactions() {
    final ResolvingProducer kafkaProducer = new ResolvingProducer(getValidMap());
    kafkaProducer.initTransactions();
    Mockito.verify(mockedProducer1, Mockito.times(1)).initTransactions();
  }

  @Test
  public void beginTransaction() {
    final ResolvingProducer kafkaProducer = new ResolvingProducer(getValidProperties());
    kafkaProducer.beginTransaction();
    Mockito.verify(mockedProducer1, Mockito.times(1)).beginTransaction();
  }

  @Test
  public void sendOffsetsToTransaction_GroupIdString() {
    final String expectedGroupId = groupResolver.resolveGroup(GROUP);
    final ArgumentCaptor<String> groupIdCaptor = ArgumentCaptor.forClass(String.class);

    final ResolvingProducer kafkaProducer = new ResolvingProducer(getValidProperties());
    kafkaProducer
        .sendOffsetsToTransaction(new HashMap<TopicPartition, OffsetAndMetadata>(), GROUP);
    Mockito.verify(mockedProducer1, Mockito.times(1))
        .sendOffsetsToTransaction(Matchers.anyMap(), groupIdCaptor.capture());

    assertEquals(expectedGroupId,groupIdCaptor.getValue());
  }

  @Test
  public void sendOffsetsToTransaction_ConsumerGroupMetaData() {
    final String expectedGroupId = groupResolver.resolveGroup(GROUP);
    final ArgumentCaptor<String> groupIdCaptor = ArgumentCaptor.forClass(String.class);

    final ResolvingProducer kafkaProducer = new ResolvingProducer(getValidProperties());
    kafkaProducer
        .sendOffsetsToTransaction(new HashMap<TopicPartition, OffsetAndMetadata>(), GROUP);
    Mockito.verify(mockedProducer1, Mockito.times(1))
        .sendOffsetsToTransaction(Matchers.anyMap(), groupIdCaptor.capture());

    assertEquals(expectedGroupId,groupIdCaptor.getValue());
  }

  @Test
  public void commitTransaction() {
    final ResolvingProducer kafkaProducer = new ResolvingProducer(getValidProperties());
    kafkaProducer.commitTransaction();
    Mockito.verify(mockedProducer1, Mockito.times(1)).commitTransaction();
  }

  @Test
  public void abortTransaction() {
    final ResolvingProducer kafkaProducer = new ResolvingProducer(getValidProperties());
    kafkaProducer.abortTransaction();
    Mockito.verify(mockedProducer1, Mockito.times(1)).abortTransaction();
  }

  @Test
  public void send() throws Exception {
    final String expectedTopic = topicResolver.resolveTopic(STREAM);
    final ResolvingProducer<String, String> kafkaProducer = spy(
        new ResolvingProducer<String, String>(getValidProperties()));

    final String key = "MyKey";
    final String value = "MyValue";
    final ProducerRecord<String, String> record = new ProducerRecord<>(STREAM, key, value);
    kafkaProducer.send(record);

    final ArgumentCaptor<ProducerRecord> producerRecordCaptor = ArgumentCaptor
        .forClass(ProducerRecord.class);
    Mockito.verify(mockedProducer1, Mockito.times(1)).send(producerRecordCaptor.capture());
    assertEquals(producerRecordCaptor.getValue().topic(), expectedTopic);
  }

  @Test
  public void sendWithCallback() {
    final String expectedTopic = topicResolver.resolveTopic(STREAM);

    final ResolvingProducer<String, String> kafkaProducer = spy(
        new ResolvingProducer<String, String>(getValidProperties()));

    final String key = "MyKey";
    final String value = "MyValue";
    final ProducerRecord<String, String> record = new ProducerRecord<>(STREAM, key, value);

    // Created to compare to mock input
    final Callback produceCallback = new Callback() {
      @Override
      public void onCompletion(RecordMetadata metadata, Exception exception) {
        // never called;
      }
    };

    kafkaProducer.send(record, produceCallback);

    final ArgumentCaptor<ProducerRecord> producerRecordCaptor = ArgumentCaptor
        .forClass(ProducerRecord.class);
    final ArgumentCaptor<Callback> callbackCaptor = ArgumentCaptor.forClass(Callback.class);
    Mockito.verify(mockedProducer1, Mockito.times(1))
        .send(producerRecordCaptor.capture(), callbackCaptor.capture());
    assertEquals(expectedTopic, producerRecordCaptor.getValue().topic());
  }

  @Test
  public void flush() {
    final ResolvingProducer kafkaProducer = spy(new ResolvingProducer(getValidProperties()));

    kafkaProducer.flush();

    Mockito.verify(mockedProducer1, Mockito.times(1)).flush();
  }

  @Test
  public void partitionsFor() {
    final String expectedTopic = topicResolver.resolveTopic(STREAM);
    final ResolvingProducer kafkaProducer = spy(new ResolvingProducer(getValidProperties()));

    kafkaProducer.partitionsFor(STREAM);

    final ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
    Mockito.verify(mockedProducer1, Mockito.times(1)).partitionsFor(topicCaptor.capture());

    final String capturedTopic = topicCaptor.getValue();
    assertEquals(expectedTopic, capturedTopic);
  }

  @Test
  public void metrics() {
    final ResolvingProducer kafkaProducer = spy(new ResolvingProducer(getValidProperties()));

    kafkaProducer.metrics();

    Mockito.verify(mockedProducer1, Mockito.times(1)).metrics();
  }
}
