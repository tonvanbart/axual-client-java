package io.axual.serde.valueheader;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Arrays;
import java.util.Map;

import io.axual.serde.utils.SerdeUtils;

public class ValueHeaderDeserializer implements Deserializer<ValueHeader> {

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        // nothing to configure
    }

    @Override
    public ValueHeader deserialize(String topic, byte[] data) {
        return deserialize(topic, null, data);
    }

    @Override
    public ValueHeader deserialize(String topic, Headers headers, byte[] data) {
        if (!SerdeUtils.containsValueHeader(data)) {
            throw new SerializationException("Blob does not contain a valid value header: " + Arrays.toString(data));
        }

        return new ValueHeader(
                SerdeUtils.getMessageId(data),
                SerdeUtils.getSerializationTimestamp(data),
                SerdeUtils.importCopyFlags(data));
    }

    @Override
    public void close() {
        // nothing to close
    }
}
