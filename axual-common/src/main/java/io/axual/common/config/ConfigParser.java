package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.types.Password;

import java.util.Map;

import io.axual.common.exception.PropertyNotSetException;

public class ConfigParser {
    private ConfigParser() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T parseConfig(Map input, String key, boolean mandatory, T defaultValue) {
        Object value = input.get(key);
        if (value != null) {
            return (T) value;
        }

        if (mandatory && defaultValue == null) {
            throw new PropertyNotSetException(key);
        }

        return defaultValue;
    }

    public static <T> T parseAndRemoveConfig(Map input, String key, boolean mandatory, T defaultValue) {
        T result = parseConfig(input, key, mandatory, defaultValue);
        input.remove(key);
        return result;
    }

    public static String parseStringConfig(Map input, String key, boolean mandatory, String defaultValue) {
        return parseConfig(input, key, mandatory, defaultValue);
    }

    public static String parseAndRemoveStringConfig(Map input, String key, boolean mandatory, String defaultValue) {
        String result = parseStringConfig(input, key, mandatory, defaultValue);
        input.remove(key);
        return result;
    }

    public static String parseAndFilterStringConfig(Map<String, Object> input, String key, boolean mandatory, String defaultValue, Map output) {
        try {
            return parseStringConfig(input, key, mandatory, defaultValue);
        } finally {
            output.remove(key);
        }
    }

    public static Password parsePasswordConfig(Map input, String key, boolean mandatory, String defaultValue) {
        return parsePasswordConfig(input, key, mandatory, new Password(defaultValue));
    }

    public static Password parsePasswordConfig(Map input, String key, boolean mandatory, Password defaultValue) {
        Object value = input.get(key);
        if (value instanceof String) {
            return new Password((String) value);
        } else if (value instanceof Password) {
            return (Password) value;
        } else {
            if (mandatory && (defaultValue == null || defaultValue.value() == null)) {
                throw new PropertyNotSetException(key);
            }

            return defaultValue;
        }
    }

    public static Password parseAndFilterPasswordConfig(Map<String, Object> input, String key, boolean mandatory, String defaultValue, Map output) {
        return parseAndFilterPasswordConfig(input, key, mandatory, new Password(defaultValue), output);
    }

    public static Password parseAndFilterPasswordConfig(Map<String, Object> input, String key, boolean mandatory, Password defaultValue, Map output) {
        try {
            return parsePasswordConfig(input, key, mandatory, defaultValue);
        } finally {
            output.remove(key);
        }
    }
}
