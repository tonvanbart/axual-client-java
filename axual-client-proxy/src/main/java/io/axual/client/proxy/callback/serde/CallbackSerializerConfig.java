package io.axual.client.proxy.callback.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.callback.core.CallbackConfig;
import io.axual.client.proxy.generic.serde.BaseSerializerProxyConfig;

public class CallbackSerializerConfig<T> extends BaseSerializerProxyConfig<T> {
    public static final String BACKING_SERIALIZER_CONFIG = "callbackserializer.backing.serializer";
    private final CallbackConfig config;

    public CallbackSerializerConfig(Map<String, ?> configs, boolean isKey) {
        super(new HashMap<>(configs), isKey, BACKING_SERIALIZER_CONFIG);
        config = new CallbackConfig(new HashMap<>(configs));
    }

    public CallbackConfig getCallbackConfig() {
        return config;
    }
}
