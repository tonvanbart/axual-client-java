package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Deserializer;

import io.axual.client.AxualClient;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.common.annotation.InterfaceStability;

/**
 * This is the base for all AxualClient Consumer configurations. It contains the settings all
 * consumers have in common. Extend this class when defining a new Consumer type. A Builder pattern
 * is used for initialization.
 *
 * @param <K> the type of the key that will be consumed. A compatible key deserializer must be
 *            provided to serialize the objects
 * @param <V> the type of the value that will be consumed. A compatible value deserializer must be
 *            provided to serialize the objects
 */
@InterfaceStability.Evolving
public class BaseConsumerConfig<K, V> {
    public static final ProxyChain DEFAULT_PROXY_CHAIN = ProxyChain.newBuilder()
            .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
            .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
            .append(ProxyTypeRegistry.LINEAGE_PROXY_ID)
            .append(ProxyTypeRegistry.HEADER_PROXY_ID)
            .build();

    private static final int DEFAULT_POLL_SIZE = 100;
    private DeliveryStrategy deliveryStrategy;
    private String stream;
    private Object keyDeserializer;
    private Object valueDeserializer;
    private final int maximumPollSize;
    private final ProxyChain proxyChain;

    /**
     * Instantiates a new Base consumer config.
     *
     * @param builder the builder containing the configuration values
     */
    BaseConsumerConfig(Builder<K, V, ?> builder) {
        deliveryStrategy = builder.deliveryStrategy;
        stream = builder.stream;
        keyDeserializer = builder.keyDeserializer;
        valueDeserializer = builder.valueDeserializer;
        maximumPollSize = builder.maximumPollSize;
        proxyChain = builder.proxyChain;
    }

    /**
     * Returns the name of the stream this configuration should read from.
     *
     * @return the stream namet
     */
    public String getStream() {
        return stream;
    }

    /**
     * Returns the delivery strategy the consumer should implement. see {@link DeliveryStrategy} for
     * the available strategies.
     *
     * @return the delivery strategy used.
     */
    public DeliveryStrategy getDeliveryStrategy() {
        return deliveryStrategy;
    }

    /**
     * Returns the configured key deserializer. The class must implement {@link Deserializer}
     *
     * @return the configured key deserializer
     */
    public Object getKeyDeserializer() {
        return keyDeserializer;
    }

    /**
     * Returns the configured value deserializer. The class must implement {@link Deserializer}
     *
     * @return the configured value deserializer
     */
    public Object getValueDeserializer() {
        return valueDeserializer;
    }

    /**
     * Returns the maximum amount of message retrieved in a single poll.
     *
     * @return the maximum amount
     */
    public Integer getMaximumPollSize() {
        return maximumPollSize;
    }

    /**
     * Returns the chain of producer proxies this producer uses.
     *
     * @return the proxy chain used to configure this producer.
     */
    public ProxyChain getProxyChain() {
        return proxyChain;
    }

    /**
     * The Builder is used to set the configuration options.
     *
     * @param <K> the type of the key that will be consumed. A compatible key deserializer must be
     *            provided to deserialize the objects
     * @param <V> the type of the value that will be consumed. A compatible value deserializer must
     *            be provided to serialize the objects
     * @param <T> the type of the Builder that is to be used.
     */
    protected static class Builder<K, V, T extends Builder<K, V, T>> {
        private DeliveryStrategy deliveryStrategy = DeliveryStrategy.AT_LEAST_ONCE;
        private String stream;
        private Object keyDeserializer;
        private Object valueDeserializer;
        private int maximumPollSize = DEFAULT_POLL_SIZE;
        private ProxyChain proxyChain = DEFAULT_PROXY_CHAIN;

        /**
         * Sets the strategy ot be used by the the consumer. See {@link DeliveryStrategy} for the
         * available strategies
         *
         * @param deliveryStrategy the strategy to use for the consumer
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setDeliveryStrategy(DeliveryStrategy deliveryStrategy) {
            this.deliveryStrategy = deliveryStrategy;
            return (T) this;
        }

        /**
         * Sets the name of the stream to consume from.
         *
         * @param stream The stream name
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setStream(String stream) {
            this.stream = stream;
            return (T) this;
        }

        /**
         * The full name of the class implementing {@link Deserializer} which will return the key
         * objects from deserialization
         *
         * @param keyDeserializerClassName The key deserializer class name
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setKeyDeserializer(String keyDeserializerClassName) {
            keyDeserializer = keyDeserializerClassName;
            return (T) this;
        }

        /**
         * The configured class or instance implementing {@link Deserializer} which will return the
         * key objects from deserialization
         *
         * @param keyDeserializer The configured key deserializer
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setKeyDeserializer(Deserializer<K> keyDeserializer) {
            this.keyDeserializer = keyDeserializer;
            return (T) this;
        }

        /**
         * The full name of the class implementing {@link Deserializer} which will return the value
         * objects from deserialization
         *
         * @param valueDeserializerClassName The configured value deserializer
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setValueDeserializer(String valueDeserializerClassName) {
            valueDeserializer = valueDeserializerClassName;
            return (T) this;
        }

        /**
         * The configured class or instance implementing {@link Deserializer} which will return the
         * value objects from deserialization
         *
         * @param valueDeserializer The configured value deserializer
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setValueDeserializer(Deserializer<V> valueDeserializer) {
            this.valueDeserializer = valueDeserializer;
            return (T) this;
        }

        /**
         * Sets the number of messages to be retrieved by a single poll command.
         *
         * @param maximumPollSize The number of message to be read, value must be between 1 and 500
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setMaximumPollSize(int maximumPollSize) {
            this.maximumPollSize = maximumPollSize;
            return (T) this;
        }

        /**
         * Configures the proxy chain to initialize for this consumer.
         *
         * @param proxyChain The chain of consumer proxies to set up.
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setProxyChain(ProxyChain proxyChain) {
            this.proxyChain = proxyChain;
            return (T) this;
        }

        /**
         * Validate the variables set in the config object.
         *
         * @throws IllegalStateException when one of more variables are not set (correctly)
         */
        public void validate() {
            if (deliveryStrategy == null) {
                throw new IllegalStateException("Error building configuration: DeliveryStrategy is missing");
            }
            if (stream == null || stream.length() == 0) {
                throw new IllegalStateException("Error building configuration: stream is missing");
            }
            if (keyDeserializer == null || ((keyDeserializer instanceof String) && ((String) keyDeserializer).isEmpty())) {
                throw new IllegalStateException("Error building configuration: keyDeserializer is missing");
            }
            if (valueDeserializer == null || ((valueDeserializer instanceof String) && ((String) valueDeserializer).isEmpty())) {
                throw new IllegalStateException("Error building configuration: valueDeserializer is missing");
            }
            if (maximumPollSize < 1 || maximumPollSize > 500) {
                throw new IllegalStateException("Consumer poll size must be between 0 and 500, current value is " + maximumPollSize);
            }
        }

        /**
         * Builds the {@link BaseConsumerConfig} object which is used by the {@link AxualClient} to
         * configure a consumer
         *
         * @return a {@link BaseConsumerConfig} object
         */
        public BaseConsumerConfig<K, V> build() {
            validate();
            return new BaseConsumerConfig<>(this);
        }
    }
}
