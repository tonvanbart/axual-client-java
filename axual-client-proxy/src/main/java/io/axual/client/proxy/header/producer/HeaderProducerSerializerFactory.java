package io.axual.client.proxy.header.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.generic.serde.SerializerProxy;
import io.axual.client.proxy.generic.serde.SerializerProxyFactory;
import io.axual.client.proxy.wrapped.serde.WrappedSerializer;
import io.axual.client.proxy.wrapped.serde.WrappedSerializerConfig;

public class HeaderProducerSerializerFactory<T> implements SerializerProxyFactory<T> {
    private final Object backingSerializer;

    public HeaderProducerSerializerFactory(Object backingSerializer) {
        this.backingSerializer = backingSerializer;
    }

    @Override
    public SerializerProxy<T> create(Map<String, Object> configs, boolean isKey) {
        Map<String, Object> props = new HashMap<>(configs);
        props.put(WrappedSerializerConfig.SERIALIZER_CONFIG, backingSerializer);
        SerializerProxy<T> result = new WrappedSerializer<>();
        result.configure(props, isKey);
        return result;
    }
}
