package io.axual.client.proxy.switching.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.apache.kafka.common.serialization.Serializer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;

import io.axual.client.proxy.generic.producer.ExtendedProducerRecord;
import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.client.proxy.switching.discovery.DiscoverySubscriber;
import io.axual.client.proxy.switching.generic.SwitchingProxy;
import io.axual.common.concurrent.LockedObject;
import io.axual.common.tools.MapUtil;
import io.axual.discovery.client.tools.DiscoveryConfigParserV2;

/**
 * Proxy Kafka producer which takes care of switching.
 * @param <K> Key type.
 * @param <V> Value type.
 */
public class SwitchingProducer<K, V> extends SwitchingProxy<ProducerProxy<K, V>, SwitchingProducerConfig<K, V>> implements ProducerProxy<K, V> {

    /** Indicates if initTransactions has been called, as we need to keep this after a switch. */
    private boolean transactionsInited = false;

    public SwitchingProducer(Map<String, Object> configs) {
        super(new SwitchingProducerConfig<>(configs),
                new DiscoverySubscriber<>(
                        new DiscoveryConfigParserV2().parse(configs),
                        SwitchingProducer.class.getSimpleName(),
                        new ProducerSwitcher<>(),
                        false));
    }

    public SwitchingProducer(Map<String, Object> configs, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(SerdeUtil.addSerializersToConfigs(configs, keySerializer, valueSerializer));
    }

    public SwitchingProducer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public SwitchingProducer(Properties properties, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(MapUtil.objectToStringMap(properties), keySerializer, valueSerializer);
    }

    /**
     * Register the producer with the transaction coordinator of the cluster.
     * If this method was called and the producer switches over to a new cluster, it will re-register itself there.
     */
    @Override
    public void initTransactions() {
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.initTransactions();
        }
        transactionsInited = true;
    }

    /**
     * Start a transaction.
     */
    @Override
    public void beginTransaction() {
        // NOTE: this uses the "old" maybeReplaceProxiedObject; the reason is that we are beginning
        // a new transaction anyway, logically there can't be an old one to abort.
        // Calling beginTransaction() before committing or aborting a possible earlier one would constitute
        // an error on the part of the client programmer.
        if (maybeReplaceProxiedObject(false) && transactionsInited) {
            this.initTransactions();
        }
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.beginTransaction();
        }
    }

    /**
     * See {@link KafkaProducer#sendOffsetsToTransaction(Map, String)}
     * @throws TransactionSwitchedException if the producer switches over to another cluster during a transaction.
     */
    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> offsets,
                                         String consumerGroupId) {
        // NOTE: this checks the transactionsInited boolean first, to prevent any unnecessary switches.
        if (transactionsInited && maybeReplaceProxiedObject(false, transactionsInited)) {
            this.initTransactions();
            throw new TransactionSwitchedException("Producer switched to a new cluster. You need to start the transaction again.");
        }
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.sendOffsetsToTransaction(offsets, consumerGroupId);
        }
    }

    /**
     * See {@link KafkaProducer#sendOffsetsToTransaction(Map, ConsumerGroupMetadata)}
     * @throws TransactionSwitchedException if the producer switches over to another cluster during a transaction.
     */
    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> offsets,
                                         ConsumerGroupMetadata groupMetadata) throws ProducerFencedException {
        // need to document possible IllegalStateException here if we switched before.
        // NOTE: this checks the transactionsInited boolean first, to prevent any unnecessary switches.
        if (transactionsInited && maybeReplaceProxiedObject(false, transactionsInited)) {
            this.initTransactions();
            throw new TransactionSwitchedException("Producer switched to a new cluster. You need to start the transaction again.");
        }
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.sendOffsetsToTransaction(offsets, groupMetadata);
        }
    }

    /**
     * See {@link KafkaProducer#commitTransaction()}
     */
    @Override
    public void commitTransaction() {
        // Always commit on the cluster we're on, we might switch on the next send().
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.commitTransaction();
        }
    }

    /**
     * See {@link KafkaProducer#abortTransaction()}
     */
    @Override
    public void abortTransaction() {
        // Always abort on the cluster we're on, we might switch on the next send().
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.abortTransaction();
        }
    }

    /**
     * See {@link KafkaProducer#send(ProducerRecord)}
     * @throws TransactionSwitchedException if the producer switches over to another cluster during a transaction.
     */
    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> record) {
        // NOTE: maybeReplace...() should always be first in the condition to make sure that this also happens
        // if transactionsInited == false!
        if (maybeReplaceProxiedObject(false, transactionsInited) && transactionsInited) {
            this.initTransactions();
            throw new TransactionSwitchedException("Producer switched to a new cluster. You need to start the transaction again.");
        }
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.send(convertProducerRecord(record));
        }
    }

    /**
     * See {@link KafkaProducer#send(ProducerRecord, Callback)}
     * @throws TransactionSwitchedException if the producer switches over to another cluster during a transaction.
     */
    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> record, Callback callback) {
        // NOTE: maybeReplace...() should always be first in the condition to make sure that this also happens
        // if transactionsInited == false!
        if (maybeReplaceProxiedObject(false, transactionsInited) && transactionsInited) {
            this.initTransactions();
            throw new TransactionSwitchedException("Producer switched to a new cluster. You need to start the transaction again.");
        }
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.send(convertProducerRecord(record), callback);
        }
    }

    /**
     * See {@link KafkaProducer#flush()}
     */
    @Override
    public void flush() {
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.flush();
        }
    }

    /**
     * See {@link KafkaProducer#partitionsFor(String)}
     */
    @Override
    public List<PartitionInfo> partitionsFor(String topic) {
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.partitionsFor(topic);
        }
    }

    /**
     * See {@link KafkaProducer#metrics()}
     */
    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        try (LockedObject<ProducerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.metrics();
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // End of public interface of KafkaProducer
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private ExtendedProducerRecord<K, V> convertProducerRecord(ProducerRecord<K, V> record) {
        // Set the ExtendedProducerRecord context for downstream usage. When the ResolvingProducer
        // is chained to the SwitchingProducer, then it will receive the relevant "tenant",
        // "instance" and "environment" fields through this context. Keys are only added if they
        // do not exist already.

        // Extract and extend the context from the record if possible
        if (record instanceof ExtendedProducerRecord) {
            ExtendedProducerRecord<K, V> extendedRecord = (ExtendedProducerRecord<K, V>) record;

            // Add all discovery properties to the context for downstream usage
            MapUtil.putAllIfAbsent(extendedRecord.context(), getCurrentDiscoveryResult().getConfigs());

            return extendedRecord;
        }

        // Return an ExtendedProducerRecord with discovery context
        return new ExtendedProducerRecord<>(
                record.topic(),
                record.partition(),
                record.timestamp(),
                record.key(),
                record.value(),
                record.headers(),
                new HashMap<>(getCurrentDiscoveryResult().getConfigs()));
    }
}
