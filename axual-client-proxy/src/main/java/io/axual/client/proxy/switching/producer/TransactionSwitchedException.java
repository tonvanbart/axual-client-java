package io.axual.client.proxy.switching.producer;

/**
 * Exception signalling that a cluster switch occurred in the middle of a Kafka transaction.
 * This is an unchecked exception.
 */
public class TransactionSwitchedException extends RuntimeException {

    public TransactionSwitchedException() {
    }

    public TransactionSwitchedException(String message) {
        super(message);
    }

    public TransactionSwitchedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransactionSwitchedException(Throwable cause) {
        super(cause);
    }

    public TransactionSwitchedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
