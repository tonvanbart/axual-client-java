package io.axual.discovery.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

/**
 * This exception indicates something went wrong during Discovery Client Registration
 */
public class DiscoveryClientRegistrationException extends Exception {
    public DiscoveryClientRegistrationException(String message) {
        super(message);
    }

    public DiscoveryClientRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DiscoveryClientRegistrationException(Throwable cause) {
        super(cause);
    }

    protected DiscoveryClientRegistrationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
