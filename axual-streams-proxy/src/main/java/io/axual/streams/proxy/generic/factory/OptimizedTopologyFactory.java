package io.axual.streams.proxy.generic.factory;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;

import java.util.Properties;

import io.axual.common.annotation.InterfaceStability;

/**
 * Interface needed to provide the topology for the streaming job
 * Implementing this versus the TopologyFactory will give access to the configuration properties.
 */
@InterfaceStability.Evolving
public interface OptimizedTopologyFactory extends TopologyFactory {
    Topology create(StreamsBuilder builder, Properties properties);

    @Override
    default Topology create(StreamsBuilder builder) {
        return create(builder, null);
    }

    class Wrapper implements OptimizedTopologyFactory {
        private final TopologyFactory topologyFactory;

        Wrapper(TopologyFactory topologyFactory) {
            this.topologyFactory = topologyFactory;
        }

        @Override
        public Topology create(StreamsBuilder builder, Properties properties) {
            return topologyFactory.create(builder);
        }

        @Override
        public Topology create(StreamsBuilder builder) {
            return topologyFactory.create(builder);
        }

        public static OptimizedTopologyFactory ensureOptimizedTopologyFactory(TopologyFactory topologyFactory) {
            if (topologyFactory == null) {
                return null;
            } else if (topologyFactory instanceof OptimizedTopologyFactory) {
                return (OptimizedTopologyFactory) topologyFactory;
            } else {
                return new Wrapper(topologyFactory);
            }
        }
    }
}
