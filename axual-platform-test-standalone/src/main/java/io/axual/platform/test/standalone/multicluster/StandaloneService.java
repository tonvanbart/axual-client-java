package io.axual.platform.test.standalone.multicluster;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static io.axual.platform.test.standalone.config.ClusterConstants.MIN_DISTRIBUTOR_DISTANCE;
import static io.axual.platform.test.standalone.config.ClusterConstants.MIN_DISTRIBUTOR_TIMEOUT;
import static io.axual.platform.test.standalone.config.ClusterConstants.MIN_TTL;

import io.axual.common.config.ClientConfig;
import io.axual.platform.test.core.ClusterUnit;
import io.axual.platform.test.core.ClusterUnitConfig;
import io.axual.platform.test.core.InstanceUnitConfig;
import io.axual.platform.test.core.PlatformUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.standalone.AxualStandalone;
import io.axual.platform.test.standalone.config.StandaloneApplicationConfig;
import io.axual.platform.test.standalone.config.StandaloneStreamConfig;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.XSlf4j;
import org.apache.avro.Schema;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Health.Builder;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;

/**
 * The Standalone Service provides basic control functionality for the Platform Test Standalone
 */
@XSlf4j
@Service
public class StandaloneService implements InitializingBean, HealthIndicator {

  private final AxualStandalone standalone;

  private HashMap<StandaloneApplicationConfig, ClientConfig> applicationMap = new HashMap<>();
  private HashMap<ClientConfig, StandaloneApplicationConfig> reverseAplicationMap = new HashMap<>();

  @Autowired
  public StandaloneService(AxualStandalone standalone) {
    log.entry(standalone);
    this.standalone = standalone;
    log.exit();
  }

  @Override
  public void afterPropertiesSet() {
    log.entry();
    // build lookup map for rest service
    for (ClientConfig clientConfig : standalone.getPlatformUnit().getInstance().getApplications()) {
      String assignedCluster = standalone.getPlatformUnit().getInstance()
          .getClusterFor(clientConfig).getName();
      StandaloneApplicationConfig app = new StandaloneApplicationConfig(
          clientConfig.getApplicationId(), clientConfig.getEnvironment(), assignedCluster);
      applicationMap.put(app, clientConfig);
      reverseAplicationMap.put(clientConfig, app);
    }
    log.exit();
  }

  /**
   * Get the Discovery API TTL Setting
   *
   * @return TTL in milliseconds
   */
  public long getTTL() {
    log.entry();
    long result = standalone.getPlatformUnit().getInstance().getDiscoveryUnit().getTtl();
    return log.exit(result);
  }

  /**
   * Set the Discovery API TTL Setting
   *
   * @param ttl the new TTL value, if smaller than {@link io.axual.platform.test.standalone.config.ClusterConstants#MIN_TTL},
   *            MIN_TTL will be set
   * @return the new TTL value
   */
  public long setTTL(long ttl) {
    log.entry(ttl);
    if (ttl < MIN_TTL) {
      ttl = MIN_TTL;
    }
    standalone.getPlatformUnit().getInstance().getDiscoveryUnit().setTtl(ttl);
    return log.exit(ttl);
  }

  /**
   * Get the Discovery API Distributor Timeout Setting
   *
   * @return Distributor Timeout in milliseconds
   */
  public long getDistributorTimeout() {
    log.entry();
    long result = standalone.getPlatformUnit().getInstance().getDiscoveryUnit()
        .getDistributorTimeout();
    return log.exit(result);
  }

  /**
   * Set the Discovery API API Distributor Timeout Setting
   *
   * @param distributorTimeout the new TTL value, if smaller than {@link io.axual.platform.test.standalone.config.ClusterConstants#MIN_DISTRIBUTOR_TIMEOUT},
   *                           MIN_DISTRIBUTOR_TIMEOUT will be set
   * @return the new Distributor Timeout value
   */
  public long setDistributorTimeout(long distributorTimeout) {
    log.entry(distributorTimeout);
    if (distributorTimeout < MIN_DISTRIBUTOR_TIMEOUT) {
      distributorTimeout = MIN_DISTRIBUTOR_TIMEOUT;
    }
    standalone.getPlatformUnit().getInstance().getDiscoveryUnit()
        .setDistributorTimeout(distributorTimeout);
    return log.exit(distributorTimeout);
  }

  /**
   * Get the Discovery API Distributor Distance Setting
   *
   * @return Distributor Distance in steps
   */
  public long getDistributorDistance() {
    log.entry();
    long result = standalone.getPlatformUnit().getInstance().getDiscoveryUnit()
        .getDistributorDistance();
    return log.exit(result);
  }

  /**
   * Set the Discovery API API Distributor Distance Setting
   *
   * @param distributorDistance the new TTL value, if smaller than {@link io.axual.platform.test.standalone.config.ClusterConstants#MIN_DISTRIBUTOR_DISTANCE},
   *                            MIN_DISTRIBUTOR_DISTANCE will be set
   * @return the new Distributor Distance value
   */
  public long setDistributorDistance(long distributorDistance) {
    log.entry(distributorDistance);
    if (distributorDistance < MIN_DISTRIBUTOR_DISTANCE) {
      distributorDistance = MIN_DISTRIBUTOR_DISTANCE;
    }
    standalone.getPlatformUnit().getInstance().getDiscoveryUnit()
        .setDistributorDistance(distributorDistance);
    return log.exit(distributorDistance);
  }

  /**
   * Get the instance configuration for the platform
   *
   * @return the Instance Configuration used
   */
  public InstanceUnitConfig getInstanceConfig() {
    log.entry();
    InstanceUnitConfig result = standalone.getPlatformUnit().getInstanceUnitConfig();
    return log.exit(result);
  }

  /**
   * Get the cluster configurations for the platform
   *
   * @return A list of cluster configurations
   */
  public List<ClusterUnitConfig> getAllClusterConfigs() {
    log.entry();
    List<ClusterUnitConfig> result = new ArrayList<>(
        this.standalone.getPlatformUnit().getClusterUnitConfigs().values());
    return log.exit(result);
  }

  /**
   * Get the cluster configurations for the specified cluster
   *
   * @param clusterName The name of the cluster
   * @return the cluster configuration for the specified cluster
   */
  public ClusterUnitConfig getClusterConfig(String clusterName) {
    log.entry(clusterName);
    ClusterUnitConfig clusterUnitConfig = this.standalone.getPlatformUnit().getClusterUnitConfigs()
        .get(clusterName);
    if (clusterUnitConfig == null) {
      throw new IllegalArgumentException(String.format("Unknown clusterName: %s", clusterName));
    }
    return log.exit(clusterUnitConfig);
  }

  /**
   * Get all registered applications
   *
   * @return A set of applications
   */
  public Set<StandaloneApplicationConfig> getApplications() {
    log.entry();
    Set<StandaloneApplicationConfig> result = applicationMap.keySet();
    return log.exit(result);
  }

  /**
   * Add a new application registration.
   * <p>
   * Setting the environment to null will add the application with the default environment of the
   * standalone. If the clustername is not set then the first registered cluster will be used
   *
   * @param applicationId the identifier of the application
   * @param environment   the environment where this application is deployed
   * @param clusterName   the cluster to assign this application to
   * @return the application registration
   */
  public StandaloneApplicationConfig addApplication(String applicationId, String environment,
      String clusterName) {
    log.entry(applicationId, environment, clusterName);
    if (environment == null) {
      environment = standalone.getEnvironment();
    }
    StandaloneApplicationConfig application = new StandaloneApplicationConfig(applicationId,
        environment, clusterName);
    ClusterUnit clusterUnit = standalone.getPlatformUnit().getCluster(clusterName);
    if (clusterUnit == null) {
      clusterUnit = standalone.getPlatformUnit().getCluster(0);
    }
    ClientConfig clientConfig = standalone.getPlatformUnit().getInstance()
        .getClientConfig(applicationId, false, environment, clusterUnit);
    application = new StandaloneApplicationConfig(clientConfig.getApplicationId(),
        clientConfig.getEnvironment(), clusterUnit.getName());
    applicationMap.put(application, clientConfig);
    reverseAplicationMap.put(clientConfig, application);
    return log.exit(application);
  }

  /**
   * Assign an application to a specific cluster Setting the environment to null will add the
   * application with the default environment of the standalone.
   *
   * @param applicationId the identifier of the application
   * @param environment   the environment where this application is deployed
   * @param clusterName   the cluster to assign this application to
   * @return the application registration
   */
  public StandaloneApplicationConfig assignApplicationToCluster(String applicationId,
      String environment, String clusterName) {
    log.entry(applicationId, environment, clusterName);
    if (environment == null) {
      environment = standalone.getEnvironment();
    }
    StandaloneApplicationConfig application = new StandaloneApplicationConfig(applicationId,
        environment, clusterName);
    ClusterUnit clusterUnit = standalone.getPlatformUnit().getCluster(clusterName);
    ClientConfig clientConfig = applicationMap.get(application);
    if (clusterUnit == null) {
      throw new IllegalArgumentException(String.format("Unknown clusterName: %s", clusterName));
    }
    if (clientConfig == null) {
      throw new IllegalArgumentException(String.format("Unknown application: %s", application));
    }
    standalone.getPlatformUnit().getInstance().directApplicationTo(clientConfig, clusterUnit);
    application.setAssignedCluster(clusterUnit.getName());
    applicationMap.put(application, clientConfig);
    reverseAplicationMap.put(clientConfig, application);
    return log.exit(application);
  }

  /**
   * Gets a map of cluster names with a list of applications registered to that cluster
   *
   * @return the map with applications per cluster
   */
  public Map<String, List<StandaloneApplicationConfig>> getApplicationsPerCluster() {
    log.entry();
    HashMap<String, List<StandaloneApplicationConfig>> result = new HashMap<>();
    for (ClientConfig clientConfig : standalone.getPlatformUnit().getInstance().getApplications()) {
      ClusterUnit clusterUnit = standalone.getPlatformUnit().getInstance()
          .getClusterFor(clientConfig);
      if (clusterUnit != null) {
        StandaloneApplicationConfig application = reverseAplicationMap.get(clientConfig);
        result.compute(clusterUnit.getName(), (a, b) -> b == null ? new ArrayList<>() : b)
            .add(application);
      }
    }
    return log.exit(result);
  }

  /**
   * Get a list off applications registered to a specific cluster
   *
   * @param clusterName the name of the target cluster
   * @return a list of applications registered to the specified cluster
   */
  public List<StandaloneApplicationConfig> getApplicationsForCluster(String clusterName) {
    log.entry(clusterName);
    if (standalone.getPlatformUnit().getCluster(clusterName) == null) {
      throw new IllegalArgumentException(String.format("Unknown clusterName: %s", clusterName));
    }
    Map<String, List<StandaloneApplicationConfig>> map = getApplicationsPerCluster();
    List<StandaloneApplicationConfig> result = map.getOrDefault(clusterName, new ArrayList<>());
    return log.exit(result);
  }

  private static StandaloneStreamConfig convertStreamConfig(StreamConfig config, boolean isRaw) {
    log.entry(config, isRaw);
    StandaloneStreamConfig newConfig = new StandaloneStreamConfig();
    newConfig.setName(config.getName());
    newConfig.setEnvironment(config.getEnvironment());
    newConfig.setPartitions(config.getPartitions());
    newConfig.setIsRawTopic(isRaw);
    newConfig
        .setKeyClass(config.getKeySchema() == null ? null : config.getKeySchema().getFullName());
    newConfig.setValueClass(
        config.getValueSchema() == null ? null : config.getValueSchema().getFullName());
    return log.exit(newConfig);
  }

  private static Map<String, StandaloneStreamConfig> convertStreamConfigMap(
      Map<String, StreamConfig> input, boolean isRaw) {
    log.entry(input, isRaw);
    Map<String, StandaloneStreamConfig> topics = new HashMap<>();
    for (Map.Entry<String, StreamConfig> entry : input.entrySet()) {
      topics.put(entry.getKey(), convertStreamConfig(entry.getValue(), isRaw));
    }
    return log.exit(topics);
  }

  /**
   * Get a map of stream registration names and the configurations
   *
   * @return the map of stream configurations
   */
  public Map<String, StandaloneStreamConfig> getStreams() {
    log.entry();
    Map<String, StandaloneStreamConfig> result = convertStreamConfigMap(
        standalone.getPlatformUnit().getInstance().getStreams(), false);
    return log.exit(result);
  }

  /**
   * Get a map of raw topic registration names and the configurations
   *
   * @return the map of raw topic configurations
   */
  public Map<String, StandaloneStreamConfig> getRawTopics() {
    log.entry();
    Map<String, StandaloneStreamConfig> result = convertStreamConfigMap(
        standalone.getPlatformUnit().getInstance().getRawTopics(), true);
    return log.exit(result);
  }

  /**
   * Create a new stream or raw topic. Setting the environment to null will add the application with
   * the default environment of the standalone.
   *
   * @param isRawTopic  do not resolve the name during create, but use the exact name provided
   * @param name        the name of the stream or topic
   * @param environment the environment where this topic needs to be created
   * @param partitions  the number of partitions for the topic
   * @param keyClass    the Avro Schema class name for the key, can be null
   * @param valueClass  the Avro Schema class name for the value, can be null
   * @return the stream configuration created;
   */
  public StandaloneStreamConfig createTopic(boolean isRawTopic, String name, String environment,
      int partitions, String keyClass, String valueClass) {
    log.entry(isRawTopic, name, environment, partitions, keyClass, valueClass);
    if (environment == null) {
      environment = standalone.getEnvironment();
    }
    StreamConfig newConfig = new StreamConfig();
    newConfig.setName(name);
    newConfig.setEnvironment(environment);
    newConfig.setPartitions(partitions);
    newConfig.setKeySchema(keyClass == null ? null : getSchema(keyClass));
    newConfig.setValueSchema(valueClass == null ? null : getSchema(valueClass));

    if (isRawTopic) {
      if (!standalone.getPlatformUnit().getInstance().addRawTopic(newConfig)) {
        return null;
      }
    } else {
      if (!standalone.getPlatformUnit().getInstance().addStream(newConfig)) {
        return null;
      }
    }
    return convertStreamConfig(newConfig, isRawTopic);
  }

  private Schema getSchema(String clazz) {
    log.entry(clazz);
    Schema newSchema = null;
    try {
      Class randomClass = Thread.currentThread().getContextClassLoader().loadClass(clazz);
      Method getClassSchema = randomClass.getMethod("getClassSchema");
      newSchema = (Schema) getClassSchema.invoke(randomClass.newInstance());
    } catch (ClassNotFoundException e) {
      log.error("Could not find class for name {}", clazz);
      log.catching(e);
    } catch (Exception e) {
      log.error("Could not create a Schema from class {}", clazz);
      log.catching(e);
    }
    return log.exit(newSchema);
  }

  @Override
  public Health getHealth(boolean includeDetails) {
    PlatformUnit pu = standalone.getPlatformUnit();

    Builder builder = Health.up();
    boolean discoveryRunning = pu.getInstance().getDiscoveryUnit().isRunning();

    if (!discoveryRunning) {
      builder.down();
    }
    if (includeDetails) {
      builder = builder.withDetail("Discovery2", discoveryRunning ? "UP" : "DOWN");
    }

    for (String clusterName : pu.getClusterUnitConfigs().keySet()) {
      ClusterUnit clusterUnit = pu.getCluster(clusterName);
      boolean srUp = clusterUnit.getSchemaRegistryUnit().isRunning();
      boolean brokerUp = clusterUnit.getKafkaUnit().isRunning();
      if (!srUp || !brokerUp) {
        builder.down();
      }
      if (includeDetails) {
        builder = builder.withDetail(clusterName + "_SchemaRegistry", srUp ? "UP" : "DOWN");
        builder = builder.withDetail(clusterName + "_Broker", brokerUp ? "UP" : "DOWN");
      }
    }
    return builder.build();
  }

  @Override
  public Health health() {
    return getHealth(false);
  }
}
