package io.axual.client.proxy.header.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.axual.client.proxy.exception.DuplicateValueHeaderException;
import io.axual.client.proxy.generic.serde.BaseSerializerProxy;
import io.axual.serde.utils.HeaderUtils;
import io.axual.serde.utils.SerdeUtils;
import io.axual.serde.valueheader.ValueHeader;
import io.axual.serde.valueheader.ValueHeaderSerializer;

import static io.axual.client.proxy.lineage.LineageHeaders.COPY_FLAGS_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.MESSAGE_ID_HEADER;
import static io.axual.client.proxy.lineage.LineageHeaders.SERIALIZATION_TIME_HEADER;

public class HeaderSerializer<T> extends BaseSerializerProxy<T, HeaderSerializerConfig<T>> {
    private static final Logger LOG = LoggerFactory.getLogger(HeaderSerializer.class);
    private ValueHeaderSerializer valueHeaderSerializer;

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        configure(new HeaderSerializerConfig<>(new HashMap<>(configs), isKey));

        // Instantiate our value header deserializer
        valueHeaderSerializer = new ValueHeaderSerializer();
        valueHeaderSerializer.configure(config.getDownstreamConfigs(), isKey);
    }

    @Override
    public byte[] serialize(String topic, T object) {
        return serialize(topic, null, object);
    }

    @Override
    public byte[] serialize(String topic, Headers headers, T object) {
        // Encode the object using the backing serializer
        byte[] bodyData = backingSerializer.serialize(topic, headers, object);

        // Check if we need to encode the header values
        if (!config.isKey() && config.valueHeadersEnabled()) {
            ensureNoValueHeaders(object);

            ValueHeader valueHeader = headers != null ? deriveFromLineageHeaders(headers) : new ValueHeader();

            // Serialize the header and into a byte array
            byte[] headerData = valueHeaderSerializer.serialize(topic, valueHeader);

            // If body is empty, then no need to concatenate arrays
            if (bodyData == null) {
                return headerData;
            }

            // Concatenate the header and body byte streams
            byte[] result = new byte[headerData.length + bodyData.length];
            System.arraycopy(headerData, 0, result, 0, headerData.length);
            System.arraycopy(bodyData, 0, result, headerData.length, bodyData.length);
            return result;
        }

        return bodyData;
    }

    public static ValueHeader deriveFromLineageHeaders(Headers headers) {
        UUID messageId = HeaderUtils.decodeUuidHeader(headers.lastHeader(MESSAGE_ID_HEADER));
        Long serializationTime = HeaderUtils.decodeLongHeader(headers.lastHeader(SERIALIZATION_TIME_HEADER));
        Integer copyFlags = HeaderUtils.decodeIntegerHeader(headers.lastHeader(COPY_FLAGS_HEADER));

        return new ValueHeader(
                messageId != null ? messageId : UUID.randomUUID(),
                serializationTime != null ? serializationTime : System.currentTimeMillis(),
                copyFlags != null ? (byte) (copyFlags & 0xff) : (byte) 0
        );
    }

    private static void ensureNoValueHeaders(Object object) {
        if (object instanceof byte[] && SerdeUtils.containsValueHeader((byte[]) object)) {
            // Should never reach here!!
            LOG.error("Duplicate value header encoding!!");
            throw new DuplicateValueHeaderException("Found value headers in byte array to serialize");
        }
    }

    @Override
    public void close(Duration duration) {
        if (valueHeaderSerializer != null) {
            valueHeaderSerializer.close();
        }
        super.close(duration);
    }
}
