package io.axual.client.proxy.generic.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ExtendedProducerRecord<K, V> extends ProducerRecord<K, V> {
    private final Map<String, Object> context;
    private Headers overriddenHeaders;

    public ExtendedProducerRecord(String topic, Integer partition, Long timestamp, K key, V value, Iterable<Header> headers, Map<String, Object> context) {
        super(topic, partition, timestamp, key, value, headers);
        // The context is taken over directly from the caller to allow context to be transferred
        // downstream in the proxy chain. Through modifying its contents the toplevel caller of
        // Producer::send can later query a message's context. The sharing of context is a
        // required feature!! Do not take away this feature by making it a clone or unmodifiable
        // Map.
        this.context = context != null ? new HashMap<>(context) : new HashMap<>();
    }

    public Map<String, Object> context() {
        return context;
    }

    @Override
    public Headers headers() {
        return overriddenHeaders != null ? overriddenHeaders : super.headers();
    }

    public void setHeaders(Headers headers) {
        overriddenHeaders = headers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof ExtendedProducerRecord)) {
            return false;
        } else if (!super.equals(o)) {
            return false;
        } else {
            ExtendedProducerRecord<?, ?> that = (ExtendedProducerRecord) o;
            return Objects.equals(this.context, that.context);
        }
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.context != null ? this.context.hashCode() : 0);
        return result;
    }
}
