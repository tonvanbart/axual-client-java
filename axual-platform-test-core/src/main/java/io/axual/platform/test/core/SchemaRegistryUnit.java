package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

import com.github.tomakehurst.wiremock.WireMockServer;
import io.axual.common.annotation.InterfaceStability;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.avro.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@InterfaceStability.Evolving
public class SchemaRegistryUnit {

  private static final Logger LOG = LoggerFactory.getLogger(SchemaRegistryUnit.class);
  private static final String LOCALHOST = "localhost";
  private static final String BIND_ALL = "0.0.0.0";
  private static final String POST_SUBJECT_VERSIONS_RESPONSE_TEMPLATE = "{\"id\":%d}";
  private static final String GET_SUBJECT_VERSIONS_RESPONSE_TEMPLATE = "[ 1 ]";
  private static final String GET_SCHEMA_BY_ID_RESPONSE_TEMPLATE = "{\"schema\": \"%s\"}";
  private static final String GET_SPECIFIC_SUBJECT_VERSION_RESPONSE_TEMPLATE = "{\"subject\":\"%s\",\"version\":1,\"id\":%d,\"schema\": \"%s\"}";

  private final WireMockServer server;
  private final String bindAddress;
  private final String advertisedAddress;
  private final int port;
  List<String> subjects = new ArrayList<>();
  List<Integer> ids = new ArrayList<>();

  public SchemaRegistryUnit() {
    this(null, null, 0);
  }

  public SchemaRegistryUnit(String bindAddress, String advertisedAddress, int port) {
    this.bindAddress = bindAddress != null ? bindAddress : BIND_ALL;
    this.advertisedAddress = advertisedAddress != null ? advertisedAddress : LOCALHOST;
    this.port = port;
    this.server = new WireMockServer(
        wireMockConfig().bindAddress(this.bindAddress).port(this.port).disableRequestJournal());
  }

  int getPort() {
    return server.port();
  }

  void registerSchema(final String topic, final Schema keySchema, final Schema valueSchema) {
    if (keySchema != null) {
      registerSubject(topic + "-key", keySchema);
    }
    if (valueSchema != null) {
      registerSubject(topic + "-value", valueSchema);
    }
  }

  private String makeJsonFromList(List<? extends Object> list) {
    StringBuilder subjectResponseBuilder = new StringBuilder();
    subjectResponseBuilder.append(String.format("[%n"));
    Iterator<?> iterator = list.iterator();
    while (iterator.hasNext()) {
      String next = String.format("\"%s\"", iterator.next());
      if (iterator.hasNext()) {
        subjectResponseBuilder.append(String.format("%s,%n", next));
      } else {
        subjectResponseBuilder.append(String.format("%s%n", next));
      }
    }
    subjectResponseBuilder.append(String.format("]%n"));
    return subjectResponseBuilder.toString();
  }

  private synchronized void registerSubject(final String subject, final Schema schema) {
    if (schema != null) {
      if (subjects.contains(subject)) {
        LOG.warn("Subject '{}' already exists, skipping registration", subject);
        return;
      }

      subjects.add(subject);
      Collections.sort(subjects);

      // Use schema hash to enable identical checks
      final int id = schema.toString(false).hashCode();
      if (!ids.contains(id)) {
        ids.add(id);
        Collections.sort(ids);
      }

      server.stubFor(get(urlMatching("^\\/subjects\\/" + subject + "\\/versions\\/?"))
          .willReturn(aResponse()
              .withStatus(200)
              .withBody(GET_SUBJECT_VERSIONS_RESPONSE_TEMPLATE)));

      server.stubFor(get(urlMatching("^\\/subjects\\/?"))
          .willReturn(aResponse()
              .withStatus(200)
              .withBody(makeJsonFromList(subjects))));

      server.stubFor(get(urlMatching("^\\/schemas\\/ids\\/?"))
          .willReturn(aResponse()
              .withStatus(200)
              .withBody(makeJsonFromList(ids))));

      // Used by producers
      server.stubFor(post(urlMatching("^\\/subjects\\/" + subject + "\\/versions\\/?"))
          .willReturn(aResponse()
              .withStatus(200)
              .withBody(String.format(POST_SUBJECT_VERSIONS_RESPONSE_TEMPLATE, id))));

      // Used by Consumers to fetch their deserialize Schema
      server.stubFor(get(urlEqualTo("/subjects/" + subject + "/versions/1"))
          .willReturn(aResponse()
              .withStatus(200)
              .withBody(String.format(GET_SPECIFIC_SUBJECT_VERSION_RESPONSE_TEMPLATE, subject, id,
                  jsonEscape(schema.toString())))));

      // Used by Consumers to fetch their deserialize Schema
      server.stubFor(get(urlEqualTo("/subjects/" + subject + "/versions/latest"))
          .willReturn(aResponse()
              .withStatus(200)
              .withBody(String.format(GET_SPECIFIC_SUBJECT_VERSION_RESPONSE_TEMPLATE, subject, id,
                  jsonEscape(schema.toString())))));

      // Used by Consumers to fetch their deserialize Schema
      server.stubFor(get(urlEqualTo("/schemas/ids/" + id))
          .willReturn(aResponse()
              .withStatus(200)
              .withBody(String
                  .format(GET_SCHEMA_BY_ID_RESPONSE_TEMPLATE, jsonEscape(schema.toString())))));
    }
  }

  public String getAdvertisedAddress() {
    return advertisedAddress;
  }

  public String getBaseURL() {
    return String.format("http://%s:%d", this.advertisedAddress, this.server.port());
  }

  void start() {
    server.start();
  }

  void stop() {
    server.stop();
  }

  public boolean isRunning() {
    return server.isRunning();
  }

  private String jsonEscape(final String schema) {
    return schema.replace("\"", "\\\"");
  }
}
