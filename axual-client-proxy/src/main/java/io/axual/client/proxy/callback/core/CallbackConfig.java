package io.axual.client.proxy.callback.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;

import io.axual.common.config.BaseConfig;

public class CallbackConfig extends BaseConfig {
    public static final String CALL_FACTORY_CONFIG = "callback.factory";

    private final MethodCallFactory callFactory;

    public CallbackConfig(Map<String, Object> configs) {
        super(configs);
        callFactory = getConfiguredInstance(CALL_FACTORY_CONFIG, MethodCallFactory.class, true);
    }

    public MethodCallFactory getCallFactory() {
        if (callFactory != null) {
            return callFactory;
        }

        return (callId, object, method) -> new MethodCall() {
            @Override
            public void close() {
                // Zero operation
            }

            @Override
            public void onEnter(String[] paramNames, Object... params) {
                // Zero operation
            }

            @Override
            public void onException(Throwable t) {
                // Zero operation
            }

            @Override
            public void onResult(Object result) {
                // Zero operation
            }

            @Override
            public void onExit() {
                // Zero operation
            }
        };
    }
}
