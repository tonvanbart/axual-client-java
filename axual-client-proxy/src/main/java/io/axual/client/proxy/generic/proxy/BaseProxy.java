package io.axual.client.proxy.generic.proxy;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.time.Duration;
import java.util.Map;

import io.axual.common.config.BaseConfig;

public class BaseProxy<C extends BaseConfig> implements Proxy {
    private static final Duration INDEFINITE = Duration.ofDays(999999);

    protected C config;

    public BaseProxy(C config) {
        this.config = config;
    }

    @Override
    public Map<String, Object> getConfigs() {
        return config.getConfigs();
    }

    @Override
    public Object getConfig(String key) {
        return config.getConfigs().get(key);
    }

    @Override
    public final void close() {
        close(INDEFINITE);
    }

    @Override
    public void close(Duration timeout) {
        // Nothing to close in the base class
    }
}
