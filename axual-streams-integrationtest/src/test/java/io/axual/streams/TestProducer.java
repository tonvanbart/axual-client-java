package io.axual.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.producer.ProduceCallback;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.client.test.Random;
import io.axual.common.tools.TimestampUtil;
import io.axual.platform.test.core.ClusterUnit;
import io.axual.platform.test.junit4.BasePlatformUnit;

import static com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly;
import static java.util.UUID.randomUUID;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class TestProducer extends Thread implements AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(TestProducer.class);

    private final String applicationId = "testproducer-" + randomUUID().toString();
    private final BasePlatformUnit platform;
    private final AxualClient client;
    private final String topic;

    private final boolean logProduces;
    private Producer<Random, Random> producer;
    private boolean shouldStop = false;
    private long produceCounter = 0;

    public TestProducer(BasePlatformUnit platform, String topic, boolean logProduces) {
        this.platform = platform;
        this.topic = topic;
        this.logProduces = logProduces;
        client = new AxualClient(platform.instance().getClientConfig(applicationId));
    }

    @Override
    public void run() {
        long baseTimestamp = System.currentTimeMillis();

        SpecificAvroProducerConfig<Random, Random> specificProducerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.LOSING_ORDER)
                .setProxyChain(ProxyChain.newBuilder()
                        .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
                        .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
//                                .append(ProxyTypeRegistry.LOGGING_PROXY_ID, MapUtil.put(new HashMap<>(), "level", "WARN"))
                        .append(ProxyTypeRegistry.LINEAGE_PROXY_ID)
                        .append(ProxyTypeRegistry.HEADER_PROXY_ID)
                        .build())
                .build();

        try (final Producer<Random, Random> producer = client.buildProducer(specificProducerConfig)) {
            while (!shouldStop) {
                ProducerMessage<Random, Random> message = ProducerMessage.<Random, Random>newBuilder()
                        .setStream(topic)
                        .setKey(Random.newBuilder()
                                .setRandom(String.valueOf(produceCounter))
                                .build())
                        .setValue(Random.newBuilder()
                                .setRandom("" + TimestampUtil.formatDelta(System.currentTimeMillis() - baseTimestamp))
                                .build())
                        .build();

                if (logProduces) {
                    LOG.info("Producing on " + topic + ": #" + message.getKey().getRandom() + " @ " + message.getValue().getRandom());
                }

                try {
                    producer.produce(message, new ProduceCallback<Random, Random>() {
                        @Override
                        public void onComplete(ProducedMessage<Random, Random> message) {
                            LOG.info("Produced #" + message.getMessage().getKey().getRandom() + " on " + message.getStream() + "@" + message.getCluster());
                        }

                        @Override
                        public void onError(ProducerMessage<Random, Random> message, ExecutionException exception) {
                            LOG.error("Error producing #" + message.getKey().getRandom() + ": " + exception);
                        }
                    }).get();
                    produceCounter++;
                } catch (Exception e) {
                    // Ignore
                }

                sleepUninterruptibly(100, MILLISECONDS);
            }
            LOG.info("Closing producer");
        }
        LOG.info("Producer closed");
    }

    public void close() {
        LOG.info("Closing producer client");
        client.close();
        LOG.info("Producer client closed");
    }

    public long getProduceCounter() {
        return produceCounter;
    }

    public void stopProducing() {
        shouldStop = true;
    }

    public void switchToCluster(ClusterUnit cluster) {
        if (client != null) {
            platform.platform().getInstance().directApplicationTo(client.getConfig(), cluster);
        }
    }
}
