## Some background on proxy switching

Proxy switching can occur when the switching layer is configured in the proxy config.
When Discovery API indicates that the cluster we're connecting to is going to (be taken) offline,
the switching layer will transparently switch over to another cluster.

### classes overview
With the switching producer as an example, this is an overview of the classes involved:
```plantuml
class SwitchingProducer extends SwitchingProxy
class SwitchingProxy extends DynamicClientProxy {
	+maybeReplaceProxiedObject(boolean)
}
class DynamicClientProxy {
}
class DiscoverySubscriber<T extends ClientProxy> implements ClientProxyReplacer {
	switcher: ClientProxySwitcher
}
interface ClientProxyReplacer {
	+needToReplace(): boolean
	+replace(oldProxy: T, config: C): T
	+close()
}
interface ClientProxySwitcher {
}

SwitchingProxy --> DiscoverySubscriber: contains
DynamicClientProxy --> ClientProxyReplacer: contains
DiscoverySubscriber --> ClientProxySwitcher
note right of DynamicClientProxy: proxy replacer in client proxy is same object instance\nas subscriber in switching proxy!

```
### High level logic flow
Rough overview of the call sequence in case of a switch, again with the producer as an example:
```plantuml
autoactivate on

participant SwitchingProducer #99ff99
participant SwitchingProxy #99ff99
participant DynamicClientProxy #99ff99
Note right of SwitchingProducer: <b>NOTE</b> the lightgreen participants are all the\nsame instance of an extension hierarchy

[-> SwitchingProducer: send(record)
SwitchingProducer -> SwitchingProxy: maybeReplaceProxiedObject
SwitchingProxy -> DiscoverySubscriber: checkDiscovery()
DiscoverySubscriber -> DiscoveryClientRegistry: checkProperties
DiscoveryClientRegistry --> DiscoverySubscriber: onDiscoveryPropertiesChange()
note right: all subscribed subscribers
DiscoverySubscriber -> DiscoverySubscriber: set needToSwitch\nkeep result
deactivate DiscoverySubscriber
DiscoverySubscriber --> SwitchingProxy
SwitchingProxy -> DynamicClientProxy: maybeReplaceProxiedObject()
DynamicClientProxy -> DiscoverySubscriber: needToReplace()
DiscoverySubscriber --> DynamicClientProxy: true
DynamicClientProxy -> DynamicClientProxy: replaceProxiedObject()
deactivate DynamicClientProxy
DynamicClientProxy --> SwitchingProxy: true
SwitchingProxy --> SwitchingProducer: true
note right of SwitchingProducer: (actual send happens here)
[<-- SwitchingProducer

```