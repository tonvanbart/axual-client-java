package io.axual.client.proxy.switching.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.generic.client.ClientProxy;
import io.axual.discovery.client.DiscoveryResult;

/**
 * Interface for client proxy switchers.
 * @param <T> type of client proxy handled by this switcher.
 * @param <C> configuration type for the proxy type.
 */
public interface ClientProxySwitcher<T extends ClientProxy, C extends BaseClientProxyConfig> {

    /**
     * Switch the proxy for one that is set up according to the new discovery result.
     * @param oldProxy the old proxy to be replaced.
     * @param config the configuration to use for the new proxy.
     * @param oldResult previous discovery result (i.e. before the switch).
     * @param newResult new discovery result.
     * @return the new proxy.
     * @see DiscoveryResult
     */
    T switchProxy(T oldProxy, C config, DiscoveryResult oldResult, DiscoveryResult newResult);
}
