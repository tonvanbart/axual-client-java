package io.axual.client.proxy.generic.registry;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.callback.admin.CallbackAdminConfig;
import io.axual.client.proxy.callback.admin.CallbackAdminFactory;
import io.axual.client.proxy.callback.consumer.CallbackConsumerConfig;
import io.axual.client.proxy.callback.consumer.CallbackConsumerFactory;
import io.axual.client.proxy.callback.serde.CallbackDeserializerConfig;
import io.axual.client.proxy.callback.serde.CallbackDeserializerFactory;
import io.axual.client.proxy.callback.producer.CallbackProducerConfig;
import io.axual.client.proxy.callback.producer.CallbackProducerFactory;
import io.axual.client.proxy.callback.serde.CallbackSerializerConfig;
import io.axual.client.proxy.callback.serde.CallbackSerializerFactory;
import io.axual.client.proxy.generic.proxy.ProxyFactory;
import io.axual.client.proxy.header.consumer.HeaderConsumerConfig;
import io.axual.client.proxy.header.consumer.HeaderConsumerFactory;
import io.axual.client.proxy.header.producer.HeaderProducerConfig;
import io.axual.client.proxy.header.producer.HeaderProducerFactory;
import io.axual.client.proxy.header.serde.HeaderDeserializerConfig;
import io.axual.client.proxy.header.serde.HeaderDeserializerFactory;
import io.axual.client.proxy.header.serde.HeaderSerializerConfig;
import io.axual.client.proxy.header.serde.HeaderSerializerFactory;
import io.axual.client.proxy.lineage.consumer.LineageConsumerConfig;
import io.axual.client.proxy.lineage.consumer.LineageConsumerFactory;
import io.axual.client.proxy.lineage.producer.LineageProducerConfig;
import io.axual.client.proxy.lineage.producer.LineageProducerFactory;
import io.axual.client.proxy.lineage.serde.LineageDeserializerConfig;
import io.axual.client.proxy.lineage.serde.LineageDeserializerFactory;
import io.axual.client.proxy.lineage.serde.LineageSerializerConfig;
import io.axual.client.proxy.lineage.serde.LineageSerializerFactory;
import io.axual.client.proxy.logging.admin.LoggingAdminConfig;
import io.axual.client.proxy.logging.admin.LoggingAdminFactory;
import io.axual.client.proxy.logging.consumer.LoggingConsumerConfig;
import io.axual.client.proxy.logging.consumer.LoggingConsumerFactory;
import io.axual.client.proxy.logging.producer.LoggingProducerConfig;
import io.axual.client.proxy.logging.producer.LoggingProducerFactory;
import io.axual.client.proxy.logging.serde.LoggingDeserializerConfig;
import io.axual.client.proxy.logging.serde.LoggingDeserializerFactory;
import io.axual.client.proxy.logging.serde.LoggingSerializerConfig;
import io.axual.client.proxy.logging.serde.LoggingSerializerFactory;
import io.axual.client.proxy.noop.admin.NoOpAdminConfig;
import io.axual.client.proxy.noop.admin.NoOpAdminFactory;
import io.axual.client.proxy.noop.serde.NoOpDeserializerConfig;
import io.axual.client.proxy.noop.serde.NoOpDeserializerFactory;
import io.axual.client.proxy.noop.serde.NoOpSerializerConfig;
import io.axual.client.proxy.noop.serde.NoOpSerializerFactory;
import io.axual.client.proxy.resolving.admin.ResolvingAdminConfig;
import io.axual.client.proxy.resolving.admin.ResolvingAdminFactory;
import io.axual.client.proxy.resolving.consumer.ResolvingConsumerConfig;
import io.axual.client.proxy.resolving.consumer.ResolvingConsumerFactory;
import io.axual.client.proxy.resolving.producer.ResolvingProducerConfig;
import io.axual.client.proxy.resolving.producer.ResolvingProducerFactory;
import io.axual.client.proxy.resolving.serde.ResolvingDeserializerConfig;
import io.axual.client.proxy.resolving.serde.ResolvingDeserializerFactory;
import io.axual.client.proxy.resolving.serde.ResolvingSerializerConfig;
import io.axual.client.proxy.resolving.serde.ResolvingSerializerFactory;
import io.axual.client.proxy.switching.admin.SwitchingAdminConfig;
import io.axual.client.proxy.switching.admin.SwitchingAdminFactory;
import io.axual.client.proxy.switching.consumer.SwitchingConsumerConfig;
import io.axual.client.proxy.switching.consumer.SwitchingConsumerFactory;
import io.axual.client.proxy.switching.producer.SwitchingProducerConfig;
import io.axual.client.proxy.switching.producer.SwitchingProducerFactory;

public class ProxyTypeRegistry {
    public enum ProxyType {
        ADMIN,
        CONSUMER,
        PRODUCER,
        DESERIALIZER,
        SERIALIZER
    }

    public static class ProxyTypeRegistration {
        private final String backingClassName;
        private final String backingPropertyName;

        private ProxyTypeRegistration(String backingClassName, String backingPropertyName) {
            this.backingPropertyName = backingPropertyName;
            this.backingClassName = backingClassName;
        }

        String getBackingClassName() {
            return backingClassName;
        }

        String getBackingPropertyName() {
            return backingPropertyName;
        }
    }

    private static final Map<ProxyType, Map<String, ProxyTypeRegistration>> proxyRegistration = new EnumMap<>(ProxyType.class);

    public static final String CALLBACK_PROXY_ID = "CALLBACK";
    public static final String HEADER_PROXY_ID = "HEADER";
    public static final String LINEAGE_PROXY_ID = "LINEAGE";
    public static final String LOGGING_PROXY_ID = "LOGGING";
    public static final String RESOLVING_PROXY_ID = "RESOLVING";
    public static final String SWITCHING_PROXY_ID = "SWITCHING";

    static {
        // Callback proxies
        registerProxyType(ProxyType.ADMIN, CALLBACK_PROXY_ID, CallbackAdminFactory.class, CallbackAdminConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.CONSUMER, CALLBACK_PROXY_ID, CallbackConsumerFactory.class, CallbackConsumerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.PRODUCER, CALLBACK_PROXY_ID, CallbackProducerFactory.class, CallbackProducerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.DESERIALIZER, CALLBACK_PROXY_ID, CallbackDeserializerFactory.class, CallbackDeserializerConfig.BACKING_DESERIALIZER_CONFIG);
        registerProxyType(ProxyType.SERIALIZER, CALLBACK_PROXY_ID, CallbackSerializerFactory.class, CallbackSerializerConfig.BACKING_SERIALIZER_CONFIG);

        // Header proxies
        registerProxyType(ProxyType.ADMIN, HEADER_PROXY_ID, NoOpAdminFactory.class, NoOpAdminConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.CONSUMER, HEADER_PROXY_ID, HeaderConsumerFactory.class, HeaderConsumerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.PRODUCER, HEADER_PROXY_ID, HeaderProducerFactory.class, HeaderProducerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.DESERIALIZER, HEADER_PROXY_ID, HeaderDeserializerFactory.class, HeaderDeserializerConfig.BACKING_DESERIALIZER_CONFIG);
        registerProxyType(ProxyType.SERIALIZER, HEADER_PROXY_ID, HeaderSerializerFactory.class, HeaderSerializerConfig.BACKING_SERIALIZER_CONFIG);

        // Lineage proxies
        registerProxyType(ProxyType.ADMIN, LINEAGE_PROXY_ID, NoOpAdminFactory.class, NoOpAdminConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.CONSUMER, LINEAGE_PROXY_ID, LineageConsumerFactory.class, LineageConsumerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.PRODUCER, LINEAGE_PROXY_ID, LineageProducerFactory.class, LineageProducerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.DESERIALIZER, LINEAGE_PROXY_ID, LineageDeserializerFactory.class, LineageDeserializerConfig.BACKING_DESERIALIZER_CONFIG);
        registerProxyType(ProxyType.SERIALIZER, LINEAGE_PROXY_ID, LineageSerializerFactory.class, LineageSerializerConfig.BACKING_SERIALIZER_CONFIG);

        // Logging proxies
        registerProxyType(ProxyType.ADMIN, LOGGING_PROXY_ID, LoggingAdminFactory.class, LoggingAdminConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.CONSUMER, LOGGING_PROXY_ID, LoggingConsumerFactory.class, LoggingConsumerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.PRODUCER, LOGGING_PROXY_ID, LoggingProducerFactory.class, LoggingProducerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.DESERIALIZER, LOGGING_PROXY_ID, LoggingDeserializerFactory.class, LoggingDeserializerConfig.BACKING_DESERIALIZER_CONFIG);
        registerProxyType(ProxyType.SERIALIZER, LOGGING_PROXY_ID, LoggingSerializerFactory.class, LoggingSerializerConfig.BACKING_SERIALIZER_CONFIG);

        // Resolving proxies
        registerProxyType(ProxyType.ADMIN, RESOLVING_PROXY_ID, ResolvingAdminFactory.class, ResolvingAdminConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.CONSUMER, RESOLVING_PROXY_ID, ResolvingConsumerFactory.class, ResolvingConsumerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.PRODUCER, RESOLVING_PROXY_ID, ResolvingProducerFactory.class, ResolvingProducerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.DESERIALIZER, RESOLVING_PROXY_ID, ResolvingDeserializerFactory.class, ResolvingDeserializerConfig.BACKING_DESERIALIZER_CONFIG);
        registerProxyType(ProxyType.SERIALIZER, RESOLVING_PROXY_ID, ResolvingSerializerFactory.class, ResolvingSerializerConfig.BACKING_SERIALIZER_CONFIG);

        // Switching proxies
        registerProxyType(ProxyType.ADMIN, SWITCHING_PROXY_ID, SwitchingAdminFactory.class, SwitchingAdminConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.CONSUMER, SWITCHING_PROXY_ID, SwitchingConsumerFactory.class, SwitchingConsumerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.PRODUCER, SWITCHING_PROXY_ID, SwitchingProducerFactory.class, SwitchingProducerConfig.BACKING_FACTORY_CONFIG);
        registerProxyType(ProxyType.DESERIALIZER, SWITCHING_PROXY_ID, NoOpDeserializerFactory.class, NoOpDeserializerConfig.BACKING_DESERIALIZER_CONFIG);
        registerProxyType(ProxyType.SERIALIZER, SWITCHING_PROXY_ID, NoOpSerializerFactory.class, NoOpSerializerConfig.BACKING_SERIALIZER_CONFIG);
    }

    private ProxyTypeRegistry() {
    }

    public static void registerProxyType(ProxyType type, String id, Class<? extends ProxyFactory> factoryClass, String backingFactoryPropertyName) {
        if (!proxyRegistration.containsKey(type)) {
            proxyRegistration.put(type, new HashMap<>());
        }
        proxyRegistration.get(type).put(id, new ProxyTypeRegistration(factoryClass.getName(), backingFactoryPropertyName));
    }

    static ProxyTypeRegistration getProxyRegistration(ProxyType type, String id) {
        if (proxyRegistration.containsKey(type)) {
            return proxyRegistration.get(type).get(id);
        }
        return null;
    }
}
