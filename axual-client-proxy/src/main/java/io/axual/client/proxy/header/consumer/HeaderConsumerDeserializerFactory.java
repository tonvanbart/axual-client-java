package io.axual.client.proxy.header.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.generic.serde.DeserializerProxy;
import io.axual.client.proxy.generic.serde.DeserializerProxyFactory;
import io.axual.client.proxy.wrapped.serde.WrappedDeserializer;
import io.axual.client.proxy.wrapped.serde.WrappedDeserializerConfig;

public class HeaderConsumerDeserializerFactory<T> implements DeserializerProxyFactory<T> {
    private final Object backingDeserializer;

    public HeaderConsumerDeserializerFactory(Object backingDeserializer) {
        this.backingDeserializer = backingDeserializer;
    }

    @Override
    public DeserializerProxy<T> create(Map<String, Object> configs, boolean isKey) {
        Map<String, Object> props = new HashMap<>(configs);
        props.put(WrappedDeserializerConfig.DESERIALIZER_CONFIG, backingDeserializer);
        DeserializerProxy<T> result = new WrappedDeserializer<>();
        result.configure(props, isKey);
        return result;
    }
}
