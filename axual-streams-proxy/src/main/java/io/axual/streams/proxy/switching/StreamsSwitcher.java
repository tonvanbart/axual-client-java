package io.axual.streams.proxy.switching;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.streams.StreamsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;
import java.util.Map;

import io.axual.client.proxy.switching.generic.BaseClientProxySwitcher;
import io.axual.common.annotation.InterfaceStability;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.streams.proxy.generic.factory.StreamsProxyFactory;
import io.axual.streams.proxy.generic.proxy.StreamsProxy;

@InterfaceStability.Evolving
public class StreamsSwitcher extends BaseClientProxySwitcher<StreamsProxy, SwitchingStreamsConfig> {

    private static final Logger LOG = LoggerFactory.getLogger(StreamsSwitcher.class);

    private static final String DEFAULT_TMP_DIR = System.getProperty("java.io.tmpdir");

    @Override
    @SuppressWarnings("unchecked")
    protected StreamsProxy createProxyObject(SwitchingStreamsConfig config, DiscoveryResult discoveryResult) {
        Map<String, Object> downstreamConfigs = config.getDownstreamConfigs();

        // Add all discovery properties
        downstreamConfigs.putAll(discoveryResult.getConfigs());

        // Cluster switch needs to guarantee clean context.
        final String stateDir =
                (String) config.getConfigs().getOrDefault(StreamsConfig.STATE_DIR_CONFIG, DEFAULT_TMP_DIR);
        downstreamConfigs.put(StreamsConfig.STATE_DIR_CONFIG, Paths.get(stateDir, discoveryResult.getCluster()).toString());

        LOG.info("Creating a new {} with properties: {}", config.getProxyType(), downstreamConfigs);
        StreamsProxyFactory factory = (StreamsProxyFactory) config.getBackingFactory();
        return factory.create(downstreamConfigs);
    }
}
