package io.axual.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.awaitility.Awaitility;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import io.axual.client.proxy.axual.consumer.AxualConsumer;
import io.axual.client.proxy.axual.consumer.AxualConsumerConfig;
import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.client.proxy.axual.producer.AxualProducerConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyChainElement;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.client.proxy.lineage.LineageHeaders;
import io.axual.client.proxy.lineage.core.LineageConfig;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.DefaultHandlerFactory;
import io.axual.streams.streams.StreamRunner;

import static io.axual.client.config.DeliveryStrategy.AT_LEAST_ONCE;
import static io.axual.serde.utils.HeaderUtils.addIntegerHeader;
import static io.axual.serde.utils.HeaderUtils.addUuidHeader;
import static io.axual.serde.utils.HeaderUtils.decodeIntegerHeader;
import static io.axual.serde.utils.HeaderUtils.decodeUuidHeader;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Will test that copy flag and message id headers will only be forwarded when explicitly authorized
 * Introduced because of ALTR-1837
 * WARNING, this may become absolute with future Kafka Streams version (>2.3.0) as this is caused by a Streams bug
 */
public class StreamsCopyHeaderLeakIT {
    private static final Logger LOG = LoggerFactory.getLogger(StreamsCopyHeaderLeakIT.class);
    public static final String APP_ID_VERIFIER = "copyheaderleakit-verifier";
    public static final String APP_ID_PRODUCER_STREAM = "copyheaderleakit-producer-stream";
    public static final String APP_ID_PRODUCER_TABLE = "copyheaderleakit-producer-table";
    public static final String APP_ID_STREAM_READ_MAP_WRITE = "copyheaderleakit-stream-read-map-write";
    public static final String APP_ID_STREAM_JOIN = "copyheaderleakit-stream-join";

    public static final String JOIN_STORE_NAME = "test-store-StreamsCopyHeaderLeakIT";
    public static final String CHANGELOG_ADDITION = "changelog";

    public static final String STREAM_NAME_INPUT_STREAM = "source-stream-StreamsCopyHeaderLeakIT";
    public static final String STREAM_NAME_INPUT_TABLE = "source-table-StreamsCopyHeaderLeakIT";
    public static final String STREAM_NAME_JOIN_TABLE = String.join("-", APP_ID_STREAM_JOIN, JOIN_STORE_NAME, CHANGELOG_ADDITION);

    public static final String STREAM_NAME_OUTPUT_STREAM = "target-stream-StreamsCopyHeaderLeakIT";

    public static final int PARTITIONS = 1;

    public static final int COPY_FLAG_STREAM = 0b00000001;
    public static final int COPY_FLAG_TABLE = 0b00000010;
    public static final Long TABLE_RECORD_VALUE_ADDITION = 100L;

    @Rule
    public SingleClusterPlatformUnit platformUnit = new SingleClusterPlatformUnit()
            .addStream(new StreamConfig()
                    .setName(STREAM_NAME_INPUT_STREAM).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(STREAM_NAME_INPUT_TABLE).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(STREAM_NAME_JOIN_TABLE).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(STREAM_NAME_OUTPUT_STREAM).setPartitions(PARTITIONS));

    public static final ProxyChain PROXY_CHAIN_VERIFIER = ProxyChain.newBuilder()
            .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
            .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
            .build();

    private <K, V> Thread createVerifierThread(String stream,
                                               ClientConfig clientConfig,
                                               AtomicBoolean keepRunnning,
                                               AtomicBoolean isRunning,
                                               LinkedList<ConsumerRecord<K, V>> consumedRecords,
                                               Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        isRunning.set(false);
        return new Thread(() -> {
            Thread.currentThread().setName(String.format("Verifier-%s", stream));
            LOG.info("Verifier stream {}: start, {} messages in list", stream, consumedRecords.size());

            Map<String, Object> consumerConfig = KafkaUtil.getKafkaConfigs(clientConfig);
            consumerConfig.put(AxualConsumerConfig.CHAIN_CONFIG, PROXY_CHAIN_VERIFIER);
            consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
            consumerConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
            consumerConfig.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, "false");

            try (AxualConsumer<K, V> consumer = new AxualConsumer<K, V>(consumerConfig, keyDeserializer, valueDeserializer)) {
                List<PartitionInfo> streamPartitions = consumer.partitionsFor(stream);
                List<TopicPartition> toAssign = streamPartitions.stream().map((a) -> new TopicPartition(a.topic(), a.partition())).collect(Collectors.toList());
                consumer.assign(toAssign);
                consumer.seekToEnd(toAssign);
                while (keepRunnning.get()) {
                    isRunning.set(true);
                    ConsumerRecords<K, V> records = consumer.poll(Duration.ofMillis(200));
                    for (ConsumerRecord<K, V> record : records) {
                        consumedRecords.add(record);
                    }
                    LOG.info("Verifier stream {}: Consumed {} new messages, total consumed {}", stream, records.count(), consumedRecords.size());
                    consumer.commitSync();
                }
            }

            isRunning.set(false);
            LOG.info("Verifier stream {}: end, consumed {} messages", stream, consumedRecords.size());
        });
    }

    @Test
    public void testReadMapWriteWithoutHeaderPropagation() throws Exception {
        // Start proxy consumer using PROXY_CHAIN_VERIFIER listening on STREAM_NAME_OUTPUT_WITHOUT_EXPLICIT_HEADER, store all Consumer Record Object received in LinkedList
        //
        // topology
        // 1 Create source stream from STREAM_NAME_INPUT_WITH_EXPLICIT_HEADER
        // 2 map to Key String, Value String
        // 3 Write to STREAM_NAME_OUTPUT_WITHOUT_EXPLICIT_HEADER
        // Start Streams App with app id APP_ID_STREAM_READ_MAP_WRITE_NO_PROP
        //
        // Start Producer for STREAM_NAME_INPUT_WITH_EXPLICIT_HEADER
        // Produce 10 messaged in iteration with payload: Key Long iter, Value Long iter, MessageId new UUID(iter,iter), CopyFlag COPY_FLAG_STREAM
        // Wait for at most 30 seconds to receive the messages with the consumer.
        // Verify that the received messages are not using these headers

        final String source = STREAM_NAME_INPUT_STREAM;
        final String target = STREAM_NAME_OUTPUT_STREAM;
        final String streamsAppId = APP_ID_STREAM_READ_MAP_WRITE;

        final LinkedList<ProducerRecord<Long, Long>> producerRecords = new LinkedList<>();
        final LinkedList<ConsumerRecord<String, String>> consumerRecords = new LinkedList<>();

        final ClientConfig verifierClientConfig = platformUnit.instance().getClientConfig(APP_ID_VERIFIER, false);
        final AtomicBoolean verifierKeepRunning = new AtomicBoolean(true);
        final AtomicBoolean verifierIsRunning = new AtomicBoolean(false);
        final Thread verifierThread = createVerifierThread(target, verifierClientConfig, verifierKeepRunning, verifierIsRunning, consumerRecords, new StringDeserializer(), new StringDeserializer());
        verifierThread.setDaemon(false);
        verifierThread.start();
        try {
            Awaitility.await("Wait for consumer to start")
                    .atMost(30, SECONDS)
                    .pollInterval(100, MILLISECONDS)
                    .until(() -> verifierIsRunning.get());

            final Map<String, Object> lineageConfig = new HashMap<>(2);
            lineageConfig.put(LineageConfig.ENABLE_SYSTEM_PRODUCE, false);
            final ProxyChain proxyChainProducer = ProxyChain.newBuilder()
                    .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
                    .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
                    .append(ProxyChainElement.newBuilder()
                            .setProxyId(ProxyTypeRegistry.LINEAGE_PROXY_ID)
                            .setConfigs(lineageConfig)
                            .build())
                    .build();

            final ClientConfig producerClientConfig = platformUnit.instance().getClientConfig(APP_ID_PRODUCER_STREAM, false);
            final Map<String, Object> producerConfig = KafkaUtil.getKafkaConfigs(producerClientConfig);
            producerConfig.put(AxualProducerConfig.CHAIN_CONFIG, proxyChainProducer);
            producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
            producerConfig.put(ProducerConfig.RETRIES_CONFIG, "0");
            try (Producer<Long, Long> producer = new AxualProducer<>(producerConfig, new LongSerializer(), new LongSerializer())) {
                final long timeout = System.currentTimeMillis() + 15000;// produce should not take longer than 15 seconds
                final AtomicLong counter = new AtomicLong(1);
                while (System.currentTimeMillis() < timeout && counter.get() <= 10) {
                    final Long count = counter.get();
                    ProducerRecord<Long, Long> producerRecord = new ProducerRecord<>(source, count, count);
                    addUuidHeader(producerRecord.headers(), LineageHeaders.MESSAGE_ID_HEADER, new UUID(count, count));
                    addIntegerHeader(producerRecord.headers(), LineageHeaders.COPY_FLAGS_HEADER, COPY_FLAG_STREAM);
                    Future<RecordMetadata> future = producer.send(producerRecord);
                    try {
                        future.get();
                        LOG.info("Producer {} - Produce of {} successful, continuing", source, count);
                        counter.incrementAndGet();
                        producerRecords.add(producerRecord);
                    } catch (InterruptedException | ExecutionException e) {
                        LOG.info("Producer {} - Produce of {} failed, retrying", source, count);
                    }
                }
                LOG.info("Producer {} - Ending produce loop with counter at {}", source, counter.get());
            }

            final ClientConfig streamsConfig = platformUnit.instance().getClientConfig(streamsAppId, false);

            try (AxualStreamsClient streamsClient = new AxualStreamsClient(streamsConfig)) {
                final TopologyFactory testTopology = builder -> {
                    builder
                            .stream(source, Consumed.with(Serdes.Long(), Serdes.Long()))
                            .peek((key, value) -> LOG.info("Stream {} - Incoming message: key: {} value: {}", source, key, value))
                            .map((key, value) -> new KeyValue<>(key.toString(), value.toString()))
                            .peek((key, value) -> LOG.info("Stream {} - Mapped to message: key: {} value: {}", source, key, value))
                            .to(target, Produced.with(Serdes.String(), Serdes.String()));
                    return builder.build();
                };

                // Prepare streamRunner
                final StreamRunnerConfig runnerConfig = StreamRunnerConfig.builder()
                        .setDeliveryStrategy(AT_LEAST_ONCE)
                        .setUncaughtExceptionHandler(new DefaultHandlerFactory())
                        .setDefaultKeySerde(Serdes.String())
                        .setDefaultValueSerde(Serdes.String())
                        .setTopologyFactory(testTopology)
                        .build();

                StreamRunner runner = streamsClient.buildStreamRunner(runnerConfig);
                runner.start();
                Awaitility.await("Wait for running state of Kafka Streams")
                        .atMost(15, SECONDS)
                        .pollInterval(100, MILLISECONDS)
                        .until(() -> runner.state() == KafkaStreams.State.RUNNING);
                LOG.info("Runner started");

                Awaitility.await("Wait for consumed messages to catch up to produced messages")
                        .atMost(30, SECONDS)
                        .pollInterval(100, MILLISECONDS)
                        .until(() -> consumerRecords.size() >= producerRecords.size());

                // Stop streams
                runner.stop();
            }
        } finally {
            verifierKeepRunning.set(false);
            verifierThread.join();
        }


        assertFalse("Produced records list should not be empty", producerRecords.isEmpty());
        assertFalse("Consumed records list should not be empty", consumerRecords.isEmpty());
        assertThat("The produced records list should contain the same or less messages than the consumed records list", producerRecords.size(), lessThanOrEqualTo(consumerRecords.size()));
        for (ProducerRecord<Long, Long> producerRecord : producerRecords) {
            // for each record, find the consumer record
            final Long producedValue = producerRecord.value();
            final String stringValue = producedValue.toString();

            Optional<ConsumerRecord<String, String>> foundCr = consumerRecords.stream().filter(cr -> stringValue.equals(cr.value())).findFirst();
            assertTrue(String.format("No consumed record for value %d could be found", producedValue), foundCr.isPresent());
            final Header consumedMessageId = foundCr.get().headers().lastHeader(LineageHeaders.MESSAGE_ID_HEADER);
            if (consumedMessageId != null) {
                final Header producedMessageId = producerRecord.headers().lastHeader(LineageHeaders.MESSAGE_ID_HEADER);
                UUID producedId = new UUID(producedValue, producedValue);
                UUID consumedId = decodeUuidHeader(consumedMessageId);
                assertNotEquals("Message ID should not be the same", producedId, consumedId);
            }

            final Header consumedCopyFlag = foundCr.get().headers().lastHeader(LineageHeaders.COPY_FLAGS_HEADER);
            if (consumedCopyFlag != null) {
                Integer notExpected = COPY_FLAG_STREAM;
                Integer copyFlag = decodeIntegerHeader(consumedCopyFlag);
                assertNotEquals("Copy Flag should not be the same", notExpected, copyFlag);
            }
        }
    }

    @Test
    public void testJoinStreamWithoutHeaderPropagation() throws Exception {
        // Start proxy consumer using PROXY_CHAIN_VERIFIER listening on STREAM_NAME_OUTPUT_WITHOUT_EXPLICIT_HEADER, store all Consumer Record Objects received in LinkedList
        //
        // Start Producer for STREAM_NAME_INPUT_TABLE_WITH_EXPLICIT_HEADER
        // Produce 10 berichten on STREAM_NAME_INPUT_TABLE_WITH_EXPLICIT_HEADER with Key Long iter, Value Long iter, MessageId new UUID(iter,iter), CopyFlag COPY_FLAG_STREAM

        // topology
        // 1 Stream STREAM_NAME_INPUT_WITH_EXPLICIT_HEADER
        // 2 Table STREAM_NAME_INPUT_TABLE_WITH_EXPLICIT_HEADER
        // 3 Join Streams, map rule (value_1 + value_2)
        // 4 Write to STREAM_NAME_OUTPUT_WITHOUT_EXPLICIT_HEADER
        // Start Streams App with app id APP_ID_STREAM_JOIN_NO_PROP
        //
        // Start Producer for STREAM_NAME_INPUT_WITH_EXPLICIT_HEADER
        // Produce 10 berichten on STREAM_NAME_INPUT_WITH_EXPLICIT_HEADER with Key Long iter, Value Long iter, MessageId new UUID(iter,iter), CopyFlag COPY_FLAG_TABLE
        // Wait for at most 30 seconds to receive the messages with the consumer and stop the Streams App.
        // Verify that the received messages are NOT using the message id and copy flag headers as the source stream or the join stream

        final String sourceStream = STREAM_NAME_INPUT_STREAM;
        final String sourceTable = STREAM_NAME_INPUT_TABLE;
        final String target = STREAM_NAME_OUTPUT_STREAM;
        final String streamsAppId = APP_ID_STREAM_JOIN;

        final LinkedList<ProducerRecord<Long, Long>> producedStreamRecords = new LinkedList<>();
        final LinkedList<ProducerRecord<Long, Long>> producedTableRecords = new LinkedList<>();
        final LinkedList<ConsumerRecord<String, String>> consumerRecords = new LinkedList<>();

        final ClientConfig verifierClientConfig = platformUnit.instance().getClientConfig(APP_ID_VERIFIER, false);
        final AtomicBoolean verifierKeepRunning = new AtomicBoolean(true);
        final AtomicBoolean verifierIsRunning = new AtomicBoolean(false);
        final Thread verifierThread = createVerifierThread(target, verifierClientConfig, verifierKeepRunning, verifierIsRunning, consumerRecords, new StringDeserializer(), new StringDeserializer());
        verifierThread.setDaemon(false);
        verifierThread.start();
        try {
            Awaitility.await("Wait for consumer to start")
                    .atMost(30, SECONDS)
                    .pollInterval(100, MILLISECONDS)
                    .until(() -> verifierIsRunning.get());

            final Map<String, Object> lineageConfig = new HashMap<>(2);
            lineageConfig.put(LineageConfig.ENABLE_SYSTEM_PRODUCE, true);

            final ProxyChain proxyChainProducer = ProxyChain.newBuilder()
                    .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
                    .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
                    .append(ProxyChainElement.newBuilder()
                            .setProxyId(ProxyTypeRegistry.LINEAGE_PROXY_ID)
                            .setConfigs(lineageConfig)
                            .build())
                    .build();

            final ClientConfig producerTableClientConfig = platformUnit.instance().getClientConfig(APP_ID_PRODUCER_TABLE, false);
            final Map<String, Object> producerTableConfig = KafkaUtil.getKafkaConfigs(producerTableClientConfig);
            producerTableConfig.put(AxualProducerConfig.CHAIN_CONFIG, proxyChainProducer);
            producerTableConfig.put(ProducerConfig.ACKS_CONFIG, "all");
            producerTableConfig.put(ProducerConfig.RETRIES_CONFIG, "0");
            try (Producer<Long, Long> producerTable = new AxualProducer<>(producerTableConfig, new LongSerializer(), new LongSerializer())) {
                final long timeout = System.currentTimeMillis() + 15000;// produce should not take longer than 15 seconds
                final AtomicLong counter = new AtomicLong(1);
                while (System.currentTimeMillis() < timeout && counter.get() <= 10) {
                    final Long count = counter.get();
                    final Long countValue = count + TABLE_RECORD_VALUE_ADDITION;
                    ProducerRecord<Long, Long> producerTableRecord = new ProducerRecord<>(sourceTable, count, countValue);
                    addUuidHeader(producerTableRecord.headers(), LineageHeaders.MESSAGE_ID_HEADER, new UUID(count, countValue));
                    addIntegerHeader(producerTableRecord.headers(), LineageHeaders.COPY_FLAGS_HEADER, COPY_FLAG_TABLE);
                    Future<RecordMetadata> future = producerTable.send(producerTableRecord);
                    try {
                        future.get();
                        LOG.info("ProducerTable {} - Produce of key {} value {} successful, continuing", sourceTable, count, countValue);
                        counter.incrementAndGet();
                        producedTableRecords.add(producerTableRecord);
                    } catch (InterruptedException | ExecutionException e) {
                        LOG.info("ProducerTable {} - Produce of key {} value {} failed, retrying", sourceTable, count, countValue);
                    }
                }
                LOG.info("ProducerTable {} - Ending produce loop with counter at {}", sourceTable, counter.get());
            }

            final ClientConfig streamsConfig = platformUnit.instance().getClientConfig(streamsAppId, false);

            try (AxualStreamsClient streamsClient = new AxualStreamsClient(streamsConfig)) {
                final TopologyFactory testTopology = builder -> {

                    Materialized<Long, Long, KeyValueStore<Bytes, byte[]>> materialized = Materialized.<Long, Long, KeyValueStore<Bytes, byte[]>>as(JOIN_STORE_NAME).withKeySerde(Serdes.Long()).withValueSerde(Serdes.Long());

                    KStream<Long, Long> inputStream = builder.stream(sourceStream, Consumed.with(Serdes.Long(), Serdes.Long()));
                    KTable<Long, Long> inputTable = builder.table(sourceTable, materialized);

                    inputStream
                            .peek((key, value) -> LOG.info("Stream {} - Incoming message: key: {} value: {}", sourceStream, key, value))
                            .join(inputTable, (v1, v2) -> v1 + v2)
                            .peek((key, value) -> LOG.info("Stream {} - Joined to: key: {} value: {}", sourceStream, key, value))
                            .map((key, value) -> new KeyValue<>(key.toString(), value.toString()))
                            .peek((key, value) -> LOG.info("Stream {} - Mapped to message: key: {} value: {}", sourceStream, key, value))
                            .to(target, Produced.with(Serdes.String(), Serdes.String()));
                    return builder.build();
                };

                // Prepare streamRunner
                final StreamRunnerConfig runnerConfig = StreamRunnerConfig.builder()
                        .setDeliveryStrategy(AT_LEAST_ONCE)
                        .setUncaughtExceptionHandler(new DefaultHandlerFactory())
                        .setDefaultKeySerde(Serdes.String())
                        .setDefaultValueSerde(Serdes.String())
                        .setTopologyFactory(testTopology)
                        .build();

                StreamRunner runner = streamsClient.buildStreamRunner(runnerConfig);
                runner.start();
                Awaitility.await("Wait for running state of Kafka Streams")
                        .atMost(15, SECONDS)
                        .pollInterval(100, MILLISECONDS)
                        .until(() -> runner.state() == KafkaStreams.State.RUNNING);
                LOG.info("Runner started");


                final ClientConfig producerStreamClientConfig = platformUnit.instance().getClientConfig(APP_ID_PRODUCER_STREAM, false);
                final Map<String, Object> producerStreamConfig = KafkaUtil.getKafkaConfigs(producerStreamClientConfig);
                producerStreamConfig.put(AxualProducerConfig.CHAIN_CONFIG, proxyChainProducer);
                producerStreamConfig.put(ProducerConfig.ACKS_CONFIG, "all");
                producerStreamConfig.put(ProducerConfig.RETRIES_CONFIG, "0");
                try (Producer<Long, Long> producerStream = new AxualProducer<>(producerStreamConfig, new LongSerializer(), new LongSerializer())) {
                    final long timeout = System.currentTimeMillis() + 15000;// produce should not take longer than 15 seconds
                    final AtomicLong counter = new AtomicLong(1);
                    while (System.currentTimeMillis() < timeout && counter.get() <= 10) {
                        final Long count = counter.get();
                        ProducerRecord<Long, Long> producerStreamRecord = new ProducerRecord<>(sourceStream, count, count);
                        addUuidHeader(producerStreamRecord.headers(), LineageHeaders.MESSAGE_ID_HEADER, new UUID(count, count));
                        addIntegerHeader(producerStreamRecord.headers(), LineageHeaders.COPY_FLAGS_HEADER, COPY_FLAG_STREAM);
                        Future<RecordMetadata> future = producerStream.send(producerStreamRecord);
                        try {
                            future.get();
                            LOG.info("Producer {} - Produce of {} successful, continuing", sourceStream, count);
                            counter.incrementAndGet();
                            producedStreamRecords.add(producerStreamRecord);
                        } catch (InterruptedException | ExecutionException e) {
                            LOG.info("Producer {} - Produce of {} failed, retrying", sourceStream, count);
                        }
                    }
                    LOG.info("Producer {} - Ending produce loop with counter at {}", sourceStream, counter.get());
                }

                Awaitility.await("Wait for consumed messages to catch up to produced messages")
                        .atMost(30, SECONDS)
                        .pollInterval(100, MILLISECONDS)
                        .until(() -> consumerRecords.size() >= producedStreamRecords.size());

                // Stop streams
                runner.stop();
            }
        } finally {
            verifierKeepRunning.set(false);
            verifierThread.join();
        }


        assertFalse("Produced records list should not be empty", producedStreamRecords.isEmpty());
        assertFalse("Consumed records list should not be empty", consumerRecords.isEmpty());
        assertThat("The produced records list should contain the same or less messages than the consumed records list", producedStreamRecords.size(), lessThanOrEqualTo(consumerRecords.size()));
        for (ProducerRecord<Long, Long> producerRecord : producedStreamRecords) {
            // for each record, find the consumer record
            final Long producedKey = producerRecord.key();
            final String stringKey = producedKey.toString();

            Optional<ProducerRecord<Long, Long>> foundTr = producedTableRecords.stream().filter(tr -> producedKey.equals(tr.key())).findFirst();
            assertTrue(String.format("No table record for value %d could be found", producedKey), foundTr.isPresent());

            Optional<ConsumerRecord<String, String>> foundCr = consumerRecords.stream().filter(cr -> stringKey.equals(cr.key())).findFirst();
            assertTrue(String.format("No consumed record for value %d could be found", producedKey), foundCr.isPresent());
            final Header consumedMessageId = foundCr.get().headers().lastHeader(LineageHeaders.MESSAGE_ID_HEADER);
            if (consumedMessageId != null) {
                UUID producedStreamId = new UUID(producedKey, producedKey);
                UUID producedTableId = new UUID(foundTr.get().key(), foundTr.get().value());
                UUID consumedId = decodeUuidHeader(consumedMessageId);
                assertNotEquals("Message ID should not be the same as the record on input stream", producedStreamId, consumedId);
                assertNotEquals("Message ID should not be the same as the record on input table", producedTableId, consumedId);
            }

            final Header consumedCopyFlag = foundCr.get().headers().lastHeader(LineageHeaders.COPY_FLAGS_HEADER);
            if (consumedCopyFlag != null) {
                Integer notExpectedStream = COPY_FLAG_STREAM;
                Integer notExpectedTable = COPY_FLAG_TABLE;
                Integer copyFlag = decodeIntegerHeader(consumedCopyFlag);
                assertNotEquals("Copy Flag should not be the same as the record on input stream", notExpectedStream, copyFlag);
                assertNotEquals("Copy Flag should not be the same as the record on input table", notExpectedTable, copyFlag);
            }
        }

    }
}
