package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.serialization.Deserializer;

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.exception.ClientException;
import io.axual.serde.avro.SpecificAvroDeserializer;

/**
 * Use this ConsumerConfig to consume specific, or typed, Avro objects from a stream.
 *
 * @param <K> The specific Avro Type which will be used as a key.
 * @param <V> The specific Avro Type which will be used as a value.
 */
@InterfaceStability.Evolving
public class SpecificAvroConsumerConfig<K extends SpecificRecord, V extends SpecificRecord> extends BaseAvroConsumerConfig<K, V> {
    private SpecificAvroConsumerConfig(Builder<K, V, ?> builder) {
        super(builder);
    }

    /**
     * The Builder can be used to set the configuration options, except for the deserializers.
     *
     * @param <K> The specific Avro Type which will be used as a key.
     * @param <V> The specific Avro Type which will be used as a value.
     * @return a new instance of builder
     */
    public static <K extends SpecificRecord, V extends SpecificRecord> Builder<K, V, ?> builder() {
        return new Builder<>();
    }

    /**
     * The Builder is used to set the configuration options.
     *
     * @param <K> The specific Avro Type which will be used as a key.
     * @param <V> The specific Avro Type which will be used as a value.
     * @param <T> the type of the Builder that is to be used.
     */
    public static class Builder<K extends SpecificRecord, V extends SpecificRecord, T extends Builder<K, V, T>> extends BaseAvroConsumerConfig.Builder<K, V, T> {
        public Builder() {
            super.setKeyDeserializer(SpecificAvroDeserializer.class.getName());
            super.setValueDeserializer(SpecificAvroDeserializer.class.getName());
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param keyDeserializerClassName The configured key deserializer class name
         * @return the {@link SpecificAvroConsumerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setKeyDeserializer(String keyDeserializerClassName) {
            throw new ClientException("Not allowed to override key deserializer class");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param keyDeserializer The configured key deserializer
         * @return the {@link SpecificAvroConsumerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setKeyDeserializer(Deserializer<K> keyDeserializer) {
            throw new ClientException("Not allowed to override key deserializer class");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param valueDeserializerClassName The configured value deserializer class name
         * @return the {@link SpecificAvroConsumerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setValueDeserializer(String valueDeserializerClassName) {
            throw new ClientException("Not allowed to override value deserializer class");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param valueDeserializer The configured value deserializer
         * @return the {@link SpecificAvroConsumerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setValueDeserializer(Deserializer<V> valueDeserializer) {
            throw new ClientException("Not allowed to override value deserializer class");
        }

        @Override
        public SpecificAvroConsumerConfig<K, V> build() {
            validate();
            return new SpecificAvroConsumerConfig<>(this);
        }
    }
}
