package io.axual.client.proxy.header.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import io.axual.client.proxy.test.TestConsumerProxy;
import io.axual.client.proxy.test.TestConsumerProxyFactory;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.discovery.client.DiscoveryClientRegistry;

import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({HeaderConsumer.class, TestConsumerProxyFactory.class, TestConsumerProxy.class, DiscoveryClientRegistry.class})
@PowerMockIgnore({"javax.net.ssl.*","jdk.internal.reflect.*"})
public class HeaderConsumerTest {
    private static final String APPLICATION_ID = "io.axual.test";
    private static final String APPLICATION_VERSION = "1.0";
    private static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
    private static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
    private static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");

    @Mock
    private TestConsumerProxy<String, String> mockedConsumer1, mockedConsumer2, mockedConsumer3;

    private Properties getValidProperties() {
        Properties consumerProps = new Properties();
        consumerProps.putAll(getValidMap());
        return consumerProps;
    }

    private Map<String, Object> getValidMap() {
        Map<String, Object> consumerMap = new HashMap<>();

        consumerMap.put(HeaderConsumerConfig.BACKING_FACTORY_CONFIG, TestConsumerProxyFactory.class.getName());
        consumerMap.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
        consumerMap.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);

        consumerMap.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "bootstrap1");
        consumerMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerMap.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "100");
        consumerMap.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerMap.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        SslConfig sslConfig = SslConfig.newBuilder()
                .setKeyPassword(SSL_PASSWORD)
                .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                .setKeystorePassword(SSL_PASSWORD)
                .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                .setTruststorePassword(SSL_PASSWORD)
                .setEnableHostnameVerification(false)
                .build();
        KafkaUtil.getKafkaConfigs(sslConfig, consumerMap);

        return consumerMap;
    }

    // This method is called by tests to validate different constructors
    private void validConstructorTest(final HeaderConsumer consumer) {
        final HeaderConsumerConfig config = (HeaderConsumerConfig) Whitebox.getInternalState(consumer, "config");

        Assert.assertEquals(SSL_KEYSTORE_LOCATION, config.getSslConfig().getKeystoreLocation());
        Assert.assertEquals(SSL_TRUSTSTORE_LOCATION, config.getSslConfig().getTruststoreLocation());
        Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeystorePassword());
        Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getTruststorePassword());
        Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeyPassword());

        // Verify initialisation of Apache KafkaProducer with mocked properties from the mocked provider
        verifyNew(TestConsumerProxy.class, Mockito.times(1));
    }

    @Before
    public void setUp() throws Exception {
        whenNew(TestConsumerProxy.class).withAnyArguments().thenReturn(mockedConsumer1, mockedConsumer2, mockedConsumer3);
    }

    @Test
    public void construction_ValidProperties() {
        HeaderConsumer<String, String> consumer = spy(new HeaderConsumer<String, String>(getValidProperties()));
        validConstructorTest(consumer);
    }

    @Test
    public void construction_ValidMap() {
        HeaderConsumer<String, String> consumer = spy(new HeaderConsumer<String, String>(getValidMap()));
        validConstructorTest(consumer);
    }
}
