package io.axual.client.proxy.axual;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.kafka.clients.consumer.ConsumerPartitionAssignor;
import org.apache.kafka.clients.consumer.RangeAssignor;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.PartitionInfo;
import org.junit.Test;


public class RangeAssignorTest {

  private void addPartitions(Set<PartitionInfo> partitions, String topic, int count) {
    for (int i = 0; i < count; i++) {
      partitions.add(new PartitionInfo(topic, i, null, null, null));
    }
  }

  @Test
  public void testTwoConsumersTwoTopics() {
    final Set<PartitionInfo> partitions = new HashSet<>();
    addPartitions(partitions, "topicA", 12);
    addPartitions(partitions, "topicB", 12);
    final Cluster cluster = new Cluster("MyCluster", new HashSet<>(), partitions, new HashSet<>(),
        new HashSet<>());

    Map<String, ConsumerPartitionAssignor.Subscription> subscriptionMap = new HashMap<>();
    subscriptionMap
        .put("consumer1", new ConsumerPartitionAssignor.Subscription(singletonList("topicA")));
    subscriptionMap
        .put("consumer2", new ConsumerPartitionAssignor.Subscription(singletonList("topicB")));
    RangeAssignor assignor = new RangeAssignor();

    ConsumerPartitionAssignor.GroupSubscription subscriptions = new ConsumerPartitionAssignor.GroupSubscription(
        subscriptionMap
    );

    // Perform the partition assignment
    final ConsumerPartitionAssignor.GroupAssignment assignments = assignor
        .assign(cluster, subscriptions);

    assertEquals(12, assignments.groupAssignment().get("consumer1").partitions().size());
    for (int i = 0; i < 12; i++) {
      assertEquals("topicA",
          assignments.groupAssignment().get("consumer1").partitions().get(i).topic());
    }
    assertEquals(12, assignments.groupAssignment().get("consumer2").partitions().size());
    for (int i = 0; i < 12; i++) {
      assertEquals("topicB",
          assignments.groupAssignment().get("consumer2").partitions().get(i).topic());
    }
  }

}
