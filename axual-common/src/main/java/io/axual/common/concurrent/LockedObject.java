package io.axual.common.concurrent;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockedObject<T> {
    private T object;

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public LockedObject(T object) {
        this.object = object;
    }

    public ReadLock getReadLock() {
        return new ReadLock();
    }

    public WriteLock getWriteLock() {
        return new WriteLock();
    }

    // The ReadLock exposes the protected object through a public immutable variable
    public class ReadLock implements AutoCloseable {
        public final T object;

        private ReadLock() {
            lock.readLock().lock();
            this.object = LockedObject.this.object;
        }

        @Override
        public void close() {
            lock.readLock().unlock();
        }
    }

    // The WriteLock exposes the protected object through a public variable. It can be overwritten
    // by the code that captures the write lock. When the lock is released, the object is compared
    // to the previous object. If changed, we overwrite the main object held.
    public class WriteLock implements AutoCloseable {
        private final T oldObject;
        private T object;

        private WriteLock() {
            lock.writeLock().lock();
            this.oldObject = LockedObject.this.object;
            this.object = LockedObject.this.object;
        }

        @Override
        public void close() {
            if (this.object != oldObject) {
                LockedObject.this.object = object;
            }

            lock.writeLock().unlock();
        }

        public T getObject(){
            return this.object;
        }
        public void setObject(T object){
            this.object = object;
        }
    }
}
