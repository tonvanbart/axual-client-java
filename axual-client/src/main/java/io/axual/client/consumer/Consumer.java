package io.axual.client.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.concurrent.Future;

import io.axual.client.config.BaseConsumerConfig;
import io.axual.client.exception.ConsumeFailedException;

/**
 * Common interface used for all Axual Client Consumers, exposing only what is needed to
 * start/stop/end consuming.
 *
 * @param <K> the type parameter
 * @param <V> the type parameter
 */
public interface Consumer<K, V> extends AutoCloseable {
    /**
     * Get the configuration for this consumer
     *
     * @return the configuration of this consumer
     */
    BaseConsumerConfig<K, V> getConfig();

    /**
     * Start the consuming process
     *
     * @return a boolean indicating whether the consuming was successfully started
     */
    Future<ConsumeFailedException> startConsuming();

    /**
     * Stops consuming and disconnects
     *
     * @return null if the consumer closed correctly, or an exception to throw in the calling thread
     * otherwise
     */
    ConsumeFailedException stopConsuming();

    /**
     * Indicated whether the consumer is still consuming, might return ConsumeFailedException if no
     * longer consuming because of an error
     *
     * @return true if the consumer is currently consuming, false otherwise
     */
    boolean isConsuming();

    /**
     * Returns the result of the background consume task. May block until task/thread completes.
     *
     * @return a result in the form of an exception, or null if no error
     */
    ConsumeFailedException getConsumeResult();

    /**
     * Closes the consumer. Throws any exception returned by stopConsuming() if applicable.
     */
    @Override
    void close();
}
