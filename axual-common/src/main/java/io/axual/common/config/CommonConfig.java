package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.annotation.InterfaceStability;

/**
 * This class holds generic constants used throughout the Axual client libraries.
 */
@InterfaceStability.Evolving
public class CommonConfig {
    private CommonConfig() {
    }

    /**
     * The constant APPLICATION_ID.
     */
    public static final String APPLICATION_ID = "app.id";
    /**
     * The constant APPLICATION_VERSION.
     */
    public static final String APPLICATION_VERSION = "app.version";
    /**
     * The constant ENDPOINT.
     */
    public static final String ENDPOINT = "endpoint";
    /**
     * The constant TENANT.
     */
    public static final String TENANT = "tenant";
    /**
     * The constant INSTANCE.
     */
    public static final String INSTANCE = "instance";
    /**
     * The constant ENVIRONMENT.
     */
    public static final String ENVIRONMENT = "environment";
    /**
     * The constant SYSTEM_PROPERTY.
     */
    public static final String SYSTEM_PROPERTY = "system";
    /**
     * The constant CLUSTER_PROPERTY.
     */
    public static final String CLUSTER_PROPERTY = "cluster";
}
