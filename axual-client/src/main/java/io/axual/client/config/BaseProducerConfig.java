package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Serializer;

import io.axual.client.AxualClient;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.common.annotation.InterfaceStability;

/**
 * This is the base for all AxualClient Producer configurations. It contains the settings all
 * producers have in common. Extend this class when defining a new Producer type. A Builder pattern
 * is used for initialization.
 *
 * @param <K> the type of the key that will be produced. A compatible key serializer must be
 *            provided to serialize the objects
 * @param <V> the type of the value that will be produced. A compatible value serializer must be
 *            provided to serialize the objects
 */
@InterfaceStability.Evolving
public class BaseProducerConfig<K, V> {
    public static final ProxyChain DEFAULT_PROXY_CHAIN = ProxyChain.newBuilder()
            .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
            .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
            .append(ProxyTypeRegistry.LINEAGE_PROXY_ID)
            .append(ProxyTypeRegistry.HEADER_PROXY_ID)
            .build();
    public static final long DEFAULT_LINGER_MS = 30;
    public static final int DEFAULT_BATCH_SIZE = 524288;

    private final DeliveryStrategy deliveryStrategy;
    private final OrderingStrategy orderingStrategy;
    private final boolean blocking;
    private final int messageBufferSize;
    private final int messageBufferWaitTimeout;
    private final Object keySerializer;
    private final Object valueSerializer;
    private final int batchSize;
    private final long lingerMs;
    private final ProxyChain proxyChain;

    /**
     * Instantiates a new Base producer config.
     *
     * @param builder the builder containing the configuration values
     */
    protected BaseProducerConfig(Builder<K, V, ?> builder) {
        deliveryStrategy = builder.deliveryStrategy;
        orderingStrategy = builder.orderingStrategy;
        blocking = builder.blocking;
        messageBufferSize = builder.messageBufferSize;
        messageBufferWaitTimeout = builder.messageBufferWaitTimeout;
        keySerializer = builder.keySerializer;
        valueSerializer = builder.valueSerializer;
        batchSize = builder.batchSize;
        lingerMs = builder.lingerMs;
        proxyChain = builder.proxyChain;
    }

    /**
     * Returns the delivery strategy the producer should implement. See {@link DeliveryStrategy} for
     * the available strategies.
     *
     * @return the delivery strategy configured in this object
     */
    public DeliveryStrategy getDeliveryStrategy() {
        return deliveryStrategy;
    }

    /**
     * Returns the ordering strategy the producer should implement. See {@link OrderingStrategy} for
     * the available strategies.
     *
     * @return the ordering strategy configured in this object
     */
    public OrderingStrategy getOrderingStrategy() {
        return orderingStrategy;
    }

    /**
     * This indicates if the produce call is blocked when the messageBuffer is full
     *
     * @return true if blocking
     */
    public boolean isBlocking() {
        return blocking;
    }

    /**
     * Returns the maximum size of the message buffer used by the worker.
     *
     * @return the maximum size of the message buffer
     */
    public int getMessageBufferSize() {
        return messageBufferSize;
    }

    /**
     * Returns the maximum amount of time in milliseconds to wait for room in the message buffer.
     *
     * @return the message buffer wait timeout
     */
    public int getMessageBufferWaitTimeout() {
        return messageBufferWaitTimeout;
    }

    /**
     * Returns the configured serializer used for key serialization. The class must implement {@link
     * Serializer}
     *
     * @return the the name of the class used for key serialization
     */
    public Object getKeySerializer() {
        return keySerializer;
    }

    /**
     * Returns the configured serializer used for value serialization. The class must implement
     * {@link Serializer}
     *
     * @return the the name of the class used for value serialization
     */
    public Object getValueSerializer() {
        return valueSerializer;
    }

    /**
     * Returns the maximum size in bytes of the batch of messages to produce before sending. When
     * the batch size has been reached, or when the linger time has been exceeded, the messages will
     * be send. The linger time {@link #getLingerMs()} also plays a role in the batching of
     * messages
     *
     * @return the maximum batch size
     */
    public Integer getBatchSize() {
        return batchSize;
    }

    /**
     * Returns the time to wait after a batch has been created before sending. When the batch size
     * has been reached, or when the linger time has been exceeded, the messages will be send. The
     * linger time {@link #getBatchSize()} also plays a role in the batching of messages
     *
     * @return the time to wait before sending the batch
     */
    public long getLingerMs() {
        return lingerMs;
    }

    /**
     * Returns the chain of producer proxies this producer uses.
     *
     * @return the proxy chain used to configure this producer.
     */
    public ProxyChain getProxyChain() {
        return proxyChain;
    }

    /**
     * The Builder is used to set the configuration options.
     *
     * @param <K> the type of the key that will be produced. A compatible key serializer must be
     *            provided to serialize the objects
     * @param <V> the type of the value that will be produced. A compatible value serializer must be
     *            provided to serialize the objects
     * @param <T> the type of the Builder that is to be used.
     */
    public static class Builder<K, V, T extends Builder<K, V, T>> {
        protected DeliveryStrategy deliveryStrategy;
        protected OrderingStrategy orderingStrategy = OrderingStrategy.LOSING_ORDER;
        protected Boolean blocking;
        protected Integer messageBufferSize;
        protected int messageBufferWaitTimeout = 0;
        protected Object keySerializer;
        protected Object valueSerializer;
        protected int batchSize = DEFAULT_BATCH_SIZE;
        protected long lingerMs = DEFAULT_LINGER_MS;
        protected ProxyChain proxyChain = DEFAULT_PROXY_CHAIN;

        /**
         * Sets the delivery strategy to be used by the producer. See {@link DeliveryStrategy} for
         * the available strategies.
         *
         * @param deliveryStrategy the strategy to use for the producer
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setDeliveryStrategy(DeliveryStrategy deliveryStrategy) {
            this.deliveryStrategy = deliveryStrategy;
            return (T) this;
        }

        /**
         * Sets the ordering strategy to be used by the producer. See {@link OrderingStrategy} for
         * the available strategies.
         *
         * @param orderingStrategy the strategy to use for the producer
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setOrderingStrategy(OrderingStrategy orderingStrategy) {
            this.orderingStrategy = orderingStrategy;
            return (T) this;
        }

        /**
         * Indicate if the produce will block when the messageBuffer is full. If set to true, the
         * produce will wait until a spot in the buffer becomes available.
         *
         * @param blocking the blocking
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setBlocking(boolean blocking) {
            this.blocking = blocking;
            return (T) this;
        }

        /**
         * The size of the message buffer. This indicates how many messages can be in-flight
         * simultaneously
         *
         * @param messageBufferSize the message buffer size
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setMessageBufferSize(int messageBufferSize) {
            this.messageBufferSize = messageBufferSize;
            return (T) this;
        }

        /**
         * Set the amount of time to wait for the message buffer before dropping the message
         *
         * @param messageBufferWaitTimeout amount of time to wait in milliseconds
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setMessageBufferWaitTimeout(int messageBufferWaitTimeout) {
            this.messageBufferWaitTimeout = messageBufferWaitTimeout;
            return (T) this;
        }

        /**
         * The full name of the class implementing {@link Serializer} which will accept the key
         * objects as input
         *
         * @param keySerializerClassName The configured key serializer the producer should use
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setKeySerializer(String keySerializerClassName) {
            keySerializer = keySerializerClassName;
            return (T) this;
        }

        /**
         * The configured class or instance implementing {@link Serializer} which will accept the
         * key objects as input
         *
         * @param keySerializer The configured key serializer the producer should use
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setKeySerializer(Serializer<K> keySerializer) {
            this.keySerializer = keySerializer;
            return (T) this;
        }

        /**
         * The full name of the class implementing {@link Serializer} which will accept the value
         * objects as input
         *
         * @param valueSerializerClassName The configured value serializer the producer should use
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setValueSerializer(String valueSerializerClassName) {
            valueSerializer = valueSerializerClassName;
            return (T) this;
        }

        /**
         * The configured class or instance implementing {@link Serializer} which will accept the
         * value objects as input
         *
         * @param valueSerializer The configured value serializer the producer should use
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setValueSerializer(Serializer<V> valueSerializer) {
            this.valueSerializer = valueSerializer;
            return (T) this;
        }

        /**
         * Sets the maximum size in bytes of the batch of messages to produce before sending. When
         * the batch size has been reached, or when the linger time has been exceeded, the messages
         * will be send. The linger time {@link #setLingerMs(long)} also plays a role in the
         * batching of messages
         *
         * @param batchSize The batch size in bytes
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setBatchSize(int batchSize) {
            this.batchSize = batchSize;
            return (T) this;
        }

        /**
         * Sets the time to wait in milliseconds before sending a created batch. When the batch size
         * has been reached, or when the linger time has been exceeded, the messages will be send.
         * The linger time also plays a role in the batching of
         * messages.
         *
         * @param lingerMs The amount of milliseconds to wait to send a batch
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setLingerMs(long lingerMs) {
            this.lingerMs = lingerMs;
            return (T) this;
        }

        /**
         * Configures the proxy chain to initialize for this producer.
         *
         * @param proxyChain The chain of producer proxies to set up.
         * @return the {@link Builder} object to be used for further configuration
         */
        public T setProxyChain(ProxyChain proxyChain) {
            this.proxyChain = proxyChain;
            return (T) this;
        }

        /**
         * Validate the variables set in the config object.
         *
         * @throws IllegalStateException when one of more variables are not set (correctly)
         */
        public void validate() {
            if (deliveryStrategy == null) {
                throw new IllegalStateException("Error building configuration: DeliveryStrategy is missing");
            }
            if (blocking == null) {
                blocking = true;
            }
            if (messageBufferSize == null) {
                messageBufferSize = 1000;
            }
            if (keySerializer == null || ((keySerializer instanceof String) && ((String) keySerializer).isEmpty())) {
                throw new IllegalStateException("Error building producer configuration: keySerializer is missing");
            }
            if (valueSerializer == null || ((valueSerializer instanceof String) && ((String) valueSerializer).isEmpty())) {
                throw new IllegalStateException("Error building producer configuration: valueSerializer is missing");
            }
        }

        /**
         * Builds the {@link BaseProducerConfig} object which is used by the {@link AxualClient} to
         * configure a producer
         *
         * @return a {@link BaseProducerConfig} object
         */
        public BaseProducerConfig<K, V> build() {
            validate();
            return new BaseProducerConfig<>(this);
        }
    }
}
