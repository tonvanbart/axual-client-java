package io.axual.client.proxy.generic.registry;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ProxyChainTest {
    @Test
    public void testToString() {
        Map<String, Object> configs = new HashMap<>();
        configs.put("key1", "value1");
        configs.put("key2", "value2");
        ProxyChain chain = ProxyChain.newBuilder()
                .append("dummy1")
                .append("dummy2", configs)
                .build();
        String chainStr = chain.toString();
        assertEquals("dummy1;dummy2:key1=value1,key2=value2", chainStr);
    }

    @Test
    public void testFromString() {
        ProxyChain chain = ProxyChain.parse("dummy1;dummy2:key1=value1,key2=value2");
        List<ProxyChainElement> elements = chain.getElements();

        assertEquals(2, elements.size());

        assertEquals("dummy1", elements.get(0).getProxyId());
        assertEquals(0, elements.get(0).getConfigs().size());

        assertEquals("dummy2", elements.get(1).getProxyId());
        assertEquals(2, elements.get(1).getConfigs().size());
        assertEquals("value1", elements.get(1).getConfigs().get("key1"));
        assertEquals("value2", elements.get(1).getConfigs().get("key2"));
    }

    @Test
    public void testContainsElement() {
        ProxyChain chain = ProxyChain.parse("dummy1;dummy2:key1=value1,key2=value2");

        assertTrue(chain.containsElement("dummy1"));
        assertFalse(chain.containsElement("dumb"));
    }

    @Test
    public void testBuilderRemove() {
        Map<String, Object> configs = new HashMap<>();
        configs.put("key1", "value1");
        configs.put("key2", "value2");
        ProxyChain chain = ProxyChain.newBuilder()
                .append("dummy1")
                .append("dummy2", configs)
                .remove("dummy1")
                .build();

        assertFalse(chain.containsElement("dummy1"));
    }

    @Test
    public void testBuilderFromChain() {
        Map<String, Object> configs = new HashMap<>();
        configs.put("key1", "value1");
        configs.put("key2", "value2");
        ProxyChain chain = ProxyChain.newBuilder()
                .append("dummy1")
                .append("dummy2", configs)
                .remove("dummy1")
                .build();
        ProxyChain clone = ProxyChain.newBuilder(chain).build();

        assertEquals(chain.getElements().size(), clone.getElements().size());
        for (ProxyChainElement element : chain.getElements()) {
            assertTrue(clone.containsElement(element.getProxyId()));
        }
    }
}
