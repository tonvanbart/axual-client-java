package io.axual.client.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.awaitility.Awaitility;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.test.Random;
import io.axual.common.tools.SleepUtil;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.DualClusterPlatformUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

public class ConsumerSwitchIT {
    private static final Logger LOG = LoggerFactory.getLogger(ConsumerSwitchIT.class);
    private static final String STREAM = "general-random-ConsumerSwitchIT";
    private static final long TTL = 5000; // five seconds
    private static final long DISTRIBUTOR_TIMEOUT = TTL;
    private static final long DISTRIBUTOR_DISTANCE = 1L;

    private static final long longerThanTTL = Math.round(Math.max(DISTRIBUTOR_TIMEOUT * DISTRIBUTOR_DISTANCE, TTL) * 1.5);
    private static final long shorterThanTTL = Math.round(TTL * 0.5);

    @Rule
    public DualClusterPlatformUnit platform = new DualClusterPlatformUnit(false)
            .addStream(new StreamConfig()
                    .setName(STREAM)
                    .setKeySchema(Random.getClassSchema())
                    .setValueSchema(Random.getClassSchema())
                    .setPartitions(1));

    private static Random createRandom(final int id) {
        return Random.newBuilder().setRandom(Integer.toString(id)).build();
    }

    @Before
    public void prepare() {
        platform.instance().getDiscoveryUnit().setTtl(TTL);
        platform.instance().getDiscoveryUnit().setDistributorTimeout(DISTRIBUTOR_TIMEOUT);
        platform.instance().getDiscoveryUnit().setDistributorDistance(DISTRIBUTOR_DISTANCE);
    }

    private List<Random> produce(int start, int count, Producer<Random, Random> producer) {
        List<Random> result = new ArrayList<>(count);
        try {
            for (int id = start; id < start + count; id++) {
                Random newValue = createRandom(id);
                ProducerMessage<Random, Random> message = ProducerMessage.<Random, Random>newBuilder()
                        .setStream(STREAM)
                        .setMessageId(UUID.randomUUID())
                        .setKey(newValue)
                        .setValue(newValue)
                        .build();
                result.add(newValue);
                ProducedMessage<Random, Random> produceResult = producer.produce(message).get();
                LOG.info("Produced message " + produceResult.getMessage().getKey().getRandom() + " on cluster " + produceResult.getCluster());
            }
        } catch (ExecutionException | InterruptedException e) {
            LOG.error("Caught exception while producing", e);
        }

        return result;
    }

    @Test
    public void testSwitchingConsumer_AtLeastOnce() throws ExecutionException, InterruptedException {
        // Test that a consumer with AT_LEAST_ONCE strategy switches correctly to another cluster
        // when directed to do so by the Discovery API
        // Scenario:
        // - Set up consumer on Cluster A
        // - Produce 100 messages on Cluster A
        // - Switch consumer to cluster B
        // - Produce 100 messages on Cluster B
        // - Verify that all messages were received by the consumer

        final InstanceUnit instance = platform.instance();
        final SpecificAvroConsumerConfig<Random, Random> consumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                .setStream(STREAM)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .build();
        final SpecificAvroProducerConfig<Random, Random> producerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                .setMessageBufferSize(1)
                .setBlocking(false)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .build();

        final List<Random> producedIDs = new ArrayList<>(200);
        final List<Random> consumedIDs = new ArrayList<>(200);

        try (final AxualClient consumerClient = new AxualClient(instance.getClientConfig("aloConsumer", platform.clusterA()));
             final AxualClient producerClientA = new AxualClient(instance.getClientConfig("producer1", platform.clusterA()));
             final AxualClient producerClientB = new AxualClient(instance.getClientConfig("producer2", platform.clusterB()))) {
            try (Producer<Random, Random> producerA = producerClientA.buildProducer(producerConfig);
                 Producer<Random, Random> producerB = producerClientB.buildProducer(producerConfig);
                 Consumer<Random, Random> consumer = consumerClient.buildConsumer(consumerConfig, message -> {
                     LOG.info("Consumed {}", message.getValue());
                     consumedIDs.add(message.getValue());
                     LOG.info(String.format("Received item %d random %s", consumedIDs.size(), message.getValue()));
                 })) {

                consumer.startConsuming();

                // Producer 100 messages on Cluster A
                producedIDs.addAll(produce(0, 100, producerA));

                //Await all 100 message for 30 seconds maximum
                Awaitility.await().atMost(30, TimeUnit.SECONDS).until(() -> {
                    int size = consumedIDs.size();
                    LOG.info("Consumed List Size = {}", size);
                    return size >= 100;
                });

                // Switch consumer to cluster B
                instance.directApplicationTo(consumerClient.getConfig(), platform.clusterB());

                // Produce 100 messages on Cluster B
                producedIDs.addAll(produce(100, 100, producerB));

                //Await all 100 message for 30 seconds maximum
                Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(250, TimeUnit.MILLISECONDS).until(() -> {
                    int size = consumedIDs.size();
                    LOG.info("Consumed List Size = {}", size);
                    return size >= 200;
                });

                // Stop consuming
                consumer.stopConsuming();
            }
        }
        // Verify all messages were received
        for (Random produced : producedIDs) {
            assertThat(consumedIDs, hasItem(produced));
        }
    }

    @Test
    public void testSwitchingConsumer_AtMostOnce() throws ExecutionException, InterruptedException {
        // Test that an AT_MOST_ONCE consumer switches to another cluster when directed to do so by
        // the Discovery API
        // Scenario:
        // - Set up consumer on Cluster A
        // - Produce 100 messages on Cluster A
        // - Produce 100 messages on Cluster B
        // - Switch consumer to cluster B
        // - Wait for consumer to have switched
        // - Produce 100 messages on Cluster B
        // - Verify that all messages 0-99 and 200-299 were consumed, but not 100-199, since we expect
        //   the consumer to jumpToEnd upon connecting to another cluster

        final InstanceUnit instance = platform.instance();
        SpecificAvroConsumerConfig<Random, Random> consumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                .setStream(STREAM)
                .setDeliveryStrategy(DeliveryStrategy.AT_MOST_ONCE)
                .build();
        SpecificAvroProducerConfig<Random, Random> producerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                .setMessageBufferSize(1)
                .setBlocking(false)
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .build();

        final List<Random> producedIDsA = new ArrayList<>(100);
        final List<Random> producedIDsB_beforeSwitch = new ArrayList<>(100);
        final List<Random> producedIDsB_afterSwitch = new ArrayList<>(100);
        final List<Random> consumedIDs = new ArrayList<>(100);
        final List<Random> consumedIDs_beforeSwitch = new ArrayList<>(100);
        final List<Random> consumedIDs_afterSwitch = new ArrayList<>(100);

        try (AxualClient consumerClient = new AxualClient(instance.getClientConfig("amoConsumer", platform.clusterA()));
             AxualClient producerClientA = new AxualClient(instance.getClientConfig("producer1", platform.clusterA()));
             AxualClient producerClientB = new AxualClient(instance.getClientConfig("producer2", platform.clusterB()))) {
            try (Producer<Random, Random> producerA = producerClientA.buildProducer(producerConfig);
                 Producer<Random, Random> producerB = producerClientB.buildProducer(producerConfig);
                 Consumer<Random, Random> consumer = consumerClient.buildConsumer(consumerConfig, message -> {
                     consumedIDs.add(message.getValue());
                     LOG.info("Received item {} random {}", consumedIDs.size(), message.getValue());
                 })) {
                consumer.startConsuming();

                // Sleep before producing to ensure we will get the full first batch of 100 messages
                SleepUtil.sleep(Duration.ofMillis(longerThanTTL));

                producedIDsA.addAll(produce(0, 100, producerA));
                producedIDsB_beforeSwitch.addAll(produce(100, 100, producerB));

                //Await all 100 message for 30 seconds maximum
                Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(200, TimeUnit.MILLISECONDS).until(() -> {
                    int size = consumedIDs.size();
                    LOG.info("Consumed List Size = {}", size);
                    return size >= 100;
                });

                consumedIDs_beforeSwitch.addAll(consumedIDs);
                consumedIDs.clear();

                // Perform switch
                instance.directApplicationTo(consumerClient.getConfig(), platform.clusterB());
                LOG.info("Switched consumer to Cluster B");
                // Sleep enough time to make sure the consumer has switched and started consuming
                SleepUtil.sleep(Duration.ofMillis(longerThanTTL + TTL));

                // Produce messages 200-299 on Cluster B
                producedIDsB_afterSwitch.addAll(produce(200, 100, producerB));

                //Await all 100 message for 30 seconds maximum
                Awaitility.await().atMost(30, TimeUnit.SECONDS).pollInterval(200, TimeUnit.MILLISECONDS).until(() -> {
                    int size = consumedIDs.size();
                    LOG.info("Consumed List Size = {}", size);
                    return size >= 100;
                });

                consumer.stopConsuming();
                consumedIDs_afterSwitch.addAll(consumedIDs);
            }
        }
        // Produced 0-100 on A before switch - should be consumed
        // Produced 100-200 on B before switch - should not be consumed because upon connecting to B the consumer should seekToEnd
        // Produced 200-300 on B after switch - should be consumed

        // check if consumedIDs_beforeSwitch is in producedIDA and not in producedIDsB_beforeSwitch
        for (Random consumedID : consumedIDs_beforeSwitch) {
            assertThat(producedIDsA, hasItem(consumedID));
            assertThat(producedIDsB_beforeSwitch, not(hasItem(consumedID)));
        }

        for (Random consumedID : consumedIDs_afterSwitch) {
            assertThat(producedIDsB_afterSwitch, hasItem(consumedID));
            assertThat(producedIDsB_beforeSwitch, not(hasItem(consumedID)));
        }
    }
}
