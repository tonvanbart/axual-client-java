package io.axual.client.proxy.switching.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

/**
 * Holds constants used in distributor configurations.
 */
public class DistributorConfigs {
    private DistributorConfigs() {
    }

    /** timeout to take into account for distribution. */
    public static final String DISTRIBUTOR_TIMEOUT_CONFIG = "distributor.timeout";
    /** distance to the cluster we're distributing to. */
    public static final String DISTRIBUTOR_DISTANCE_CONFIG = "distributor.distance";
}
