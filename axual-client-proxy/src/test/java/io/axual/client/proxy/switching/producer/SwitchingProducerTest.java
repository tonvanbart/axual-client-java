package io.axual.client.proxy.switching.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import io.axual.client.proxy.switching.discovery.DiscoverySubscriber;
import io.axual.client.proxy.switching.generic.BaseClientProxySwitcher;
import io.axual.client.proxy.switching.generic.DistributorConfigs;
import io.axual.client.proxy.switching.generic.SwitchingProxy;
import io.axual.client.proxy.test.TestDiscoveryLoader;
import io.axual.client.proxy.test.TestProducerProxy;
import io.axual.client.proxy.test.TestProducerProxyFactory;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.discovery.client.DiscoveryClientRegistry;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.discovery.client.fetcher.DiscoveryLoader;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SwitchingProducer.class, TestProducerProxyFactory.class, TestProducerProxy.class,
    SwitchingProxy.class, BaseClientProxySwitcher.class, DiscoveryClientRegistry.class,
    DiscoveryLoader.class})
@PowerMockIgnore({"javax.net.ssl.*","jdk.internal.reflect.*"})
public class SwitchingProducerTest {

  private static final String TOPIC = "general-applicationlog";
  private static final String APPLICATION_ID = "io.axual.test";
  private static final String APPLICATION_VERSION = "1.0";
  private static final String TENANT = "axual";
  private static final String DISCOVERY_ENDPOINT = "https://discovery.location/v2/";
  public static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
  public static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
  public static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");

  @Mock
  TestProducerProxy<String, String> mockedProducer1, mockedProducer2;

  DiscoveryLoader mockedDiscoveryLoader;
  @Before
  public void setup() throws Exception {
    whenNew(TestProducerProxy.class).withAnyArguments()
        .thenReturn(mockedProducer1, mockedProducer2);
    mockedDiscoveryLoader = new TestDiscoveryLoader(createDiscoveryResult());
    spy(DiscoveryClientRegistry.class);
    PowerMockito.whenNew(DiscoveryLoader.class).withAnyArguments()
        .thenReturn(mockedDiscoveryLoader);
  }

  @Test
  public void construction_ValidProperties() throws Exception {
    final SwitchingProducer producer = spy(new SwitchingProducer(getValidProperties()));
    validConstructorTest(producer);
  }

  @Test
  public void construction_ValidMap() throws Exception {
    final SwitchingProducer producer = spy(new SwitchingProducer(getValidMap()));
    validConstructorTest(producer);
  }

  @Test
  public void initTransactions() {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.initTransactions();
    verify(mockedProducer1).initTransactions();
  }

  @Test
  public void beginTransaction() {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.beginTransaction();
    verify(mockedProducer1).beginTransaction();
  }

  @Test
  public void sendOffsetsToTransaction_WithGroup() {
    // given a switching producer
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    Map<TopicPartition, OffsetAndMetadata> offsets = null;
    String consumerGroupId = "somegroup";

    // when we send the offsets for a transaction
    producer.sendOffsetsToTransaction(offsets, consumerGroupId);

    // the call is forwarded to the proxied object
    verify(mockedProducer1).sendOffsetsToTransaction(offsets, consumerGroupId);
  }

  @Test
  public void sendOffsetsToTransaction() {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    Map<TopicPartition, OffsetAndMetadata> offsets = null;
    ConsumerGroupMetadata groupMetadata = new ConsumerGroupMetadata("somegroup");
    producer.sendOffsetsToTransaction(offsets, groupMetadata);

    verify(mockedProducer1).sendOffsetsToTransaction(offsets, groupMetadata);
  }

  @Test(expected = TransactionSwitchedException.class)
  public void sendOffsetsToTransaction_afterSwitch_transactionIsAborted() throws Exception {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.initTransactions();

    producer.send(new ProducerRecord(TOPIC,"key", "value"));

    // when we switch before committing offsets
    DiscoverySubscriber subscriber = (DiscoverySubscriber) Whitebox.getInternalState(producer, "subscriber");
    Whitebox.setInternalState(subscriber, "needToSwitch", Boolean.TRUE);
    Whitebox.setInternalState(subscriber, "newDiscoveryResult", createDiscoveryResult());

    // sending offsets should abort the transaction and throw an exception
    producer.sendOffsetsToTransaction(null, "somegroup");

    verify(mockedProducer1).abortTransaction();
  }

  @Test(expected = TransactionSwitchedException.class)
  public void sendOffsetsToTransaction_afterSwitch_transactionIsAborted2() throws Exception {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.initTransactions();

    producer.send(new ProducerRecord(TOPIC,"key", "value"));

    // when we switch before committing offsets
    DiscoverySubscriber subscriber = (DiscoverySubscriber) Whitebox.getInternalState(producer, "subscriber");
    Whitebox.setInternalState(subscriber, "needToSwitch", Boolean.TRUE);
    Whitebox.setInternalState(subscriber, "newDiscoveryResult", createDiscoveryResult());

    // sending offsets should abort the transaction
    producer.sendOffsetsToTransaction(null, new ConsumerGroupMetadata("", 1, "", Optional.empty()));

    verify(mockedProducer1).abortTransaction();
  }

  @Test
  public void commitTransaction() {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.commitTransaction();
    verify(mockedProducer1).commitTransaction();
  }

  @Test
  public void commitTransaction_afterSwitch_commitOnOldCluster() throws Exception {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.initTransactions();
    producer.beginTransaction();

    // produce
    producer.send(new ProducerRecord(TOPIC,"key", "value"));

    // when discovery indicates a switch
    DiscoverySubscriber subscriber = (DiscoverySubscriber) Whitebox.getInternalState(producer, "subscriber");
    Whitebox.setInternalState(subscriber, "needToSwitch", Boolean.TRUE);
    Whitebox.setInternalState(subscriber, "newDiscoveryResult", createDiscoveryResult());
    producer.commitTransaction();

    // the commit call should ignore and commit on the old cluster
    verify(mockedProducer1).commitTransaction();
  }

  @Test
  public void abortTransaction() {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.abortTransaction();
    verify(mockedProducer1).abortTransaction();
  }

  @Test
  public void abortTransaction_afterSwitch_abortsOnOldCluster() throws Exception {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.initTransactions();
    producer.beginTransaction();
    producer.send(new ProducerRecord(TOPIC,"key", "value"));

    // when a cluster switch will be done
    DiscoverySubscriber subscriber = (DiscoverySubscriber) Whitebox.getInternalState(producer, "subscriber");
    Whitebox.setInternalState(subscriber, "needToSwitch", Boolean.TRUE);
    Whitebox.setInternalState(subscriber, "newDiscoveryResult", createDiscoveryResult());

    // trying abortTransaction() should also abort on old cluster
    producer.abortTransaction();
    verify(mockedProducer1).abortTransaction();

  }

  @Test
  public void send() throws Exception {
    final SwitchingProducer producer = spy(new SwitchingProducer(getValidProperties()));

    final String key = "MyKey";
    final String value = "MyValue";
    final ProducerRecord record = new ProducerRecord(TOPIC, key, value);
    producer.send(record);

    final ArgumentCaptor<ProducerRecord> producerRecordCaptor = ArgumentCaptor
        .forClass(ProducerRecord.class);
    verify(mockedProducer1).send(producerRecordCaptor.capture());

  }

  @Test
  public void sendWithSwitch() throws Exception {
    // Setup switch data
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());

    final String key = "MyKey";
    final String value = "MyValue";
    final ProducerRecord record = new ProducerRecord(TOPIC, key, value);

    producer.send(record);

    // Force producer switch
    DiscoverySubscriber subscriber = (DiscoverySubscriber) Whitebox.getInternalState(producer, "subscriber");
    Whitebox.setInternalState(subscriber, "needToSwitch", Boolean.TRUE);
    Whitebox.setInternalState(subscriber, "newDiscoveryResult", createDiscoveryResult());

    producer.send(record);

    final ArgumentCaptor<ProducerRecord> captor1 = ArgumentCaptor.forClass(ProducerRecord.class);
    verify(mockedProducer1, Mockito.times(1)).send(captor1.capture());

    final ArgumentCaptor<ProducerRecord> captor2 = ArgumentCaptor.forClass(ProducerRecord.class);
    verify(mockedProducer2, Mockito.times(1)).send(captor2.capture());

    final ProducerRecord capturedProducerRecord1 = captor1.getValue();
    final ProducerRecord capturedProducerRecord2 = captor2.getValue();

    Assert.assertNotNull(capturedProducerRecord1);
    Assert.assertNotNull(capturedProducerRecord2);
  }

  @Test(expected = TransactionSwitchedException.class)
  public void send_AfterSwitch_transactionIsAborted() throws Exception {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.initTransactions();
    producer.beginTransaction();

    // Given that we will switch clusters
    DiscoverySubscriber subscriber = (DiscoverySubscriber) Whitebox.getInternalState(producer, "subscriber");
    Whitebox.setInternalState(subscriber, "needToSwitch", Boolean.TRUE);
    Whitebox.setInternalState(subscriber, "newDiscoveryResult", createDiscoveryResult());

    // when we call send() on the switching producer
    final ProducerRecord record = new ProducerRecord(TOPIC, "key", "value");
    producer.send(record);

    // transactions on the old producer will be aborted
    verify(mockedProducer1).abortTransaction();
    // send is called on the new producer (would result in IllegalStateException to signal transaction no longer valid)
    verify(mockedProducer2).send(record);

  }

  @Test(expected = TransactionSwitchedException.class)
  public void sendWithCallback_afterSwitch_transactionAborted() throws Exception {
    final SwitchingProducer producer = new SwitchingProducer(getValidProperties());
    producer.initTransactions();
    producer.beginTransaction();

    // Given that we will switch clusters
    DiscoverySubscriber subscriber = (DiscoverySubscriber) Whitebox.getInternalState(producer, "subscriber");
    Whitebox.setInternalState(subscriber, "needToSwitch", Boolean.TRUE);
    Whitebox.setInternalState(subscriber, "newDiscoveryResult", createDiscoveryResult());

    // when we call send() on the switching producer
    final ProducerRecord record = new ProducerRecord(TOPIC, "key", "value");
    producer.send(record, (meta, exception) -> {});

    // transactions on the old producer will be aborted
    verify(mockedProducer1).abortTransaction();

  }

  @Test
  public void sendWithCallback() {
    final SwitchingProducer producer = spy(new SwitchingProducer(getValidProperties()));

    final String key = "MyKey";
    final String value = "MyValue";
    final ProducerRecord record = new ProducerRecord(TOPIC, key, value);

    // Created to compare to mock input
    final Callback produceCallback = (metadata, exception) -> {
      // never called;
    };

    producer.send(record, produceCallback);

    final ArgumentCaptor<ProducerRecord> producerRecordCaptor = ArgumentCaptor
        .forClass(ProducerRecord.class);
    final ArgumentCaptor<Callback> callbackCaptor = ArgumentCaptor.forClass(Callback.class);
    verify(mockedProducer1, Mockito.times(1))
        .send(producerRecordCaptor.capture(), callbackCaptor.capture());

    final ProducerRecord capturedProducerRecord = producerRecordCaptor.getValue();
    Assert.assertNotNull(capturedProducerRecord);
    Assert.assertEquals(key, capturedProducerRecord.key());
    Assert.assertEquals(value, capturedProducerRecord.value());
  }

  @Test
  public void sendWithCallbackInTransaction() {
    final SwitchingProducer producer = spy(new SwitchingProducer(getValidProperties()));

    final String key = "MyKey";
    final String value = "MyValue";
    final ProducerRecord record = new ProducerRecord(TOPIC, key, value);

    // Created to compare to mock input
    final Callback produceCallback = dummyCallback();

    producer.initTransactions();
    producer.beginTransaction();
    producer.send(record, produceCallback);

    final ArgumentCaptor<ProducerRecord> producerRecordCaptor = ArgumentCaptor.forClass(ProducerRecord.class);
    final ArgumentCaptor<Callback> callbackCaptor = ArgumentCaptor.forClass(Callback.class);
    verify(mockedProducer1).send(producerRecordCaptor.capture(), callbackCaptor.capture());

    final ProducerRecord capturedProducerRecord = producerRecordCaptor.getValue();
    Assert.assertNotNull(capturedProducerRecord);
    Assert.assertEquals(key, capturedProducerRecord.key());
    Assert.assertEquals(value, capturedProducerRecord.value());
  }

  @Test
  public void flush() {
    final SwitchingProducer producer = spy(new SwitchingProducer(getValidProperties()));
    producer.flush();
    verify(mockedProducer1, Mockito.times(1)).flush();
  }

  @Test
  public void partitionsFor() {
    final SwitchingProducer producer = spy(new SwitchingProducer(getValidProperties()));

    producer.partitionsFor(TOPIC);

    final ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
    verify(mockedProducer1, Mockito.times(1)).partitionsFor(topicCaptor.capture());

    final String capturedTopic = topicCaptor.getValue();
    Assert.assertEquals(TOPIC, capturedTopic);
  }

  @Test
  public void metrics() {
    final SwitchingProducer producer = spy(new SwitchingProducer(getValidProperties()));

    producer.metrics();

    verify(mockedProducer1, Mockito.times(1)).metrics();
  }

  private DiscoveryResult createDiscoveryResult() {
    Properties result = new Properties();
    result.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "bootstrap1");
    result.setProperty(DiscoveryResult.CLUSTER_PROPERTY, "cluster1");
    result.setProperty(DistributorConfigs.DISTRIBUTOR_TIMEOUT_CONFIG, "200");
    result.setProperty(DiscoveryResult.INSTANCE_PROPERTY, "instance1");
    result.setProperty(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "schemaregistry1");
    result.setProperty(DiscoveryResult.SYSTEM_PROPERTY, "System1");
    result.setProperty(DiscoveryResult.TTL_PROPERTY, "100");
    return new DiscoveryResult(result);
  }

  private Properties getValidProperties() {
    final Properties producerProps = new Properties();
    producerProps.putAll(getValidMap());
    return producerProps;
  }

  private Map<String, Object> getValidMap() {
    final Map<String, Object> producerMap = new HashMap<>();

    producerMap.put(SwitchingProducerConfig.BACKING_FACTORY_CONFIG,
            TestProducerProxyFactory.class.getName());
    producerMap.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
    producerMap.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);
    producerMap.put(CommonConfig.ENDPOINT, DISCOVERY_ENDPOINT);
    producerMap.put(CommonConfig.TENANT, TENANT);

    producerMap.put(ProducerConfig.ACKS_CONFIG, "-1");
    producerMap.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");
    producerMap.put(ProducerConfig.RETRIES_CONFIG, "" + Integer.MAX_VALUE);
    producerMap.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    producerMap.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

    SslConfig sslConfig = SslConfig.newBuilder()
            .setKeyPassword(SSL_PASSWORD)
            .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
            .setKeystorePassword(SSL_PASSWORD)
            .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
            .setTruststorePassword(SSL_PASSWORD)
            .setEnableHostnameVerification(false)
            .build();
    KafkaUtil.getKafkaConfigs(sslConfig, producerMap);

    return producerMap;
  }

  private void validConstructorTest(final SwitchingProducer producer) throws Exception {
    final SwitchingProducerConfig config = (SwitchingProducerConfig) Whitebox.getInternalState(producer, "config");

    Assert.assertEquals(SSL_KEYSTORE_LOCATION, config.getSslConfig().getKeystoreLocation());
    Assert.assertEquals(SSL_TRUSTSTORE_LOCATION, config.getSslConfig().getTruststoreLocation());
    Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeystorePassword());
    Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getTruststorePassword());
    Assert.assertEquals(SSL_PASSWORD, config.getSslConfig().getKeyPassword());

    // Verify initialisation of backing producer with mocked properties from the mocked provider
    verifyNew(TestProducerProxy.class, Mockito.times(1));
  }

  /**
   * Returns a do-nothing Producer callback.
   */
  private Callback dummyCallback() {
    return (metadata, exception) -> {
      // never called;
    };
  }

  private <T extends Exception> void assertThrows(Class<T> exceptionType, Runnable runnable) throws Exception {
    try {
      runnable.run();
      fail("Did not get expected exception of type " + exceptionType.getSimpleName());
    } catch (Exception e) {
      if (exceptionType.isInstance(e)) {
        // this was the expected behaviour
      } else {
        throw e;
      }
    }
  }
}
