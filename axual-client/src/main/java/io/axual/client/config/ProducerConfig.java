package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.annotation.InterfaceStability;

/**
 * This configuration object can be used by all clients to configure a producer.
 * Use the {@link #builder()} to create a builder instance.
 *
 * @param <K> the type of the key that will be produced. A compatible key serializer must be
 *            provided to serialize the objects
 * @param <V> the type of the value that will be produced. A compatible value serializer must be
 *            provided to serialize the objects
 */
@InterfaceStability.Evolving
public class ProducerConfig<K, V> extends BaseProducerConfig<K, V> {
    protected ProducerConfig(Builder<K, V, ?> builder) {
        super(builder);
    }

    /**
     * Create a new Builder instance to create a {@link ProducerConfig}.
     *
     * @param <K> the type of the key that will be produced. A compatible key serializer must be
     *            provided to serialize the objects
     * @param <V> the type of the value that will be produced. A compatible value serializer must be
     *            provided to serialize the objects
     * @return A new Builder instance
     */
    public static <K, V> Builder<K, V, ?> builder() {
        return new Builder<>();
    }

    /**
     * The Builder implementation for the {@link ProducerConfig}.
     *
     * @param <K> the type of the key that will be produced. A compatible key serializer must be
     *           provided to serialize the objects
     * @param <V> the type of the value that will be produced. A compatible value serializer must be
     *           provided to serialize the objects
     * @param <T> the type of the Builder that is to be used.
     */
    public static class Builder<K, V, T extends Builder<K, V, T>> extends BaseProducerConfig.Builder<K, V, T> {
        @Override
        public ProducerConfig<K, V> build() {
            validate();
            return new ProducerConfig<>(this);
        }
    }
}
