package io.axual.client.proxy.axual.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;
import java.util.Properties;

import io.axual.client.proxy.generic.client.ClientProxyFactory;
import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.generic.consumer.StaticConsumerProxy;
import io.axual.client.proxy.generic.registry.ProxyChainUtil;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.common.tools.MapUtil;

/**
 * The type Axual consumer. This is a general purpose proxy, that sets up a "proxy chain" using
 * other proxy types. Each proxy adds a specific piece of functionality transparent to the user of
 * this class. Proxy chains are parsed in the AxualConsumerConfig and initialized by the
 * constructor. A typical chain can be configured as follows:
 * <p>
 * properties.put(AxualConsumerConfig.CHAIN_CONFIG, "SWITCHING;RESOLVING;HEADER");
 * <p>
 * This configuration sets up a chain containing the SwitchingConsumer --&gt; ResolvingConsumer
 * --&gt; LineageConsumer --&gt; HeaderConsumer --&gt; KafkaConsumer.
 * <p>
 * Parameters can also be passed to each individual proxy as follows:
 * <p>
 * properties.put(AxualConsumerConfig.CHAIN_CONFIG, "SWITCHING;LOGGING:name=MyLoggingProxy,level=WARN;RESOLVING;LINEAGE;HEADER");
 * <p>
 * In this example the LoggingConsumer gets two extra parameters passed in, namely "name" and
 * "level".
 *
 * @param <K> the key type of messages
 * @param <V> the value type of messages
 */
public class AxualConsumer<K, V> extends StaticConsumerProxy<K, V, AxualConsumerConfig<K, V>> {
    public AxualConsumer(Map<String, Object> configs) {
        super(AxualConsumer.createChain(configs));
    }

    public AxualConsumer(Map<String, Object> configs, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(SerdeUtil.addDeserializersToConfigs(configs, keyDeserializer, valueDeserializer));
    }

    public AxualConsumer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public AxualConsumer(Properties properties, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(MapUtil.objectToStringMap(properties), keyDeserializer, valueDeserializer);
    }

    private static <K, V> ClientProxyInitializer<ConsumerProxy<K, V>, AxualConsumerConfig<K, V>> createChain(Map<String, Object> configs) {
        AxualConsumerConfig<K, V> config = new AxualConsumerConfig<>(configs);
        ClientProxyFactory<ConsumerProxy<K, V>> factory = ProxyChainUtil.setupConsumerFactoryChain(config.getProxyChain(), config.getBackingFactory());
        return new ClientProxyInitializer<>(config, factory.create(config.getDownstreamConfigs()));
    }
}
