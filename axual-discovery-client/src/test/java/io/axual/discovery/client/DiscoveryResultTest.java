package io.axual.discovery.client;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

import static org.junit.Assert.assertEquals;

public class DiscoveryResultTest {

    private static final String REST_PROXY_URL = "rest.proxy.url";
    private Properties properties = new Properties();
    private String restProxyUrl = "rest.proxy.url.nl";

    @Before
    public void init() {
        properties.setProperty(REST_PROXY_URL, restProxyUrl);
    }

    @Test
    public void getRestProxyUrlTest() {
        DiscoveryResult discoveryResult = new DiscoveryResult(properties);
        assertEquals(discoveryResult.getConfigs().get(REST_PROXY_URL), restProxyUrl);
    }
}
