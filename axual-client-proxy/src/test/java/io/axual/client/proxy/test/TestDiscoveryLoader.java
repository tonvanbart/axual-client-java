package io.axual.client.proxy.test;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.discovery.client.DiscoveryConfig;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.discovery.client.fetcher.DiscoveryLoader;

public class TestDiscoveryLoader extends DiscoveryLoader {
    private static final String APP_ID = "dummy";
    private static final String APP_VERSION = "0.0.1-test";
    private final DiscoveryResult result;

    public TestDiscoveryLoader(DiscoveryResult result) {
        super(DiscoveryConfig.newBuilder()
                .setApplicationId(APP_ID)
                .setApplicationVersion(APP_VERSION)
                .setEndpoint("http://example.com")
                .build());
        this.result = result;
    }

    @Override
    public DiscoveryResult getDiscoveryResult() {
        return result;
    }

    @Override
    public boolean discoveryChanged() {
        return true;
    }
}
