package io.axual.client.proxy.wrapped.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Serializer;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.generic.proxy.BaseSerdeConfig;
import io.axual.client.proxy.generic.serde.DeserializerProxyFactory;
import io.axual.client.proxy.generic.serde.SerializerProxyFactory;

public class WrappedSerializerConfig<T> extends BaseSerdeConfig {
    public static final String SERIALIZER_CONFIG = "wrappedserializer.serializer";

    private final Serializer<T> serializer;

    public WrappedSerializerConfig(Map<String, ?> configs, boolean isKey) {
        super(configs, isKey);
        filterDownstream(SERIALIZER_CONFIG);

        Object serializerConfig = configs.get(WrappedDeserializerConfig.DESERIALIZER_CONFIG);
        if (serializerConfig instanceof DeserializerProxyFactory) {
            serializer = ((SerializerProxyFactory<T>) serializerConfig).create(new HashMap<>(getDownstreamConfigs()), isKey);
        } else {
            serializer = getConfiguredInstance(WrappedSerializerConfig.SERIALIZER_CONFIG, Serializer.class);
            serializer.configure(getDownstreamConfigs(), isKey);
        }
    }

    public Serializer<T> getSerializer() {
        return serializer;
    }
}
