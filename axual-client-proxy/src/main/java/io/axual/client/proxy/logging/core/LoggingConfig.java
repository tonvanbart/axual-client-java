package io.axual.client.proxy.logging.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.axual.common.config.ConfigParser;

public class LoggingConfig {
    public static final String LOGLEVEL_CONFIG = "level";
    public static final String METHODS_CONFIG = "methods";
    public static final String NAME_CONFIG = "name";

    private final LogLevel level;
    private final Set<String> methods;
    private final String name;

    private LoggingConfig(LogLevel level, Set<String> methods, String name) {
        this.level = level;
        this.methods = methods;
        this.name = name;
    }

    public static LoggingConfig parse(Map<String, Object> configs) {
        LogLevel level = LogLevel.parse(ConfigParser.parseAndRemoveConfig(configs, LoggingConfig.LOGLEVEL_CONFIG, false, LogLevel.DEBUG));
        Set<String> methods = new HashSet<>(Arrays.asList(ConfigParser.parseAndRemoveStringConfig(configs, LoggingConfig.METHODS_CONFIG, false, "").split(",")));
        methods.remove("");
        String name = ConfigParser.parseAndRemoveStringConfig(configs, LoggingConfig.NAME_CONFIG, false, "");
        return new LoggingConfig(level, methods, name);
    }

    public LogLevel getLevel() {
        return level;
    }

    public Set<String> getMethods() {
        return methods;
    }

    public String getName() {
        return name;
    }
}
