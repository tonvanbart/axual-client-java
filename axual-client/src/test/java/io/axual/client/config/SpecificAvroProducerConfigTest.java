package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.specific.SpecificRecord;
import org.junit.Test;

import io.axual.common.exception.ClientException;

import static org.junit.Assert.assertEquals;

public class SpecificAvroProducerConfigTest {

    @Test(expected = ClientException.class)
    public void testOverridingKeySerializerClass_ShouldThrowAnException() {
        SpecificAvroProducerConfig.Builder<SpecificRecord, SpecificRecord, ?> builder = new SpecificAvroProducerConfig.Builder<>();
        builder
                .setKeySerializer("dummy-key")
                .build();
    }

    @Test(expected = ClientException.class)
    public void testOverridingValueSerializerClass_ShouldThrowAnException() {
        SpecificAvroProducerConfig.Builder<SpecificRecord, SpecificRecord, ?> builder = new SpecificAvroProducerConfig.Builder<>();
        builder
                .setValueSerializer("dummy-value")
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuildSpecificProducerConfigWithoutSettingStrategy_ShouldThrowAnException() {
        SpecificAvroProducerConfig.builder()
                .build();
    }

    @Test
    public void testBuildSpecificProducerConfigWithAllMandatoryProperties_ShouldReturnProperties() {
        DeliveryStrategy expectedDeliveryStrategy = DeliveryStrategy.AT_LEAST_ONCE;
        OrderingStrategy expectedOrderingStrategy = OrderingStrategy.KEEPING_ORDER;
        SpecificAvroProducerConfig producerConfig = SpecificAvroProducerConfig.builder()
                .setDeliveryStrategy(expectedDeliveryStrategy)
                .setOrderingStrategy(expectedOrderingStrategy)
                .build();
        assertEquals(expectedDeliveryStrategy, producerConfig.getDeliveryStrategy());
        assertEquals(expectedOrderingStrategy, producerConfig.getOrderingStrategy());
    }
}
