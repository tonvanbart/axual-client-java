package io.axual.streams.proxy.wrapped;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;

import io.axual.streams.proxy.generic.builder.ConsumedExposer;
import io.axual.streams.proxy.generic.builder.MaterializedExposer;
import io.axual.streams.proxy.generic.builder.NameFixingDeserializer;
import io.axual.streams.proxy.generic.builder.NameFixingSerializer;

public class WrappedStreamsBuilder extends StreamsBuilder {
    @Override
    public synchronized <K, V> KTable<K, V> table(final String topic,
                                                  final Consumed<K, V> consumed,
                                                  final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return super.table(topic, convertConsumed(consumed, topic), convertMaterialized(materialized, topic));
    }

    @Override
    public synchronized <K, V> KTable<K, V> table(final String topic,
                                                  final Consumed<K, V> consumed) {
        return super.table(topic, convertConsumed(consumed, topic));
    }

    @Override
    public synchronized <K, V> KTable<K, V> table(final String topic,
                                                  final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return super.table(topic, convertMaterialized(materialized, topic));
    }

    @Override
    public synchronized <K, V> GlobalKTable<K, V> globalTable(final String topic,
                                                              final Consumed<K, V> consumed) {
        return super.globalTable(topic, convertConsumed(consumed, topic));
    }

    @Override
    public synchronized <K, V> GlobalKTable<K, V> globalTable(final String topic,
                                                              final Consumed<K, V> consumed,
                                                              final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return super.globalTable(topic, convertConsumed(consumed, topic), convertMaterialized(materialized, topic));
    }

    @Override
    public synchronized <K, V> GlobalKTable<K, V> globalTable(final String topic,
                                                              final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return super.globalTable(topic, convertMaterialized(materialized, topic));
    }

    private <K, V> Consumed<K, V> convertConsumed(Consumed<K, V> consumed, String topic) {
        ConsumedExposer<K, V> exposer = new ConsumedExposer<>(consumed);

        Consumed<K, V> result = consumed;
        if (exposer.keySerde() != null) {
            result = result.withKeySerde(Serdes.serdeFrom(
                    new NameFixingSerializer<>(exposer.keySerde().serializer(), topic),
                    new NameFixingDeserializer<>(exposer.keySerde().deserializer(), topic)));
        }

        if (exposer.valueSerde() != null) {
            result = result.withValueSerde(Serdes.serdeFrom(
                    new NameFixingSerializer<>(exposer.valueSerde().serializer(), topic),
                    new NameFixingDeserializer<>(exposer.valueSerde().deserializer(), topic)));
        }
        return result;
    }

    private <K, V> Materialized<K, V, KeyValueStore<Bytes, byte[]>> convertMaterialized(Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized, String topic) {
        MaterializedExposer<K, V, KeyValueStore<Bytes, byte[]>> exposer = new MaterializedExposer<>(materialized);

        Materialized<K, V, KeyValueStore<Bytes, byte[]>> result = materialized
                .withLoggingDisabled()
                .withCachingDisabled();

        if (exposer.keySerde() != null) {
            result = result.withKeySerde(Serdes.serdeFrom(
                    new NameFixingSerializer<>(exposer.keySerde().serializer(), topic),
                    new NameFixingDeserializer<>(exposer.keySerde().deserializer(), topic)));
        }

        if (exposer.valueSerde() != null) {
            result = result.withValueSerde(Serdes.serdeFrom(
                    new NameFixingSerializer<>(exposer.valueSerde().serializer(), topic),
                    new NameFixingDeserializer<>(exposer.valueSerde().deserializer(), topic)));
        }

        return result;
    }
}
