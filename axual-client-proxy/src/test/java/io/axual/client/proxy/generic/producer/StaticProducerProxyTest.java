package io.axual.client.proxy.generic.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Collections;
import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.test.TestProducerProxy;
import io.axual.client.proxy.test.TestProducerProxyFactory;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.discovery.client.DiscoveryClientRegistry;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticProducerProxy.class, StaticProducerProxy.class, TestProducerProxyFactory.class, TestProducerProxy.class, DiscoveryClientRegistry.class})
@PowerMockIgnore({"javax.net.ssl.*","jdk.internal.reflect.*"})
public class StaticProducerProxyTest {
    private static final String BACKING_FACTORY_CONFIG = "backing.factory";

    private static final String TOPIC = "general-applicationlog";
    private static final List<String> TOPICS = Arrays.asList(TOPIC + "1", TOPIC + "2", TOPIC + "3");

    private static final String APPLICATION_ID = "io.axual.test";
    private static final String APPLICATION_VERSION = "1.0";

    private static final String TENANT_PROPERTY = "tnt";
    private static final String ENVIRONMENT_PROPERTY = "envt";

    private static final String TENANT = "axual";
    private static final String ENVIRONMENT = "unit";

    private static final String GROUP_ID = "group";

    private static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
    private static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
    private static final PasswordConfig SSL_PASSWORD = new PasswordConfig("notsecret");

    private static final BaseClientProxyConfig<ProducerProxy<String, String>> config = new BaseClientProxyConfig<>(getValidConfig(), BACKING_FACTORY_CONFIG);

    @Mock
    private TestProducerProxy<String, String> mockedProducer;

    @Before
    public void setUp() throws Exception {
        whenNew(TestProducerProxy.class).withAnyArguments().thenReturn(mockedProducer);
    }

    private static Map<String, Object> getValidConfig() {
        final Map<String, Object> producerMap = new HashMap<>();

        producerMap.put(BACKING_FACTORY_CONFIG, TestProducerProxyFactory.class.getName());
        producerMap.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
        producerMap.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);

        producerMap.put(TENANT_PROPERTY, TENANT);
        producerMap.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);

        producerMap.put(ProducerConfig.ACKS_CONFIG, "-1");
        producerMap.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");
        producerMap.put(ProducerConfig.RETRIES_CONFIG, "" + Integer.MAX_VALUE);
        producerMap.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        producerMap.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        SslConfig sslConfig = SslConfig.newBuilder()
                .setKeyPassword(SSL_PASSWORD)
                .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                .setKeystorePassword(SSL_PASSWORD)
                .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                .setTruststorePassword(SSL_PASSWORD)
                .setEnableHostnameVerification(false)
                .build();
        KafkaUtil.getKafkaConfigs(sslConfig, producerMap);

        return producerMap;
    }

    private StaticProducerProxy<String, String, BaseClientProxyConfig<ProducerProxy<String, String>>> producer() {
        return spy(new StaticProducerProxy<String, String, BaseClientProxyConfig<ProducerProxy<String, String>>>(config));
    }

    @Test
    public void initTransactions() {
        producer().initTransactions();

        verify(mockedProducer, times(1)).initTransactions();
    }

    @Test
    public void beginTransaction() {
        producer().beginTransaction();

        verify(mockedProducer, times(1)).beginTransaction();
    }

    @Test
    public void sendOffsetsToTransaction_GroupId() {
        Map<TopicPartition, OffsetAndMetadata> offsets = Collections.emptyMap();

        producer().sendOffsetsToTransaction(offsets, GROUP_ID);

        verify(mockedProducer, times(1)).sendOffsetsToTransaction(offsets, GROUP_ID);
    }

    @Test
    public void sendOffsetsToTransaction_ConsumerGroupDescription() {
        Map<TopicPartition, OffsetAndMetadata> offsets = Collections.emptyMap();
        ConsumerGroupMetadata description = null;
        producer().sendOffsetsToTransaction(offsets,description);

        verify(mockedProducer, times(1)).sendOffsetsToTransaction(offsets, description);
    }

    @Test
    public void commitTransaction() {
        producer().commitTransaction();

        verify(mockedProducer, times(1)).commitTransaction();
    }

    @Test
    public void abortTransaction() {
        producer().abortTransaction();

        verify(mockedProducer, times(1)).abortTransaction();
    }

    @Test
    public void send() {
        producer().send(null);

        verify(mockedProducer, times(1)).send(null);
    }

    @Test
    public void sendWithCallback() {
        producer().send(null, null);

        verify(mockedProducer, times(1)).send(null, null);
    }

    @Test
    public void flush() {
        producer().flush();

        verify(mockedProducer, times(1)).flush();
    }

    @Test
    public void partitionsFor() {
        producer().partitionsFor(TOPIC);

        verify(mockedProducer, times(1)).partitionsFor(TOPIC);
    }

    @Test
    public void metrics() {
        producer().metrics();

        verify(mockedProducer, times(1)).metrics();
    }
}
