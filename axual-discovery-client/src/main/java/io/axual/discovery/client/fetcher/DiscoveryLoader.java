package io.axual.discovery.client.fetcher;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

import io.axual.discovery.client.DiscoveryConfig;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.discovery.client.exception.DiscoveryException;

/**
 * DiscoveryLoader queries the DiscoveryAPI through DiscoveryFetcher every TTL amount of time. It
 * updates the TTL according to the result it fetches. It is also able to detect changes in the
 * returned values by comparing them field-wise with the previous result.
 *
 * The DiscoveryLoader is not thread-safe. The caller should ensure that calls to the this class are
 * serialized properly.
 */
public class DiscoveryLoader implements AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(DiscoveryLoader.class);
    private static final long BACKOFF_PERIOD = 10_000L; // 10 seconds in millis

    private final DiscoveryFetcher fetcher;

    private long lastFetchTimestamp;
    private long ttl;
    private String lastCluster = null;
    private DiscoveryResult lastResult;
    private boolean firstFetch = true;
    private boolean isClosed = false;

    public DiscoveryLoader(DiscoveryConfig config) {
        fetcher = new DiscoveryFetcher(config);
    }

    public DiscoveryResult getDiscoveryResult() {
        discoveryChanged();
        return lastResult;
    }

    public void invalidate() {
        if (isClosed) {
            throw new DiscoveryException("Illegal call, DiscoveryLoader is closed");
        }
        lastFetchTimestamp = 0;
    }

    public boolean discoveryChanged() {
        if (isClosed) {
            throw new DiscoveryException("Illegal call, DiscoveryLoader is closed");
        }
        if (firstFetch) {
            firstFetch = false;
            lastResult = loadFromDiscovery();
            LOG.info("Fetched discovery properties: {}", lastResult);
            return true;
        }

        DiscoveryResult newResult = loadFromDiscovery();
        if (!Objects.equals(newResult, lastResult)) {
            LOG.info("Change in discovery properties, new properties: {}", newResult);
            lastResult = newResult;
            return true;
        } else {
            LOG.trace("No change in discovery properties: {}", newResult);
        }

        return false;
    }

    private DiscoveryResult loadFromDiscovery() {
        // Refresh discovery result only upon timeout. Do not refresh otherwise, because in case
        // of discovery error, null was returned previously and we want the client to back off for
        // ten seconds, instead of hammering the DiscoveryAPI.
        if (System.currentTimeMillis() - lastFetchTimestamp >= ttl) {
            try {
                LOG.trace("{} configuration", lastResult == null ? "Loading" : "Reloading");

                // From here on we do not care about potential failures when querying the backing provider.
                // So a null result from the backing provider is translated into an empty set of properties.
                DiscoveryResult result = fetcher.executeRequest(lastCluster);
                // Update last fetch timestamp, so we will requery at (now+TTL)
                lastFetchTimestamp = System.currentTimeMillis();

                // Check whether a proper result set was returned
                if (result != null) {
                    // Remember the "cluster" property for next time
                    lastCluster = result.getCluster();
                    // Extract the newly returned TTL, or set to default when missing
                    final long newTtl = result.getTtl();
                    if (newTtl != ttl) {
                        LOG.info("TTL updated to: {} (was: {})", newTtl, ttl);
                        ttl = newTtl;
                    }
                } else {
                    // If null is returned, there is no active cluster found. In this case we do not
                    // overwrite the lastCluster property, because in case an active cluster is found
                    // within the next 10 seconds, we should still simulate switching over from the
                    // last cluster we were connected to. Better to wait a timeout extra than to
                    // connect too early to a new target, potentially outpacing the Distributor.
                    ttl = BACKOFF_PERIOD;
                }

                LOG.trace("Reloaded configuration from discovery: {}", result);
                return result;
            } catch (Exception e) {
                LOG.warn("Could not reload configuration", e);
                // Retry in 10 seconds later, and return old result for now
                lastFetchTimestamp = System.currentTimeMillis() - ttl + BACKOFF_PERIOD;
            }
        }

        return lastResult;
    }

    @Override
    public void close() throws Exception {
        LOG.debug("Closing Discovery Loader");
        if (!isClosed) {
            fetcher.close();
            isClosed = true;
        }
    }
}
