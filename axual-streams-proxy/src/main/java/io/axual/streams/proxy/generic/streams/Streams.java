package io.axual.streams.proxy.generic.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.processor.StreamPartitioner;
import org.apache.kafka.streams.state.QueryableStoreType;
import org.apache.kafka.streams.state.StreamsMetadata;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.axual.common.annotation.InterfaceStability;

@InterfaceStability.Evolving
public interface Streams extends AutoCloseable {
    void setStateListener(KafkaStreams.StateListener listener);

    KafkaStreams.State state();

    Map<MetricName, ? extends Metric> metrics();

    void start();

    void stop();

    void close();

    /**
     * @deprecated Use the {@link #close(Duration)} method.
     */
    @Deprecated
    default void close(long timeout, TimeUnit timeUnit) {
        close(Duration.ofMillis(timeUnit.toMicros(timeout)));
    }

    void close(Duration timeout);

    void cleanUp();

    void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler eh);

    Collection<StreamsMetadata> allMetadata();

    Collection<StreamsMetadata> allMetadataForStore(String storeName);

    <K> StreamsMetadata metadataForKey(String storeName, K key, Serializer<K> keySerializer);

    <K> StreamsMetadata metadataForKey(String storeName, K key, StreamPartitioner<? super K, ?> partitioner);

    <T> T store(String storeName, QueryableStoreType<T> queryableStoreType);
}
