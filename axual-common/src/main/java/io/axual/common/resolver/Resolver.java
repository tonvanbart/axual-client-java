package io.axual.common.resolver;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.Configurable;

import java.util.Map;

public interface Resolver extends Configurable {
    // The prefix to pass in default values for certain placeholders
    String DEFAULT_PREFIX = InternalPatternResolver.DEFAULT_PREFIX;

    void configure(Map<String, ?> configs);

    /**
     * Translates the internal representation of name to the external one.
     *
     * @param name the internal name
     * @return the external name
     */
    String resolve(final String name);

    /**
     * Translates the external representation of name to the internal one.
     *
     * @param name the external name
     * @return the internal name
     */
    String unresolve(final String name);

    /**
     * Translates the external representation of a name to the internal one, returning a map of all
     * parsed fields.
     *
     * @param name the external name
     * @return a map with all parsed fields from the external name
     */
    Map<String, String> unresolveContext(String name);

    /**
     * Fetches the unresolved name from a context that was parsed earlier.
     *
     * @param context the unresolved context
     * @return the unresolved name
     */
    String getNameFromContext(Map<String, String> context);
}
