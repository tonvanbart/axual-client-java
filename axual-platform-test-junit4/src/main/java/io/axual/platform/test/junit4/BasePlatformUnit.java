package io.axual.platform.test.junit4;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-junit4
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.PlatformUnit;
import io.axual.platform.test.core.StreamConfig;
import org.junit.rules.ExternalResource;

/**
 * The base class implementing the JUnit4 ExternalResource to start and stop the Axual Test Platform
 * for each test.
 */
public class BasePlatformUnit<T extends BasePlatformUnit> extends ExternalResource {
    private final PlatformUnit platform;

    /**
     * Constructs the base platform according to the provided PlatformUnit configuration
     *
     * @param platform the platform configuration to use
     */
    BasePlatformUnit(PlatformUnit platform) {
        this.platform = platform;
    }

    /**
     * Starts the Axual Test Platform before each test
     */
    @Override
    public void before() {
        platform.start();
    }

    /**
     * Stops the Axual Test Platform before each test after each test
     */
    @Override
    public void after() {
        platform.stop();
    }

    /**
     * Get the platform configuration used
     *
     * @return the current test platform configuration
     */
    public PlatformUnit platform() {
        return platform;
    }

    /**
     * Get the instance configuration used
     *
     * @return the current instance configuration of the test platform
     */
    public InstanceUnit instance() {
        return platform.getInstance();
    }

    /**
     * Add a stream to the current configuration
     *
     * @param streamConfig The configuration for the specific Stream
     * @return the platform extension
     */
    public T addStream(StreamConfig streamConfig) {
        platform().addStream(streamConfig);
        return (T) this;
    }
}
