<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>io.axual.client</groupId>
    <artifactId>axual-client-parent</artifactId>
    <version>5.6.2-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>Axual Client Java</name>

    <licenses>
        <license>
            <name>Apache 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
            <comments>A business-friendly OSS license</comments>
        </license>
    </licenses>

    <organization>
        <name>Axual B.V.</name>
        <url>https://axual.com/</url>
    </organization>

    <description>
        Java client library that enables interaction with the Axual platform.
    </description>

    <url>https://axual.com/</url>

    <inceptionYear>2020</inceptionYear>

    <scm>
        <developerConnection>scm:git:git@gitlab.com:axual-public/axual-client-java.git</developerConnection>
        <connection>scm:git:git@gitlab.com:axual-public/axual-client-java.git</connection>
        <url>ssh://git@gitlab.com:axual-public/axual-client-java.git</url>
        <tag>HEAD</tag>
    </scm>

    <distributionManagement>
        <snapshotRepository>
            <id>snapshots</id>
            <url>${axual.maven.repository.url.snapshots}</url>
        </snapshotRepository>
        <repository>
            <id>releases</id>
            <url>${axual.maven.repository.url.releases}</url>
        </repository>
    </distributionManagement>

    <developers>
        <developer>
            <name>Team Altair</name>
            <email>altair@axual.com</email>
            <organization>Axual B.V.</organization>
            <organizationUrl>https://axual.com</organizationUrl>
        </developer>
    </developers>

    <repositories>
        <repository>
            <id>confluent</id>
            <url>http://packages.confluent.io/maven/</url>
        </repository>
    </repositories>

    <modules>
        <module>axual-common</module>
        <module>axual-common-test</module>
        <module>axual-serde</module>
        <module>axual-serde-test-schema</module>
        <module>axual-discovery-client</module>
        <module>axual-client-proxy</module>
        <module>axual-client-proxy-integrationtest</module>
        <module>axual-client</module>
        <module>axual-client-spring</module>
        <module>axual-client-test-schema</module>
        <module>axual-client-integrationtest</module>
        <module>axual-streams-proxy</module>
        <module>axual-streams</module>
        <module>axual-streams-integrationtest</module>
        <module>axual-platform-test-core</module>
        <module>axual-platform-test-junit4</module>
        <module>axual-platform-test-jupiter</module>
        <module>axual-platform-test-standalone</module>
        <module>report</module>
    </modules>

    <properties>
        <java.source.version>1.8</java.source.version>
        <java.target.version>1.8</java.target.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <!-- Plugin versions -->
        <maven.clean.plugin.version>3.0.0</maven.clean.plugin.version>
        <maven.compiler.plugin.version>3.7.0</maven.compiler.plugin.version>
        <maven.jar.plugin.version>3.0.2</maven.jar.plugin.version>
        <maven.source.plugin.version>3.0.1</maven.source.plugin.version>
        <maven.javadoc.plugin.version>3.2.0</maven.javadoc.plugin.version>
        <maven.install.plugin.version>2.5.2</maven.install.plugin.version>
        <maven.deploy.plugin.version>2.8.2</maven.deploy.plugin.version>
        <maven.enforcer.plugin.version>1.4.1</maven.enforcer.plugin.version>
        <smartic.enforcer.rules.version>1.0.2</smartic.enforcer.rules.version>
        <maven.dependency.plugin.version>3.0.2</maven.dependency.plugin.version>
        <maven.surefire.plugin.version>2.20</maven.surefire.plugin.version>
        <maven.failsafe.plugin.version>2.20</maven.failsafe.plugin.version>
        <maven.jacoco.plugin.version>0.8.0</maven.jacoco.plugin.version>
        <maven.resources.plugin.version>3.0.2</maven.resources.plugin.version>
        <maven.jaxb2.plugin.version>0.12.3</maven.jaxb2.plugin.version>
        <owasp.dependency.check.plugin.version>5.3.2</owasp.dependency.check.plugin.version>
        <maven.license.plugin.version>1.9</maven.license.plugin.version>
        <properties.maven.plugin.version>1.0.0</properties.maven.plugin.version>
        <maven.gpg.plugin.version>1.6</maven.gpg.plugin.version>
        <maven.nexus.staging.maven.plugin>1.6.7</maven.nexus.staging.maven.plugin>

        <apache.kafka.version>2.7.0</apache.kafka.version>
        <apache.avro.version>1.9.2</apache.avro.version>
        <jackson.databind.version>2.12.1</jackson.databind.version>

        <slf4j.version>1.7.30</slf4j.version>
        <junit.version>4.13.1</junit.version>
        <junit.jupiter.version>5.7.0</junit.jupiter.version>
        <org.awaitility.version>3.1.5</org.awaitility.version>
        <hamcrest.version>2.2</hamcrest.version>
        <mockito.version>1.10.19</mockito.version>
        <powermock.version>1.7.3</powermock.version>

        <!--ENFORCER Settings-->
        <axual.enforce.skip>false</axual.enforce.skip>
        <axual.enforce.fail>false</axual.enforce.fail>
        <axual.enforce.javaversion.source.regex>^1.8$</axual.enforce.javaversion.source.regex>
        <axual.enforce.javaversion.source.regex.message>The java.source.version property does not
            match the regex (default is only 1.8)
        </axual.enforce.javaversion.source.regex.message>

        <axual.enforce.javaversion.target.regex>^1.8$</axual.enforce.javaversion.target.regex>
        <axual.enforce.javaversion.target.regex.message>The java.target.version property does not
            match the regex (default is only 1.8)
        </axual.enforce.javaversion.target.regex.message>

        <axual.enforce.projectversion.regex.old>.*(\d|-SNAPSHOT)$
        </axual.enforce.projectversion.regex.old>
        <axual.enforce.projectversion.regex.old.message>Project version must end in a number or
            -SNAPSHOT.
        </axual.enforce.projectversion.regex.old.message>

        <axual.enforce.projectversion.regex>^\d+\.\d+\.\d+([\-a-zA-Z0-9\-_]*-SNAPSHOT)?$
        </axual.enforce.projectversion.regex>
        <axual.enforce.projectversion.regex.message>The version does not adhere to the
            major.minor.revision scheme with escaped branchname and -SNAPSHOT appended
        </axual.enforce.projectversion.regex.message>

        <axual.enforce.scm.regex>^((?!http|HTTP).)*$</axual.enforce.scm.regex>
        <axual.enforce.scm.regex.message>SCM developerConnection must use ssh only. http(s) is not
            allowed.
        </axual.enforce.scm.regex.message>

        <!--SONAR PROPERTIES-->
        <sonar.projectName>${project.artifactId}</sonar.projectName>
        <sonar.projectVersion>${project.version}</sonar.projectVersion>
        <sonar.java.sources>${project.build.sourceDirectory}</sonar.java.sources>
        <sonar.java.binaries>${project.build.directory}/classes</sonar.java.binaries>
        <sonar.java.tests>${project.build.directory}/test-classes</sonar.java.tests>
        <sonar.sourceEncoding>UTF-8</sonar.sourceEncoding>
        <sonar.scm.disabled>true</sonar.scm.disabled>
        <sonar.verbose>true</sonar.verbose>

        <!-- Sonar for JACOCO-->
        <sonar.java.coveragePlugin>jacoco</sonar.java.coveragePlugin>
        <sonar.dynamicAnalysis>reuseReports</sonar.dynamicAnalysis>
        <sonar.jacoco.xmlReportPaths>report/target/site/jacoco-aggregate/jacoco.xml</sonar.jacoco.xmlReportPaths>

        <!--SONAR and Testing Settings -->
        <maven.failsafe.surefire.reports>${project.build.directory}/surefire-reports</maven.failsafe.surefire.reports>
        <skipITs>false</skipITs>
        <skipUTs>false</skipUTs>

        <skipTests>false</skipTests>

        <!-- DependencyCheckSettings -->
        <failOnCVSS>4</failOnCVSS>

        <!-- License -->
        <license.licenseName>apache_v2</license.licenseName>

        <!-- Staging plugin properties -->
        <staging.progress.timeout.minutes>30</staging.progress.timeout.minutes>

        <!-- Java 11 missing JAXB types -->
        <jakarta.xml.bindapi.version>2.3.3</jakarta.xml.bindapi.version>
        <glassfish.jaxb.runtime.version>2.3.3</glassfish.jaxb.runtime.version>

    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- Submodules -->
            <dependency>
                <groupId>io.axual.common</groupId>
                <artifactId>axual-common-test</artifactId>
                <version>${project.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.common</groupId>
                <artifactId>axual-common</artifactId>
                <version>${project.version}</version>
                <scope>compile</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.discovery</groupId>
                <artifactId>axual-discovery-client</artifactId>
                <version>${project.version}</version>
                <scope>compile</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.serde</groupId>
                <artifactId>axual-serde</artifactId>
                <version>${project.version}</version>
                <scope>compile</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.serde</groupId>
                <artifactId>axual-serde-test-schema</artifactId>
                <version>${project.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.client</groupId>
                <artifactId>axual-client-proxy</artifactId>
                <version>${project.version}</version>
                <scope>compile</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.client</groupId>
                <artifactId>axual-client</artifactId>
                <version>${project.version}</version>
                <scope>compile</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.streams</groupId>
                <artifactId>axual-streams-proxy</artifactId>
                <version>${project.version}</version>
                <scope>compile</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.streams</groupId>
                <artifactId>axual-streams</artifactId>
                <version>${project.version}</version>
                <scope>compile</scope>
            </dependency>
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>axual-client-spring</artifactId>
                <version>${project.version}</version>
                <scope>compile</scope>
            </dependency>
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>axual-client-test-schema</artifactId>
                <version>${project.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.platform.test</groupId>
                <artifactId>axual-platform-test-core</artifactId>
                <version>${project.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.platform.test</groupId>
                <artifactId>axual-platform-test-junit4</artifactId>
                <version>${project.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.platform.test</groupId>
                <artifactId>axual-platform-test-jupiter</artifactId>
                <version>${project.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.platform.test</groupId>
                <artifactId>axual-platform-test-standalone</artifactId>
                <version>${project.version}</version>
                <scope>test</scope>
            </dependency>

            <!--Compile level Required-->
            <dependency>
                <groupId>org.apache.kafka</groupId>
                <artifactId>kafka-clients</artifactId>
                <version>${apache.kafka.version}</version>
                <scope>compile</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.kafka</groupId>
                <artifactId>kafka-streams</artifactId>
                <version>${apache.kafka.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>${jackson.databind.version}</version>
            </dependency>

            <!-- Test dependencies -->
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter</artifactId>
                <version>${junit.jupiter.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.avro</groupId>
                <artifactId>avro</artifactId>
                <version>${apache.avro.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-all</artifactId>
                <version>${mockito.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-core</artifactId>
                <version>${mockito.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.powermock</groupId>
                <artifactId>powermock-module-junit4</artifactId>
                <version>${powermock.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.powermock</groupId>
                <artifactId>powermock-api-mockito</artifactId>
                <version>${powermock.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.awaitility</groupId>
                <artifactId>awaitility</artifactId>
                <version>${org.awaitility.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-ext</artifactId>
                <version>${slf4j.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-core</artifactId>
                <version>${hamcrest.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <!-- clean plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>${maven.clean.plugin.version}</version>
                </plugin>
                <!-- resources plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven.resources.plugin.version}</version>
                </plugin>
                <!-- compiler plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven.compiler.plugin.version}</version>
                    <configuration>
                        <source>${java.source.version}</source>
                        <target>${java.target.version}</target>
                    </configuration>
                </plugin>
                <!-- jar plugin -->
                <plugin>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>${maven.jar.plugin.version}</version>
                    <configuration>
                        <archive>
                            <manifest>
                                <addDefaultImplementationEntries>true
                                </addDefaultImplementationEntries>
                            </manifest>
                            <manifestEntries>
                                <Created-By>${java.vm.name} ${java.version} (${java.vm.vendor})
                                </Created-By>
                                <Implementation-Vendor-ArtifactId>${project.artifactId}
                                </Implementation-Vendor-ArtifactId>
                            </manifestEntries>
                        </archive>
                    </configuration>
                </plugin>
                <!-- javadoc plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${maven.javadoc.plugin.version}</version>
                    <configuration>
                        <source>${java.source.version}</source>
                        <quiet>true</quiet>
                        <archive>
                            <manifest>
                                <addDefaultImplementationEntries>true
                                </addDefaultImplementationEntries>
                            </manifest>
                            <manifestEntries>
                                <Created-By>${java.vm.name} ${java.version} (${java.vm.vendor})
                                </Created-By>
                                <Implementation-Vendor-ArtifactId>${project.artifactId}
                                </Implementation-Vendor-ArtifactId>
                            </manifestEntries>
                        </archive>
                        <detectJavaApiLink>false</detectJavaApiLink>
                        <detectLinks>false</detectLinks>
                    </configuration>
                </plugin>
                <!-- install plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>${maven.install.plugin.version}</version>
                </plugin>
                <!-- deploy plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>${maven.deploy.plugin.version}</version>
                </plugin>
                <!-- avro plugin -->
                <plugin>
                    <groupId>org.apache.avro</groupId>
                    <artifactId>avro-maven-plugin</artifactId>
                    <version>${apache.avro.version}</version>
                </plugin>
                <!-- OWASP Dependency Check plugin -->
                <plugin>
                    <groupId>org.owasp</groupId>
                    <artifactId>dependency-check-maven</artifactId>
                    <version>${owasp.dependency.check.plugin.version}</version>
                    <configuration>
                        <failBuildOnCVSS>${failOnCVSS}</failBuildOnCVSS>
                        <suppressionFile>suppressed.xml</suppressionFile>
                        <assemblyAnalyzerEnabled>false</assemblyAnalyzerEnabled>
                    </configuration>
                </plugin>
                <!-- license plugin -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>license-maven-plugin</artifactId>
                    <version>${maven.license.plugin.version}</version>
                    <configuration>
                        <verbose>false</verbose>
                        <processStartTag>========================LICENSE_START=================================</processStartTag>
                        <processEndTag>=========================LICENSE_END==================================</processEndTag>
                        <includes>
                            <includes>**/*.java</includes>
                        </includes>
                        <excludedGroups>
                            io.axual
                        </excludedGroups>
                    </configuration>
                    <executions>
                        <execution>
                            <id>generate-license-headers</id>
                            <goals>
                                <goal>update-file-header</goal>
                            </goals>
                            <phase>process-sources</phase>
                            <configuration>
                                <licenseName>Apache 2.0</licenseName>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <!-- properties maven plugin / Used to access cross-module resources -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>properties-maven-plugin</artifactId>
                    <version>${properties.maven.plugin.version}</version>
                    <executions>
                        <execution>
                            <phase>generate-resources</phase>
                            <goals>
                                <goal>write-project-properties</goal>
                            </goals>
                            <configuration>
                                <outputFile>${project.build.outputDirectory}/properties-from-pom.properties</outputFile>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

            </plugins>
        </pluginManagement>


        <plugins>
            <!-- dependency plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>${maven.dependency.plugin.version}</version>
            </plugin>
            <!-- source plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>${maven.source.plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <archive>
                        <manifest>
                            <addDefaultImplementationEntries>true
                            </addDefaultImplementationEntries>
                        </manifest>
                        <manifestEntries>
                            <Created-By>${java.vm.name} ${java.version} (${java.vm.vendor})
                            </Created-By>
                            <Implementation-Vendor-ArtifactId>${project.artifactId}
                            </Implementation-Vendor-ArtifactId>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>
            <!-- javadoc plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!-- jacoco plugin -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${maven.jacoco.plugin.version}</version>
                <executions>
                    <execution>
                        <id>pre-unit-test</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                        <configuration>
                            <propertyName>surefireArgLine</propertyName>
                            <append>true</append>
                        </configuration>
                    </execution>
                    <execution>
                        <id>pre-integration-test</id>
                        <goals>
                            <goal>prepare-agent-integration</goal>
                        </goals>
                        <configuration>
                            <propertyName>failsafeArgLine</propertyName>
                            <append>true</append>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- surefire plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven.surefire.plugin.version}</version>
                <configuration>
                    <skipTests>${skipUTs}</skipTests>
                    <skip>${skipTests}</skip>
                    <includes>
                        <include>**/Test*.java</include>
                        <include>**/*Test.java</include>
                        <include>**/*Tests.java</include>
                        <include>**/*TestCase.java</include>
                    </includes>
                    <excludes>
                        <exclude>**/IT*.java</exclude>
                        <exclude>**/*IT.java</exclude>
                    </excludes>
                </configuration>
            </plugin>
            <!-- failsafe plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>${maven.failsafe.plugin.version}</version>
                <configuration>
                    <skip>${skipTests}</skip>
                </configuration>
                <executions>
                    <execution>
                        <id>integration-test</id>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                        <configuration>
                            <argLine>-DskipUTs</argLine>
                            <skipTests>${skipITs}</skipTests>
                        </configuration>
                    </execution>
                </executions>
                <dependencies>
                    <!-- missing jaxb in Java 11, API and runtime deps -->
                    <dependency>
                        <groupId>jakarta.xml.bind</groupId>
                        <artifactId>jakarta.xml.bind-api</artifactId>
                        <version>${jakarta.xml.bindapi.version}</version>
                    </dependency>
                    <dependency>
                        <groupId>org.glassfish.jaxb</groupId>
                        <artifactId>jaxb-runtime</artifactId>
                        <version>${glassfish.jaxb.runtime.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            <!-- Enforcer Plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>${maven.enforcer.plugin.version}</version>
                <executions>
                    <execution>
                        <id>enforce-java</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>enforce-snapshots</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <NoSnapshotDependenciesInDependencyManagementRule
                                        implementation="de.smartics.maven.enforcer.rule.NoSnapshotsInDependencyManagementRule">
                                    <onlyWhenRelease>true</onlyWhenRelease>
                                    <checkOnlyResolvedDependencies>false
                                    </checkOnlyResolvedDependencies>
                                </NoSnapshotDependenciesInDependencyManagementRule>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>de.smartics.rules</groupId>
                        <artifactId>smartics-enforcer-rules</artifactId>
                        <version>${smartic.enforcer.rules.version}</version>
                    </dependency>
                </dependencies>
                <configuration>
                    <skip>${axual.enforce.skip}</skip>
                    <fail>${axual.enforce.fail}</fail>
                    <rules>
                        <requireJavaVersion>
                            <version>[1.8,)</version>
                            <message>You should use JDK version 1.8 or higher!</message>
                        </requireJavaVersion>
                        <requireProperty>
                            <property>java.source.version</property>
                            <message>You must set the java.source.version property!</message>
                            <regex>${axual.enforce.javaversion.source.regex}</regex>
                            <regexMessage>${axual.enforce.javaversion.source.regex.message}
                            </regexMessage>
                        </requireProperty>
                        <requireProperty>
                            <property>java.target.version</property>
                            <message>You must set the java.target.version property!</message>
                            <regex>${axual.enforce.javaversion.target.regex}</regex>
                            <regexMessage>${axual.enforce.javaversion.target.regex.message}
                            </regexMessage>
                        </requireProperty>
                        <requireProperty>
                            <property>project.version</property>
                            <message>"Project version must be specified."</message>
                            <regex>${axual.enforce.projectversion.regex}</regex>
                            <regexMessage>${axual.enforce.projectversion.regex.message}
                            </regexMessage>
                        </requireProperty>
                    </rules>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <version>${maven.nexus.staging.maven.plugin}</version>
                <extensions>true</extensions>
                <configuration>
                    <serverId>releases</serverId>
                    <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                    <autoReleaseAfterClose>true</autoReleaseAfterClose>
                    <stagingProgressTimeoutMinutes>${staging.progress.timeout.minutes}</stagingProgressTimeoutMinutes>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
       <profile>
            <id>jdk8-disable-strict-javadoc</id>
            <activation>
                <jdk>[1.8,)</jdk>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <groupId>org.apache.maven.plugins</groupId>
                        <configuration>
                            <doclint>none</doclint>
                            <quiet>true</quiet>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>sign-artifacts</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>${maven.gpg.plugin.version}</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                                <configuration>
                                    <!-- This is necessary for gpg to not try to use the pinentry programs -->
                                    <gpgArguments>
                                        <arg>--pinentry-mode</arg>
                                        <arg>loopback</arg>
                                    </gpgArguments>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>check-deps</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-enforcer-plugin</artifactId>
                        <version>${maven.enforcer.plugin.version}</version>
                        <executions>
                            <execution>
                                <id>check-dependency-versions</id>
                                <goals>
                                    <goal>enforce</goal>
                                </goals>
                                <phase>compile</phase>
                                <configuration>
                                    <skip>${axual.enforce.skip}</skip>
                                    <fail>${axual.enforce.fail}</fail>
                                    <rules>
                                        <dependencyConvergence/>
                                    </rules>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
