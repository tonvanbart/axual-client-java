package io.axual.client;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.test.TestUtils;
import io.axual.common.tools.ResourceUtil;
import java.io.File;
import java.io.IOException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AxualClientConfigTest {
    private static final String APPLICATION_ID = "APPID";
    private static final String APPLICATION_VERSION = "APPVERSION";
    private static final String ENDPOINT = "ENDPOINT";
    private static final String ENVIRONMENT = "ENVIRONMENT";
    private static final String TENANT = "TENANT_VALID";

    private static final PasswordConfig SSL_KEY_PASSWORD = new PasswordConfig("notsecret");
    private static final String SSL_KEYSTORE_LOCATION = "ssl/axual.client.keystore.jks";
    private static final PasswordConfig SSL_KEYSTORE_PASSWORD = new PasswordConfig("notsecret");
    private static final String SSL_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.jks";
    private static final PasswordConfig SSL_TRUSTSTORE_PASSWORD = new PasswordConfig("notsecret");
    private static final String TEMP_FILE_NAME = "axual-temp-file.txt";

    private static final String SSL_PKCS12_KEYSTORE_LOCATION = "ssl/axual.client.keystore.p12";
    private static final String SSL_PKCS12_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.p12";
    private static final String SSL_PEM_KEYSTORE_LOCATION = "ssl/axual.client.keystore.pem";
    private static final String SSL_PEM_TRUSTSTORE_LOCATION = "ssl/axual.client.truststore.pem";

    private final Logger LOG = LoggerFactory.getLogger(AxualClientConfigTest.class);

    /**
     * Test to provide user defined temporary directory instead of default temp directory
     */
    @Test
    public void testTempDirectory() throws IOException {
        File file = null;
        try {
            file = TestUtils.createDirectoryAndFile("./axualTempTestDir", TEMP_FILE_NAME);
            int fileListCount = file.listFiles().length;
            ClientConfig config = ClientConfig.newBuilder()
                    .setApplicationId(APPLICATION_ID)
                    .setApplicationVersion(APPLICATION_VERSION)
                    .setEndpoint(ENDPOINT)
                    .setEnvironment(ENVIRONMENT)
                    .setTenant(TENANT)
                    .setTempDir(file.getAbsolutePath())
                    .setSslConfig(SslConfig.newBuilder()
                            .setKeyPassword(SSL_KEY_PASSWORD)
                            .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                            .setKeystorePassword(SSL_KEYSTORE_PASSWORD)
                            .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                            .setTruststorePassword(SSL_TRUSTSTORE_PASSWORD)
                            .build())
                    .build();
            assertTrue("Temporary directory should exist.", file.exists());
            AxualClient axualClient = new AxualClient(config);
            assertEquals("Temporary directory should contain Keystore and Truststore files", 2, file.listFiles((f, name) -> name.endsWith(".tmp")).length);

            axualClient.close();

            assertEquals("Temporary directory should not contain any security file after resource cleanup", fileListCount, file.listFiles().length);
        } finally {
            // delete temp dir
            deleteTempDir(file);
        }

    }


    /**
     * Test to disable creation of temporary security files and use original files instead.
     */
    @Test
    public void testDisableCreationOfTempSecurityFiles() throws IOException {
        File testDir = TestUtils.createDirectory("./axualTempTestDir1");
        try {
            String keystorePath = ResourceUtil.getResourceAsFile(SSL_KEYSTORE_LOCATION, testDir.getAbsolutePath());
            String truststorePath = ResourceUtil.getResourceAsFile(SSL_TRUSTSTORE_LOCATION, testDir.getAbsolutePath());

            final File keystoreFile=new File(keystorePath);
            final File truststoreFile=new File(truststorePath);

            assertTrue("Keystore should exist", keystoreFile.exists());
            assertTrue("Truststore should exist", truststoreFile.exists());

            int fileListCount = testDir.listFiles().length;
            ClientConfig config = ClientConfig.newBuilder()
                    .setApplicationId(APPLICATION_ID)
                    .setApplicationVersion(APPLICATION_VERSION)
                    .setEndpoint(ENDPOINT)
                    .setEnvironment(ENVIRONMENT)
                    .setTenant(TENANT)
                    .setTempDir(testDir.getAbsolutePath())
                    .setDisableTemporarySecurityFile(true)
                    .setSslConfig(SslConfig.newBuilder()
                            .setKeyPassword(SSL_KEY_PASSWORD)
                            .setKeystoreLocation(keystorePath)
                            .setKeystorePassword(SSL_KEYSTORE_PASSWORD)
                            .setTruststoreLocation(truststorePath)
                            .setTruststorePassword(SSL_TRUSTSTORE_PASSWORD)
                            .build())
                    .build();

            assertTrue("Temporary directory should exist.", testDir.exists());

            AxualClient axualClient = new AxualClient(config);

            assertEquals("Temporary directory should not contain any security file", fileListCount, testDir.listFiles().length);
            axualClient.close();


            assertTrue("Original keystore file should not delete after resource cleanup.", keystoreFile.exists());
            assertTrue("Original truststore file should not delete after resource cleanup. ", truststoreFile.exists());
        } finally {
            // delete temp dir
            deleteTempDir(testDir);
        }
    }

    /**
     * Test the default behavior means use java default temporary directory
     */
    @Test
    public void testDefault() throws IOException {
        File file = null;
        try {
            file = TestUtils.createDirectoryAndFile("./axualTempTestDir2", TEMP_FILE_NAME);
            int fileListCount = file.listFiles().length;
            ClientConfig config = ClientConfig.newBuilder()
                    .setApplicationId(APPLICATION_ID)
                    .setApplicationVersion(APPLICATION_VERSION)
                    .setEndpoint(ENDPOINT)
                    .setEnvironment(ENVIRONMENT)
                    .setTenant(TENANT)
                    .setSslConfig(SslConfig.newBuilder()
                            .setKeyPassword(SSL_KEY_PASSWORD)
                            .setKeystoreLocation(SSL_KEYSTORE_LOCATION)
                            .setKeystorePassword(SSL_KEYSTORE_PASSWORD)
                            .setTruststoreLocation(SSL_TRUSTSTORE_LOCATION)
                            .setTruststorePassword(SSL_TRUSTSTORE_PASSWORD)
                            .build())
                    .build();
            assertTrue("Temporary directory should exist.", file.exists());
            AxualClient axualClient = new AxualClient(config);
            assertEquals("User define temporary directory should not contain any security file.", fileListCount, file.listFiles().length);
            axualClient.close();
        } finally {
            deleteTempDir(file);
        }
    }

    private void deleteTempDir(File file) {
        if (file != null) {
            try {
                TestUtils.deleteDirectory(file);
            } catch (IOException e) {
                LOG.error("Error while deleting directory: {}", file.getAbsolutePath(), e);
            }
        }
    }


    /**
     * Test to provide user defined temporary directory instead of default temp directory
     */
    @Test
    public void testTempDirectoryForPKCS12SecurityFiles() throws IOException {
        File file = null;
        try {
            file = TestUtils.createDirectoryAndFile("./axualTempTestDir3", TEMP_FILE_NAME);
            int fileListCount = file.listFiles().length;
            ClientConfig config = ClientConfig.newBuilder()
                    .setApplicationId(APPLICATION_ID)
                    .setApplicationVersion(APPLICATION_VERSION)
                    .setEndpoint(ENDPOINT)
                    .setEnvironment(ENVIRONMENT)
                    .setTenant(TENANT)
                    .setTempDir(file.getAbsolutePath())
                    .setSslConfig(SslConfig.newBuilder()
                            .setKeyPassword(SSL_KEY_PASSWORD)
                            .setKeystoreLocation(SSL_PKCS12_KEYSTORE_LOCATION)
                            .setKeystorePassword(SSL_KEYSTORE_PASSWORD)
                            .setTruststoreLocation(SSL_PKCS12_TRUSTSTORE_LOCATION)
                            .setTruststorePassword(SSL_TRUSTSTORE_PASSWORD)
                            .setKeystoreType(SslConfig.KeystoreType.PKCS12)
                            .setTruststoreType(SslConfig.TruststoreType.PKCS12)
                            .build())
                    .build();
            assertTrue("Temporary directory should exist.", file.exists());
            AxualClient axualClient = new AxualClient(config);

            assertEquals(SslConfig.KeystoreType.PKCS12, axualClient.getConfig().getSslConfig().getKeystoreType());
            assertEquals(SslConfig.TruststoreType.PKCS12, axualClient.getConfig().getSslConfig().getTruststoreType());
            assertEquals("Temporary directory should contain Keystore and Truststore files", 2, file.listFiles((f, name) -> name.endsWith(".tmp")).length);

            axualClient.close();

            assertEquals("Temporary directory should not contain any security file after resource cleanup", fileListCount, file.listFiles().length);
        } finally {
            // delete temp dir
            deleteTempDir(file);
        }
    }

    @Test
    public void testTempDirectoryForPEMSecurityFiles() throws IOException {
        File file = null;
        try {
            file = TestUtils.createDirectoryAndFile("./axualTempTestDir3", TEMP_FILE_NAME);
            int fileListCount = file.listFiles().length;
            ClientConfig config = ClientConfig.newBuilder()
                    .setApplicationId(APPLICATION_ID)
                    .setApplicationVersion(APPLICATION_VERSION)
                    .setEndpoint(ENDPOINT)
                    .setEnvironment(ENVIRONMENT)
                    .setTenant(TENANT)
                    .setTempDir(file.getAbsolutePath())
                    .setSslConfig(SslConfig.newBuilder()
                            .setKeystoreType(SslConfig.KeystoreType.PEM)
                            .setKeystoreCertificateChain("KEYSTORE_PEM_CONTENTS")
                            .setKeystoreKey("KEY_PEM_CONTENTS")
                            .setTruststoreType(SslConfig.TruststoreType.PEM)
                            .setTruststoreCertificates("TRUSTSTORE_PEM_CONTENTS")
                            .build())
                    .build();
            assertTrue("Temporary directory should exist.", file.exists());
            AxualClient axualClient = new AxualClient(config);

            assertEquals(SslConfig.KeystoreType.PEM, axualClient.getConfig().getSslConfig().getKeystoreType());
            assertEquals(SslConfig.TruststoreType.PEM, axualClient.getConfig().getSslConfig().getTruststoreType());
            assertEquals("Temporary directory should contain no tmp files", 0, file.listFiles((f, name) -> name.endsWith(".tmp")).length);

            axualClient.close();

            assertEquals("Temporary directory should contain no tmp files", fileListCount, file.listFiles().length);
        } finally {
            // delete temp dir
            deleteTempDir(file);
        }
    }

    @Test
    public void testTempDirectoryForFileBasedPEMSecurityFiles() throws IOException {
        File file = null;
        try {
            file = TestUtils.createDirectoryAndFile("./axualTempTestDir3", TEMP_FILE_NAME);
            int fileListCount = file.listFiles().length;
            ClientConfig config = ClientConfig.newBuilder()
                    .setApplicationId(APPLICATION_ID)
                    .setApplicationVersion(APPLICATION_VERSION)
                    .setEndpoint(ENDPOINT)
                    .setEnvironment(ENVIRONMENT)
                    .setTenant(TENANT)
                    .setTempDir(file.getAbsolutePath())
                    .setSslConfig(SslConfig.newBuilder()
                            .setKeystoreType(SslConfig.KeystoreType.PEM)
                            .setKeystoreLocation(SSL_PEM_KEYSTORE_LOCATION)
                            .setKeyPassword("KEY_PEM_CONTENTS")
                            .setTruststoreType(SslConfig.TruststoreType.PEM)
                            .setTruststoreLocation(SSL_PEM_TRUSTSTORE_LOCATION)
                            .build())
                    .build();
            assertTrue("Temporary directory should exist.", file.exists());
            AxualClient axualClient = new AxualClient(config);

            assertEquals(SslConfig.KeystoreType.PEM, axualClient.getConfig().getSslConfig().getKeystoreType());
            assertEquals(SslConfig.TruststoreType.PEM, axualClient.getConfig().getSslConfig().getTruststoreType());
            assertEquals("Temporary directory should contain pem tmp files", 2, file.listFiles((f, name) -> name.endsWith(".tmp")).length);

            axualClient.close();

            assertEquals("Temporary directory should contain no tmp files", fileListCount, file.listFiles().length);
        } finally {
            // delete temp dir
            deleteTempDir(file);
        }
    }


    /**
     * Test to disable creation of temporary security files and use original files instead.
     */
    @Test
    public void testDisableCreationOfTempSecurityFilesForPKCS12SecurityFiles() throws IOException {
        File testDir = TestUtils.createDirectory("./axualTempTestDir4");
        try {
            String keystorePath = ResourceUtil
                .getResourceAsFile(SSL_PKCS12_KEYSTORE_LOCATION, testDir.getAbsolutePath());
            String truststorePath = ResourceUtil
                .getResourceAsFile(SSL_PKCS12_TRUSTSTORE_LOCATION, testDir.getAbsolutePath());

            final File keystoreFile=new File(keystorePath);
            final File truststoreFile=new File(truststorePath);

            assertTrue("Keystore should exist", keystoreFile.exists());
            assertTrue("Truststore should exist", truststoreFile.exists());

            int fileListCount = testDir.listFiles().length;
            ClientConfig config = ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(ENDPOINT)
                .setEnvironment(ENVIRONMENT)
                .setTenant(TENANT)
                .setTempDir(testDir.getAbsolutePath())
                .setDisableTemporarySecurityFile(true)
                .setSslConfig(SslConfig.newBuilder()
                    .setKeyPassword(SSL_KEY_PASSWORD)
                    .setKeystoreLocation(keystorePath)
                    .setKeystorePassword(SSL_KEYSTORE_PASSWORD)
                    .setTruststoreLocation(truststorePath)
                    .setTruststorePassword(SSL_TRUSTSTORE_PASSWORD)
                    .setKeystoreType(SslConfig.KeystoreType.PKCS12)
                    .setTruststoreType(SslConfig.TruststoreType.PKCS12)
                    .build())
                .build();

            assertTrue("Temporary directory should exist.", testDir.exists());

            AxualClient axualClient = new AxualClient(config);

            assertEquals(SslConfig.KeystoreType.PKCS12, axualClient.getConfig().getSslConfig().getKeystoreType());
            assertEquals(SslConfig.TruststoreType.PKCS12, axualClient.getConfig().getSslConfig().getTruststoreType());

            assertEquals("Temporary directory should not contain any security file", fileListCount, testDir.listFiles().length);
            axualClient.close();

            assertTrue("Original keystore file should not delete after resource cleanup.", new File(keystorePath).exists());
            assertTrue("Original truststore file should not delete after resource cleanup.", new File(truststorePath).exists());
        } finally {
            // delete temp dir
            deleteTempDir(testDir);
        }
    }
}
