package io.axual.common.test;

/*-
 * ========================LICENSE_START=================================
 * axual-common-test
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Class contains the utility method for test cases
 */
public class TestUtils {
    private static final Logger LOG = LoggerFactory.getLogger(TestUtils.class);

    @SuppressWarnings("unchecked")
    public static final String DEFAULT_PASSWORD = "notsecret";
    public static final String KEYSTORE_JKS_PATH = "/ssl/axual.client.keystore.jks";
    public static final String KEYSTORE_PEM_PATH = "/ssl/axual.client.keystore.pem";
    public static final String TRUSTSTORE_JKS_PATH = "/ssl/axual.client.truststore.jks";
    public static final String TRUSTSTORE_PEM_PATH = "/ssl/axual.client.truststore.pem";

    private TestUtils() {
        // All methods are static
    }

    /**
     *
     */
    public static File createDirectoryAndFile(String dir, String fileName) throws IOException {
        return createFile(createDirectory(dir).getAbsolutePath(), fileName).getParentFile();
    }

    /**
     *
     */
    public static File createDirectory(String dirPath) throws IOException {
        if (dirPath == null || dirPath.trim().isEmpty()) {
            throw new IOException("Unable to create directory; Reason: Directory path found null.");
        }
        File file = new File(dirPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    /**
     *
     */
    public static File createFile(String dir, String fileName) throws IOException {
        if (dir == null || dir.trim().isEmpty() || fileName == null || fileName.trim().isEmpty()) {
            throw new IOException("Unable to create file; Reason: Directory or File name is either null or empty.");
        }
        File file = new File(dir, fileName);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (!file.createNewFile()) {
            throw new IOException("Unable to create file");
        }
        return file;
    }

    /**
     *
     */
    public static void deleteDirectory(File dir) throws IOException {
        if (dir != null && dir.exists()) {
            // delete all directory files
            for (File file : dir.listFiles()) {
                Files.delete(file.toPath());
            }
            Files.delete(dir.toPath());
        }
    }

    public static InputStream getResourceAsStream(String name) {
        return TestUtils.class.getClassLoader().getResourceAsStream(name);
    }

    /**
     * Utility method that retrieves absolute path of resource file.
     * @param resourceName path of file from resources of this module.
     * @return path that can be plugged into Kafka configurations.
     */
    public static String getAbsolutePath(String resourceName) {
        Properties props = new Properties();
        try {
            props.load(getResourceAsStream("properties-from-pom.properties"));
        } catch (IOException e) {
            LOG.error(String.format("Could not get path of file %s", resourceName), e);
            return resourceName;
        }
        final String base = props.getProperty("common.resources.basedir");
        return base + "/" + resourceName;
    }

    /**
     * Utility method that retrieves contents of a file as String.
     * @param resourceName path of file from resources of this module.
     * @return String contents of file.
     */
    public static String getFileContentsAsString(String resourceName) {
        try {
            Path path = Paths.get(resourceName);
            return new String(Files.readAllBytes(path));
        } catch (IOException e) {
            LOG.error(String.format("Could not get contents of file %s", resourceName), e);
            return resourceName;
        }
    }
}
