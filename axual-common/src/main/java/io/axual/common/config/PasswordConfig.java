package io.axual.common.config;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

/**
 * PasswordConfig is used to hide the real password value in configurations.
 * {@link #toString()} will return a fixed mask.
 * {@link #getValue()} will return the real password stored.
 */
public class PasswordConfig {

    private final String value;

    /**
     * Construct a Password wrapper
     *
     * @param value the password to wrap
     */
    public PasswordConfig(String value) {
        this.value = value;
    }

    /**
     * Return the real password
     *
     * @return the password
     */
    public String getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PasswordConfig) {
            return value.equals(((PasswordConfig) obj).value);
        }
        return false;
    }

    @Override
    public String toString() {
        return "******";
    }
}
