# Axual Common Tests

This module contains code that is shared between test modules.
If more than one test modules have a use for a method or class, then it should be moved to this 
module.

This module should be included in `test` scope only.