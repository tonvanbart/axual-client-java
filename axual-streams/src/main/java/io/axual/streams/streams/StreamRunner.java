package io.axual.streams.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.StreamPartitioner;
import org.apache.kafka.streams.state.QueryableStoreType;
import org.apache.kafka.streams.state.StreamsMetadata;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;

import io.axual.client.config.DeliveryStrategy;
import io.axual.client.janitor.Janitor;
import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.axual.AxualStreams;
import io.axual.streams.proxy.axual.AxualStreamsConfig;
import io.axual.streams.proxy.generic.proxy.StreamsProxy;
import io.axual.streams.proxy.wrapped.WrappedStreamsConfig;

@InterfaceStability.Evolving
public class StreamRunner extends Janitor.ManagedCloseable implements StreamsProxy {
    private final AxualStreams streams;

    public StreamRunner(final ClientConfig clientConfig, final StreamRunnerConfig config) {
        Map<String, Object> configs = KafkaUtil.getKafkaConfigs(clientConfig);

        configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, config.getDeliveryStrategy() == DeliveryStrategy.AT_LEAST_ONCE ? "earliest" : "latest");
        configs.put(ProducerConfig.BATCH_SIZE_CONFIG, config.getBatchSize());
        configs.put(ProducerConfig.LINGER_MS_CONFIG, config.getLingerMs());
        configs.put(ProducerConfig.ACKS_CONFIG, config.getDeliveryStrategy() == DeliveryStrategy.AT_LEAST_ONCE ? "-1" : "0");
        configs.put(StreamsConfig.REQUEST_TIMEOUT_MS_CONFIG, config.getRequestTimeoutMs());

        configs.put(WrappedStreamsConfig.TOPOLOGY_FACTORY_CONFIG, config.getTopologyFactory());
        configs.put(WrappedStreamsConfig.UNCAUGHT_EXCEPTION_HANDLER_FACTORY_CONFIG, config.getUncaughtExceptionHandlerFactory());
        configs.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, config.getDefaultKeySerde());
        configs.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, config.getDefaultValueSerde());

        configs.put(StreamsConfig.TOPOLOGY_OPTIMIZATION, config.getOptimizeTopology() ? StreamsConfig.OPTIMIZE : StreamsConfig.NO_OPTIMIZATION);

        configs.put(AxualStreamsConfig.CHAIN_CONFIG, config.getProxyChain());
        streams = new AxualStreams(configs);
    }

    @Override
    public final Map<String, Object> getConfigs() {
        return streams.getConfigs();
    }

    @Override
    public final Object getConfig(String key) {
        return streams.getConfig(key);
    }

    @Override
    public void setStateListener(KafkaStreams.StateListener listener) {
        streams.setStateListener(listener);
    }

    @Override
    public KafkaStreams.State state() {
        return streams.state();
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return streams.metrics();
    }

    @Override
    public void start() {
        streams.start();
    }

    @Override
    public void stop() {
        streams.stop();
    }

    @Override
    public void close() {
        streams.close();
        super.close();
    }

    @Override
    public void close(Duration timeout) {
        streams.close(timeout);
        super.close();
    }

    @Override
    public void cleanUp() {
        streams.cleanUp();
    }

    @Override
    public void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler eh) {
        streams.setUncaughtExceptionHandler(eh);
    }

    @Override
    public Collection<StreamsMetadata> allMetadata() {
        return streams.allMetadata();
    }

    @Override
    public Collection<StreamsMetadata> allMetadataForStore(String storeName) {
        return streams.allMetadataForStore(storeName);
    }

    @Override
    public <K> StreamsMetadata metadataForKey(String storeName, K key, Serializer<K> keySerializer) {
        return streams.metadataForKey(storeName, key, keySerializer);
    }

    @Override
    public <K> StreamsMetadata metadataForKey(String storeName, K key, StreamPartitioner<? super K, ?> partitioner) {
        return streams.metadataForKey(storeName, key, partitioner);
    }

    @Override
    public <T> T store(String storeName, QueryableStoreType<T> queryableStoreType) {
        return streams.store(storeName, queryableStoreType);
    }
}
