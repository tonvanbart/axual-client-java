package io.axual.streams.proxy.generic.builder;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class NameFixingDeserializer<T> implements Deserializer<T> {
    private final Deserializer<T> deserializer;
    private final String topic;

    public NameFixingDeserializer(Deserializer<T> deserializer, String topic) {
        this.deserializer = deserializer;
        this.topic = topic;
    }

    @Override
    public T deserialize(String topic, byte[] data) {
        return deserializer.deserialize(this.topic, data);
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        deserializer.configure(configs, isKey);
    }

    @Override
    public T deserialize(String topic, Headers headers, byte[] data) {
        return deserializer.deserialize(this.topic, headers, data);
    }

    @Override
    public void close() {
        deserializer.close();
    }
}
