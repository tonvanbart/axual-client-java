package io.axual.client.proxy.logging.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

import io.axual.client.proxy.callback.core.MethodCall;
import io.axual.client.proxy.callback.core.MethodCallFactory;

public class LoggingMethodCallFactory implements MethodCallFactory {
    private final LevelLogger log;
    private final LoggingConfig config;

    public LoggingMethodCallFactory(LoggingConfig config, Class logClass) {
        this.config = config;
        log = getLogger(config.getLevel(), logClass);
    }

    private LevelLogger getLogger(LogLevel level, Class logClass) {
        final Logger logger = LoggerFactory.getLogger(logClass.getName());

        switch (level) {
            case TRACE:
                return new TraceLogger(logger);
            case DEBUG:
                return new DebugLogger(logger);
            case INFO:
                return new InfoLogger(logger);
            case WARN:
                return new WarnLogger(logger);
            case ERROR:
                return new ErrorLogger(logger);
            default:
                return new InfoLogger(logger);
        }
    }

    @Override
    public MethodCall create(UUID callId, Object object, String method) {
        return new LoggingMethodCall(config, log, callId, method);
    }
}
