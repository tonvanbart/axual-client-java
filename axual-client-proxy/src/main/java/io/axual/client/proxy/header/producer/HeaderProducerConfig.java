package io.axual.client.proxy.header.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArraySerializer;

import java.util.Map;

import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.generic.producer.ProducerProxy;

public class HeaderProducerConfig<K, V> extends BaseClientProxyConfig<ProducerProxy<K, V>> {
    public static final String BACKING_FACTORY_CONFIG = "headerproducer.backing.factory";

    HeaderProducerConfig(Map<String, Object> configs) {
        super(configs, BACKING_FACTORY_CONFIG);

        // Parse serializers
        Object keySerializer = configs.getOrDefault(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());
        Object valueSerializer = configs.getOrDefault(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());

        // Insert the HeaderProducerSerializer in the serialization chain
        putDownstream(HeaderProducerSerializer.BACKING_KEY_SERIALIZER_CONFIG, keySerializer);
        putDownstream(HeaderProducerSerializer.BACKING_VALUE_SERIALIZER_CONFIG, valueSerializer);
        putDownstream(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, HeaderProducerSerializer.class);
        putDownstream(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, HeaderProducerSerializer.class);
    }
}
