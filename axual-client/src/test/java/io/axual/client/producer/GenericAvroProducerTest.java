package io.axual.client.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.generic.GenericRecord;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import io.axual.client.config.GenericAvroProducerConfig;
import io.axual.client.producer.avro.GenericAvroProducer;
import io.axual.client.producer.generic.GenericProducer;
import io.axual.client.producer.generic.ProduceJob;
import io.axual.client.producer.generic.ProducerWorker;
import io.axual.client.producer.generic.ProducerWorkerManager;
import io.axual.common.config.ClientConfig;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(PowerMockRunner.class)
@PrepareForTest({GenericAvroProducer.class, ProducerWorkerManager.class})
public class GenericAvroProducerTest {

    @Mock
    ProducerWorker producerWorker;

    @Captor
    ArgumentCaptor<ProducerMessage> producerMessageArgumentCaptor;

    @Captor
    ArgumentCaptor<ProduceCallback> produceCallbackArgumentCaptor;

    @Mock
    ClientConfig clientConfig;

    @Mock
    GenericAvroProducerConfig producerConfig;

    @Mock
    ProducerWorkerManager producerWorkerManager;

    @Mock
    ProducerMessage producerMessage;

    @Mock
    ProduceCallback produceCallback;

    @Mock
    Future<ProducedMessage<GenericRecord, GenericRecord>> producedMessageFuture;

    GenericAvroProducer genericProducer;

    @Before
    public void setup() throws Exception {
        PowerMockito.doReturn(producerWorker).when(producerWorkerManager, "claimWorker", eq(clientConfig), any(GenericProducer.class), eq(producerConfig));
        genericProducer = Mockito.spy(new GenericAvroProducer<>(clientConfig, producerConfig, producerWorkerManager));
    }

    @Test
    public void construction() {
        Object worker = Whitebox.getInternalState(genericProducer, "worker");
        assertEquals(producerWorker, worker);
    }

    @Test
    public void produce_without_callback() {
        doReturn(producedMessageFuture).when(genericProducer).produce(any(ProducerMessage.class), any(ProduceCallback.class));

        genericProducer.produce(producerMessage);

        verify(genericProducer, times(1)).produce(producerMessageArgumentCaptor.capture(), produceCallbackArgumentCaptor.capture());

        assertEquals(producerMessageArgumentCaptor.getValue(), producerMessage);
        assertNull(produceCallbackArgumentCaptor.getValue());
    }

    @Test
    public void produce_with_callback() {
        //mock the behaviour of the inner code of the method to test
        doReturn(producedMessageFuture).when(producerWorker).queueJob(any(ProduceJob.class));

        //call the method to test
        genericProducer.produce(producerMessage, produceCallback);

        //capture the args
        verify(genericProducer, times(1)).produce(producerMessageArgumentCaptor.capture(), produceCallbackArgumentCaptor.capture());

        //check the proper calling signature
        assertEquals(producerMessage, producerMessageArgumentCaptor.getValue());
        assertEquals(produceCallback, produceCallbackArgumentCaptor.getValue());
    }

    @Test
    public void close() throws Exception {
        final AtomicInteger releaseCounter = new AtomicInteger(0);
        PowerMockito.doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                        releaseCounter.getAndIncrement();
                        return null;
                    }
                }
        ).when(producerWorkerManager, "releaseWorker", any(GenericProducer.class));

        genericProducer.close();
        assertEquals(1, releaseCounter.get());
    }
}
