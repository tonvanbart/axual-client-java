package io.axual.client.producer.avro;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.generic.GenericContainer;

import io.axual.client.config.BaseProducerConfig;
import io.axual.client.producer.generic.GenericProducer;
import io.axual.client.producer.generic.ProducerWorkerManager;
import io.axual.common.config.ClientConfig;

/**
 * Class used for all the producers, containing all the producer specific methods.
 *
 * @param <K> Type of the Key to be produced
 * @param <V> Type of the Value to be produced
 */
public class GenericAvroProducer<K extends GenericContainer, V extends GenericContainer> extends GenericProducer<K, V> {
    public GenericAvroProducer(ClientConfig clientConfig, BaseProducerConfig<K, V> producerConfig, ProducerWorkerManager producerWorkerManager) {
        super(clientConfig, producerConfig, producerWorkerManager, AvroProduceJob::new);
    }
}
