.PHONY: maven-install
maven-install:
	docker run --rm -it -v $(CURDIR):/usr/src/mymaven -v $(HOME)/.m2:/root/.m2 $ -w /usr/src/mymaven maven:3.3-jdk-8 mvn clean install

.PHONY: test
test:
	echo "test in $(CURDIR) home is $(HOME)"

.PHONY: maven-interactive
maven-interactive:
	docker run --rm -it -v $(CURDIR):/usr/src/mymaven -v $(HOME)/.m2:/root/.m2 -w /usr/src/mymaven maven:3.3-jdk-8 bash


