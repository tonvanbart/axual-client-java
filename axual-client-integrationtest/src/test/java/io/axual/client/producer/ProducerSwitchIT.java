package io.axual.client.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.consumer.Processor;
import io.axual.client.test.Random;
import io.axual.common.config.ClientConfig;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.DualClusterPlatformUnit;

import static com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class ProducerSwitchIT {
    private static final Logger LOG = LoggerFactory.getLogger(ProducerSwitchIT.class);
    private static final String STREAM = "general-random-ProducerSwitchIT";
    private static final long TTL = 5000;
    private final long DISTRIBUTOR_TIMEOUT = TTL;
    private final long DISTRIBUTOR_DISTANCE = 1;

    @Rule
    public DualClusterPlatformUnit platform = new DualClusterPlatformUnit(true)
            .addStream(new StreamConfig()
                    .setName(STREAM)
                    .setKeySchema(Random.getClassSchema())
                    .setValueSchema(Random.getClassSchema())
                    .setPartitions(1));

    @Before
    public void prepare() {
        platform.instance().getDiscoveryUnit().setTtl(TTL);
        platform.instance().getDiscoveryUnit().setDistributorTimeout(DISTRIBUTOR_TIMEOUT);
        platform.instance().getDiscoveryUnit().setDistributorDistance(DISTRIBUTOR_DISTANCE);
    }

    @Test
    public void at_least_once_keeping_order_producer_cluster_switch() throws InterruptedException {
        // Test that a producer switches to another cluster when directed to do so by the Discovery API
        // Scenario:
        // - Set up Verifier as consumer on Cluster B
        // - Create a switching producer, direct it to A initially and start producing
        // - When 5 messages are produced/received on A, switch the producer to B
        // - When 5 messages are received on B, stop the producer
        // - Verify the order of messages received is as produced

        final InstanceUnit instance = platform.instance();

        // Create queue for receiving messages
        final Queue<ConsumerMessage<Random, Random>> verifierQueue = new LinkedBlockingQueue<>();
        final List<Random> produced = new LinkedList<>();

        // Produce counter and producer stop flag
        final AtomicLong produceCounter = new AtomicLong(0);
        final AtomicBoolean producerFinished = new AtomicBoolean(false);

        final ClientConfig verifierConfig = instance.getClientConfig("verifier", platform.clusterB());
        final ClientConfig producerConfig = instance.getClientConfig("producerToTest", platform.clusterA());

        try (final AxualClient verifierClient = new AxualClient(verifierConfig);
             final AxualClient producerClient = new AxualClient(producerConfig)) {

            SpecificAvroConsumerConfig<Random, Random> specificConsumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                    .setStream(STREAM)
                    .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                    .build();
            SpecificAvroProducerConfig<Random, Random> specificProducerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                    .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                    .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                    .build();

            // Create a queue for verifying messages
            Processor<Random, Random> verifierProcessor = new EnqueueProcessor<>("Cluster B", verifierQueue, true);

            // Create producer on Cluster A and two verifier consumers
            try (final Consumer verifier = verifierClient.buildConsumer(specificConsumerConfig, verifierProcessor);
                 final Producer<Random, Random> producer = producerClient.buildProducer(specificProducerConfig)) {

                verifier.startConsuming();

                final Thread producerThread = new Thread(() -> {
                    Thread.currentThread().setName("ProducerThread");
                    while (!producerFinished.get()) {
                        final Random r = Random.newBuilder()
                                .setRandom(String.valueOf(produceCounter.get()))
                                .build();

                        LOG.info("Producing: " + r.getRandom());
                        ProducerMessage<Random, Random> message = ProducerMessage.<Random, Random>newBuilder()
                                .setStream(STREAM)
                                .setKey(r)
                                .setValue(r)
                                .build();
                        try {
                            Future<ProducedMessage<Random, Random>> future =
                                    producer.produce(message, new ProduceCallback<Random, Random>() {
                                        @Override
                                        public void onComplete(ProducedMessage<Random, Random> message) {
                                            LOG.info("Message " + message.getMessage().getKey().getRandom() + " produced successfully on cluster " + message.getCluster());
                                            produced.add(message.getMessage().getValue());
                                        }

                                        @Override
                                        public void onError(ProducerMessage<Random, Random> message, ExecutionException exception) {
                                            LOG.info("Error producing message " + message.getKey().getRandom(), exception);
                                        }
                                    });
                            if (future != null) {
                                future.get();
                                produceCounter.incrementAndGet();
                            } else {
                                LOG.warn("Produce call did not return a future");
                            }
                        } catch (InterruptedException | ExecutionException e) {
                            // Ignore
                        }

                        sleepUninterruptibly(200, MILLISECONDS);
                    }
                });
                producerThread.start();

                // Wait until we received 5 or more messages on Cluster B
                await("Producing initial messages")
                        .atMost(60, SECONDS)
                        .until(() -> verifierQueue.size() >= 5);

                // Switch to Cluster B
                LOG.info("Before switch command");
                instance.directApplicationTo(producerClient.getConfig(), platform.clusterB(), true);
                long messagesProducedOnA = produced.size();
                LOG.info("After switch command");

                // Wait until we received 5 or more messages on Cluster B
                await("Producing after switch")
                        .atMost(instance.getDiscoveryUnit().getDistributorTimeout()
                                * (instance.getDiscoveryUnit().getDistributorDistance() + 1), MILLISECONDS)
                        .pollInterval(1, SECONDS)
                        .until(() -> {
                            LOG.info(String.format("Producing after switch: Queue size %d", verifierQueue.size()));
                            return verifierQueue.size() > messagesProducedOnA + 5;
                        });

                LOG.info("Stopping Producer");
                producerFinished.set(true);
                LOG.info("Joining producer thread");
                producerThread.join();
                LOG.info("Producer stopped, sent {} messages", produced.size());

                // Wait for all messages to be consumed
                await("Waiting for consumers to catch up")
                        .atMost(30, SECONDS)
                        .pollInterval(1, SECONDS)
                        .until(() -> produced.size() == verifierQueue.size());


                // Stop verifier
                verifier.stopConsuming();
            }
        }

        // Output consumed messages
        LOG.info("Produced messages: " + produced.stream().map(k -> k.getRandom()).collect(joining(", ")));
        LOG.info("Consumed messages: " + verifierQueue.stream().map(kv -> kv.getKey().getRandom()).collect(joining(", ")));
        final List<Long> producedLong = produced.stream().map(k -> Long.valueOf(k.getRandom())).distinct().collect(toList());
        final List<Long> result = verifierQueue.stream().map(kv -> Long.valueOf(kv.getValue().getRandom())).distinct().collect(toList());

        // Check that all messages produced are consumed
        assertThat("Incorrent number of items received in verification queue", result, hasSize(producedLong.size()));
        assertThat("Items are not in correct order", result, contains(producedLong.toArray()));
    }


    @Test
    public void at_least_once_keeping_order_producer_should_maintain_order_on_cluster_switch() throws
            InterruptedException {
        // Test that a Producer switches correctly to another cluster when directed to do so by the Discovery API.
        // Correctly means that all messages produced on B are in the same order as the Producer produced
        // them.
        // Scenario:
        // - Set up a consumer on Cluster B
        // - Set up a Distributor emulator, that copies messages from Cluster A to B
        // - Start a Producer that initially produces messages to Cluster A
        // - Wait until at least 5 messages are consumed from B
        // - Pause the distributor to make it to build up a backlog of messages
        // - Wait until at least 5 more messages are produced
        // - Switch the Producer to B
        // - Wait for some time smaller than DistributorTimeout
        // - Make Distributor continue again
        // - Wait for Producer to produce 5 more messages on B (should come after distributed messages)
        // - Stop all processes
        // - Verify the order of messages received on B

        final InstanceUnit instance = platform.instance();
        final AtomicBoolean producerFinished = new AtomicBoolean(false);
        final AtomicLong produceCounter = new AtomicLong(0L);
        final Queue<ConsumerMessage<Random, Random>> verifierQueue = new LinkedBlockingQueue<>();
        final List<Random> produced = new LinkedList<>();

        ClientConfig verifierConfig = instance.getClientConfig("verifier", platform.clusterA());
        ClientConfig producerConfig = instance.getClientConfig("producerToTest", platform.clusterA());

        try (final AxualClient consumerClient = new AxualClient(verifierConfig);
             final AxualClient producerToTestClient = new AxualClient(producerConfig)) {

            // Create and start consumer with EnqueueProcessor for validation, consumes from Cluster B
            SpecificAvroConsumerConfig<Random, Random> specificConsumerConfig = SpecificAvroConsumerConfig.<Random, Random>builder()
                    .setStream(STREAM)
                    .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                    .build();
            SpecificAvroProducerConfig<Random, Random> specificProducerConfig = SpecificAvroProducerConfig.<Random, Random>builder()
                    .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                    .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                    .build();

            try (final Consumer verifier = consumerClient.buildConsumer(specificConsumerConfig, new EnqueueProcessor<>("Cluster B", verifierQueue));
                 final Producer<Random, Random> producer = producerToTestClient.buildProducer(specificProducerConfig)) {

                // Start the verifying consumer
                verifier.startConsuming();

                // Create Client and Producer with settings for Cluster A, produce AT LEAST ONCE KEEPING ORDER, Max retries
                final Thread producerThread = new Thread(() -> {
                    Thread.currentThread().setName("ProducerThread");
                    while (!producerFinished.get()) {
                        final Random r = Random.newBuilder()
                                .setRandom(String.valueOf(produceCounter.get()))
                                .build();

                        LOG.info("Producing: " + r.getRandom());
                        final AtomicBoolean success = new AtomicBoolean(false);

                        // Create a message to be produced
                        ProducerMessage<Random, Random> producerMessage = ProducerMessage.<Random, Random>newBuilder()
                                .setStream(STREAM)
                                .setKey(r)
                                .setValue(r)
                                .build();

                        try {
                            // Produce and provide a callback
                            Future<ProducedMessage<Random, Random>> future = producer.produce(producerMessage, new ProduceCallback<Random, Random>() {
                                @Override
                                public void onComplete(ProducedMessage<Random, Random> message) {
                                    LOG.info("Message " + message.getMessage().getKey().getRandom() + " produced successfully on cluster " + message.getCluster());
                                    produced.add(message.getMessage().getValue());
                                }

                                @Override
                                public void onError(ProducerMessage<Random, Random> message, ExecutionException exception) {
                                    LOG.info("Error producing message " + message.getKey().getRandom(), exception);
                                }
                            });
                            if (future != null) {
                                future.get();
                                success.set(true);
                                produceCounter.incrementAndGet();
                            } else {
                                success.set(false);
                                LOG.warn("Produce call did not return a future");
                            }
                        } catch (InterruptedException | ExecutionException e) {
                            // Ignore
                            success.set(false);
                        }

                        // Wait for the result callback and output success
                        LOG.info(String.format("Produce of Random %s %s", r.getRandom(), success.get() ? "succeeded" : "failed"));
                        sleepUninterruptibly(200, MILLISECONDS);
                    }
                });
                producerThread.start();

                // wait until Received messages equal or greater than 5
                await("Producing initial messages")
                        .atMost(60, SECONDS)
                        .until(() -> verifierQueue.size() >= 5);

                // Stop distributing for a bit, to verify that the Producer waits while switching
                final long countBeforeDistributionPause = produceCounter.get();
                platform.instance().pauseDistribution();
                await("Producing without distributing")
                        .atMost(30, SECONDS)
                        .until(() -> produced.size() > countBeforeDistributionPause + 5);

                LOG.info("Switching");
                instance.directApplicationTo(producerToTestClient.getConfig(), platform.clusterB());

                // Sleep after switch to make sure that the producer is waiting
                sleepUninterruptibly(DISTRIBUTOR_TIMEOUT / 2, MILLISECONDS);
                LOG.info(String.format("Produce counter : %d Produced :  %d Consumed size %d", produceCounter.get(), produced.size(), verifierQueue.size()));

                // Resume distribution
                platform.instance().resumeDistribution();
                LOG.info("Distributing again");

                // Producer waits until all messages produced on A are distributed to B
                await("Distributor to catch up")
                        .atMost(instance.getDiscoveryUnit().getDistributorTimeout()
                                * (instance.getDiscoveryUnit().getDistributorDistance() + 1), MILLISECONDS)
                        .pollInterval(1, SECONDS)
                        .until(() -> {
                            LOG.info(String.format("Produce counter : %d Produced :  %d Consumed size %d", produceCounter.get(), produced.size(), verifierQueue.size()));
                            return produced.size() == verifierQueue.size();
                        });

                // Wait for the producer to produce 5 more messages
                final long countAfterDistributorCaughtUp = produced.size();
                await("Producing after switch")
                        .atMost(30, SECONDS)
                        .pollInterval(1, SECONDS).until(() -> {
                    LOG.info(String.format("Producing after switch: Produced = %d - (Count[0]+5) = %d", produced.size(), countAfterDistributorCaughtUp + 5));
                    return produced.size() > countAfterDistributorCaughtUp + 5;
                });

                // Stop the producer and wait for the thread to be fully stopped (no produces anymore)
                LOG.info("Stopping Producer");
                // Stop producing new messages
                producerFinished.set(true);
                LOG.info("Joining Producer thread");
                producerThread.join();
                LOG.info("Producer stopped, sent {} messages", produced.size());

                // Wait for the producer to finish
                sleepUninterruptibly(100, MILLISECONDS);
                await("Allow consumer to finish")
                        .atMost(30, SECONDS)
                        .pollInterval(1, SECONDS)
                        .until(() -> {
                            LOG.info(String.format("Waiting for verifier to catch up: ProduceCounter = %d - Verifier queue size = %d", produceCounter.get(), verifierQueue.size()));
                            return produced.size() == verifierQueue.size();
                        });
            }
        }

        // Verify the outcomes
        LOG.info("Produced messages: " + produced.stream().map(k -> k.getRandom()).collect(joining(", ")));
        LOG.info("Consumed messages: " + verifierQueue.stream().map(kv -> kv.getKey().getRandom()).collect(joining(", ")));
        final List<Long> producedLong = produced.stream().map(k -> Long.valueOf(k.getRandom())).distinct().collect(toList());
        final List<Long> result = verifierQueue.stream().map(kv -> Long.valueOf(kv.getValue().getRandom())).distinct().collect(toList());

        // Check that all messages produced are consumed
        assertThat("Incorrent number of items received in verification queue", result, hasSize(producedLong.size()));
        assertThat("Items are not in correct order", result, contains(producedLong.toArray()));
    }
}
