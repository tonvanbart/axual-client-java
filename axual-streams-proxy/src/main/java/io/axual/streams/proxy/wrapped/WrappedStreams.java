package io.axual.streams.proxy.wrapped;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.processor.StreamPartitioner;
import org.apache.kafka.streams.state.QueryableStoreType;
import org.apache.kafka.streams.state.StreamsMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

import io.axual.common.annotation.InterfaceStability;
import io.axual.streams.proxy.generic.factory.OptimizedTopologyFactory;
import io.axual.streams.proxy.generic.proxy.StreamsProxy;

@InterfaceStability.Evolving
public class WrappedStreams implements StreamsProxy {
    private static final Logger LOG = LoggerFactory.getLogger(WrappedStreams.class);

    private final WrappedStreamsConfig config;
    private final KafkaStreams kafkaStreams;

    public WrappedStreams(Map<String, Object> configs) {
        config = new WrappedStreamsConfig(configs);

        // Get the property set for KafkaStreams
        Properties properties = new Properties();
        properties.putAll(config.getDownstreamConfigs());

        // Create the streams topology
        OptimizedTopologyFactory topologyFactory = OptimizedTopologyFactory.Wrapper.ensureOptimizedTopologyFactory(config.getTopologyFactory());
        Topology topology = topologyFactory.create(new WrappedStreamsBuilder(), properties);
        LOG.info("{}", topology.describe());

        // Create the new KafkaStreams
        kafkaStreams = new KafkaStreams(topology, properties, config.getClientSupplier());

        // If set, apply the UncaughtExceptionHandler
        if (config.getUncaughtExceptionHandlerFactory() != null) {
            Thread.UncaughtExceptionHandler handler = config.getUncaughtExceptionHandlerFactory().create(this);
            setUncaughtExceptionHandler(handler);
        }
    }

    @Override
    public void setStateListener(KafkaStreams.StateListener listener) {
        kafkaStreams.setStateListener(listener);
    }

    @Override
    public KafkaStreams.State state() {
        return kafkaStreams.state();
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return kafkaStreams.metrics();
    }

    @Override
    public void start() {
        kafkaStreams.start();
    }

    @Override
    public void stop() {
        close();
    }

    @Override
    public Map<String, Object> getConfigs() {
        return config.getConfigs();
    }

    @Override
    public Object getConfig(String key) {
        return config.getConfigs().get(key);
    }

    @Override
    public void close() {
        kafkaStreams.close();
    }

    @Override
    public void close(Duration timeout) {
        kafkaStreams.close(timeout);
    }

    @Override
    public void cleanUp() {
        kafkaStreams.cleanUp();
    }

    @Override
    public void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler eh) {
        kafkaStreams.setUncaughtExceptionHandler(eh);
    }

    @Override
    public Collection<StreamsMetadata> allMetadata() {
        return kafkaStreams.allMetadata();
    }

    @Override
    public Collection<StreamsMetadata> allMetadataForStore(String storeName) {
        return kafkaStreams.allMetadataForStore(storeName);
    }

    @Override
    public <K> StreamsMetadata metadataForKey(String storeName, K key, Serializer<K> keySerializer) {
        return kafkaStreams.metadataForKey(storeName, key, keySerializer);
    }

    @Override
    public <K> StreamsMetadata metadataForKey(String storeName, K key, StreamPartitioner<? super K, ?> partitioner) {
        return kafkaStreams.metadataForKey(storeName, key, partitioner);
    }

    @Override
    public <T> T store(String storeName, QueryableStoreType<T> queryableStoreType) {
        return kafkaStreams.store(storeName, queryableStoreType);
    }
}
