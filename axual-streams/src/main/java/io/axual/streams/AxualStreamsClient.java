package io.axual.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.client.janitor.Janitor;
import io.axual.client.janitor.TemporaryFile;
import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.tools.ResourceUtil;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.streams.StreamRunner;

/**
 * The AxualStreams class represents a streaming client application's connection to Axual Platform.
 * Through this class the application can create StreamRunners to define streaming topologies. The
 * general assumption is that an application only requires one instance of this class, which is
 * configured by passing a {@link ClientConfig} object to the constructor. The configuration passed
 * in is automatically applied to all created StreamRunners.
 */
@InterfaceStability.Evolving
public class AxualStreamsClient implements AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(AxualStreamsClient.class);

    private final ClientConfig config;
    private final Janitor janitor = new Janitor();

    /**
     * Instantiates a new Axual streams client with given configuration.
     *
     * @param config the configuration of the application's connection to Axual Platform.
     */
    public AxualStreamsClient(final ClientConfig config) {
        final SslConfig.Builder sslConfigBuilder = SslConfig.newBuilder(config.getSslConfig());
        if (!config.getDisableTemporarySecurityFile()) {
            if (sslConfigBuilder.getKeystoreLocation() != null) {
                LOG.debug("Creating temporary keystore file.");
                sslConfigBuilder.setKeystoreLocation(createTmpKeystore(config));
            }
            if (sslConfigBuilder.getTruststoreLocation() != null) {
                LOG.debug("Creating temporary truststore file.");
                sslConfigBuilder.setTruststoreLocation(createTmpTruststore(config));
            }
        }

        this.config = ClientConfig.newBuilder(config)
                .setSslConfig(sslConfigBuilder.build())
                .build();
    }

    /**
     * Gets the streaming client configuration.
     *
     * @return the configuration
     */
    public ClientConfig getConfiguration() {
        return config;
    }

    /**
     * This method is used to create a streamRunner. The config should contain at least a topology
     * (other config properties are optional)
     *
     * @param streamRunnerConfig An instance of the StreamRunnerConfig that contains configuration
     *                           in order to create this streamRunner
     * @return the stream runner
     */
    public StreamRunner buildStreamRunner(final StreamRunnerConfig streamRunnerConfig) {
        return janitor.register(new StreamRunner(config, streamRunnerConfig));
    }

    @Override
    public void close() {
        janitor.close();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // End of public interface of AxualStreamsClient
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private String createTmpKeystore(final ClientConfig original) {
        TemporaryFile tmpKeystore = new TemporaryFile(
                ResourceUtil.getResourceAsFile(original.getSslConfig().getKeystoreLocation(), original.getTempDir())
        );
        return janitor.register(tmpKeystore, false).getFilename();
    }

    private String createTmpTruststore(final ClientConfig original) {
        TemporaryFile tmpTruststore = new TemporaryFile(
                ResourceUtil.getResourceAsFile(original.getSslConfig().getTruststoreLocation(), original.getTempDir())
        );
        return janitor.register(tmpTruststore, false).getFilename();
    }

}
