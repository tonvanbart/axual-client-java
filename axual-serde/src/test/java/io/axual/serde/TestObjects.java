package io.axual.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.serde.utils.BitUtils;

/**
 * Class to provide test objects, also to test
 */
public final class TestObjects {
    public static final int[] VERSIONS = new int[]{SerdeConstants.VERSION_V1, SerdeConstants.VERSION_V2};

    private TestObjects() {
    }

    public static byte[] getSerializedMessage(boolean encodeHeader, int version, boolean isFirstOrderCopy, boolean isSecondOrderCopy, byte[] content) {
        if (encodeHeader) {
            int extendedHeaderSize;
            switch (version) {
                case SerdeConstants.VERSION_V1:
                    extendedHeaderSize = SerdeConstants.VERSION_V1_EXTENDED_HEADER_SIZE;
                    break;
                case SerdeConstants.VERSION_V2:
                    extendedHeaderSize = SerdeConstants.VERSION_V2_EXTENDED_HEADER_SIZE;
                    break;
                default:
                    throw new RuntimeException("Can not construct a Axual Client test message for version " + version);
            }

            // We test the correctness of the (de)serializer by setting up our own message manually.
            // This way we make sure all bytes in the message contain an expected field.
            byte[] message = new byte[SerdeConstants.HEADER_SIZE + extendedHeaderSize + content.length];
            BitUtils.putShortInt(message, SerdeConstants.MAGIC_OFFSET, SerdeConstants.MAGIC_INT);
            BitUtils.putShortInt(message, SerdeConstants.VERSION_OFFSET, version);
            BitUtils.putInt(message, SerdeConstants.EXTENDED_HEADER_SIZE_OFFSET, extendedHeaderSize);
            message[SerdeConstants.COPY_FLAGS_OFFSET] = 0;

            if ((version == SerdeConstants.VERSION_V1 && !isFirstOrderCopy) ||
                    (version >= SerdeConstants.VERSION_V2 && isFirstOrderCopy)) {
                message[SerdeConstants.COPY_FLAGS_OFFSET] = BitUtils.setBit(message[SerdeConstants.COPY_FLAGS_OFFSET], SerdeConstants.MESSAGE_FLAG_FIRST_ORDER_COPY_POSITION);
            }
            if (isSecondOrderCopy) {
                message[SerdeConstants.COPY_FLAGS_OFFSET] = BitUtils.setBit(message[SerdeConstants.COPY_FLAGS_OFFSET], SerdeConstants.MESSAGE_FLAG_SECOND_ORDER_COPY_POSITION);
            }

            if (version >= SerdeConstants.VERSION_V2) {
                BitUtils.putLong(message, SerdeConstants.MESSAGE_ID_MSB_OFFSET, 0x01234567);
                BitUtils.putLong(message, SerdeConstants.MESSAGE_ID_LSB_OFFSET, 0x89abcdef);
                BitUtils.putLong(message, SerdeConstants.SERIALIZATION_TIME_OFFSET, System.currentTimeMillis());
            }

            System.arraycopy(content, 0, message, SerdeConstants.HEADER_SIZE + extendedHeaderSize, content.length);

            return message;
        }

        return content;
    }
}
