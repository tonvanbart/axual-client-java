package io.axual.discovery.client.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

import io.axual.common.config.ConfigParser;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.CommonConfig;
import io.axual.discovery.client.DiscoveryConfig;

public class DiscoveryConfigParserV2 extends DiscoveryConfigParser {
    private static final String V2_ENDPOINT_SUFFIX = "v2";
    private static final String DISCOVERY_TENANT_PARAM = CommonConfig.TENANT;
    private static final String DISCOVERY_ENVIRONMENT_PARAM = "env";

    public DiscoveryConfigParserV2() {
        super(V2_ENDPOINT_SUFFIX);
    }

    public static DiscoveryConfig getDiscoveryConfig(ClientConfig config) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put(DISCOVERY_TENANT_PARAM, config.getTenant());
        parameters.put(DISCOVERY_ENVIRONMENT_PARAM, config.getEnvironment());

        String url = config.getEndpoint();
        String versionTwo = url.endsWith("/") ? V2_ENDPOINT_SUFFIX : "/" + V2_ENDPOINT_SUFFIX;

        return DiscoveryConfig.newBuilder()
                .setApplicationId(config.getApplicationId())
                .setApplicationVersion(config.getApplicationVersion())
                .setEndpoint(url + versionTwo)
                .setParameters(parameters)
                .setSslConfig(config.getSslConfig())
                .build();
    }

    public DiscoveryConfig parse(Map input) {
        // Parse tenant and environment
        String tenant = ConfigParser.parseStringConfig(input, CommonConfig.TENANT, true, null);
        String environment = ConfigParser.parseStringConfig(input, CommonConfig.ENVIRONMENT, false, null);

        Map<String, String> parameters = new HashMap<>();
        parameters.put(DISCOVERY_TENANT_PARAM, tenant);
        if (environment != null) {
            parameters.put(DISCOVERY_ENVIRONMENT_PARAM, environment);
        }

        return super.parse(input, parameters);
    }
}
