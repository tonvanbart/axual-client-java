# Axual Client Integration Tests

This module contains the code that tests the proxy client on some key end-to-end scenarios.
Those include: 
 * producing messages and 
 * expecting to consume the same
 * a cluster switch occurring on any of the above.
