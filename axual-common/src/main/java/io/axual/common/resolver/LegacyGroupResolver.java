package io.axual.common.resolver;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is able to resolve and unresolve patterns for Kafka consumer group ids. It is
 * implemented in much the same way as Kafka (de)serializers, by using a default constructor first
 * and calling {@link #configure(Map)} later. The parameters passed in during configure() determine
 * the behaviour of the resolver. The class implements the {@link GroupResolver} interface.
 */
public class LegacyGroupResolver extends GroupPatternResolver {
    private static final String FIXATED_LEGACY_GROUP_PATTERN = "{tenant}-{instance}-{environment}-{group}";
    private static final String[] LEGACY_GROUP_PATTERNS = new String[]{
            "{group}-{tenant}-{environment}-{domain}-{topic}",
            "{group}-{tenant}-{environment}-{topic}",
            "{group}-{tenant}-{environment}",
            FIXATED_LEGACY_GROUP_PATTERN,
            "{tenant}-{environment}-{group}"
    };
    private static final String RESOLVED_GROUP_ID = "resolvedgroup";
    private Map<String, Object> configs;

    @Override
    public void configure(Map<String, ?> configs) {
        Map<String, Object> modifiedConfigs = new HashMap<>(configs);
        modifiedConfigs.put(GROUP_ID_PATTERN_CONFIG, FIXATED_LEGACY_GROUP_PATTERN);
        this.configs = Collections.unmodifiableMap(modifiedConfigs);
        super.configure(modifiedConfigs);
    }

    @Override
    public String resolve(String group) {
        // If this resolver uses the {group} pattern, then check for the resolved group id field.
        // When set we can safely assume another LegacyGroupResolver unresolved the group id before
        // and has set the field explicitly for this resolver to return as resolved group id.
        Object resolvedGroupIdConfig = configs.get(RESOLVED_GROUP_ID);
        if (resolvedGroupIdConfig instanceof String) {
            return (String) resolvedGroupIdConfig;
        }
        return super.resolve(group);
    }

    @Override
    public Map<String, String> unresolveContext(String group) {
        // This method tries different patterns to unresolve the group name. It returns the first
        // one that worked.
        for (String pattern : LEGACY_GROUP_PATTERNS) {
            Map<String, String> result = unresolvePattern(pattern, group);

            // If unresolving worked, then check if it's a valid group and return
            if (result != null && isValidLegacyGroup(result)) {
                // Set the resolved group id in the context, so that any LegacyGroupResolver that
                // needs to resolve this group id later can use the field to perform a direct
                // translation.
                result.put(RESOLVED_GROUP_ID, group);
                return result;
            }
        }

        return null;
    }

    private Map<String, String> unresolvePattern(String pattern, String group) {
        // Set up the context to unresolve
        Map<String, Object> unresolveContext = new HashMap<>(configs);
        unresolveContext.put(GROUP_ID_PATTERN_CONFIG, pattern);

        // Set up a private resolver (using the base class would override its config)
        GroupResolver resolver = new GroupPatternResolver();
        resolver.configure(unresolveContext);

        // Try to unresolve the group using the context provided
        return resolver.unresolveContext(group);
    }

    private boolean isValidLegacyGroup(Map<String, String> context) {
        return context.containsKey(DEFAULT_PLACEHOLDER_VALUE) &&
                !context.get(DEFAULT_PLACEHOLDER_VALUE).contains("distributor");
    }
}
