package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.UUID.randomUUID;

public class SslUnit {
    private static final String DEFAULT = "notsecret";

    private SslUnit() {
    }

    public static SslConfig getDefaultSslConfig() {
        return SslConfig.newBuilder()
                .setKeyPassword(new PasswordConfig(DEFAULT))
                .setKeystoreLocation(resourceAsFile("/ssl/axual.client.keystore.jks"))
                .setKeystorePassword(new PasswordConfig(DEFAULT))
                .setTruststoreLocation(resourceAsFile("/ssl/axual.client.truststore.jks"))
                .setTruststorePassword(new PasswordConfig(DEFAULT))
                .setEnableHostnameVerification(false)
                .build();
    }

    public static SslConfig getDefaultSslPKCS12Config() {
        return SslConfig.newBuilder()
                .setKeyPassword(new PasswordConfig(DEFAULT))
                .setKeystoreLocation(resourceAsFile("/ssl/axual.client.keystore.p12"))
                .setKeystorePassword(new PasswordConfig(DEFAULT))
                .setTruststoreLocation(resourceAsFile("/ssl/axual.client.truststore.p12"))
                .setTruststorePassword(new PasswordConfig(DEFAULT))
                .setTruststoreType(SslConfig.TruststoreType.PKCS12)
                .setKeystoreType(SslConfig.KeystoreType.PKCS12)
                .setEnableHostnameVerification(false)
                .build();
    }

    private static String resourceAsFile(final String resourcePath) {
        try {
            File tmp = File.createTempFile(randomUUID().toString(), null);
            tmp.deleteOnExit();

            Files.copy(PlatformUnit.class.getResourceAsStream(resourcePath), tmp.toPath(), REPLACE_EXISTING);

            return tmp.getAbsolutePath();
        } catch (IOException e) {
            throw new IllegalStateException("Can not copy resource to temporary file", e);
        }
    }
}
