package io.axual.client.producer.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

import io.axual.client.exception.ProduceFailedException;
import io.axual.client.producer.ProduceCallback;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.proxy.generic.producer.ProducerProxy;

/**
 * Class that will produce a single message to a given producer and catch incoming results.
 */
public class ProduceJob<K, V> {
    private static final Logger LOG = LoggerFactory.getLogger(ProduceJob.class);

    private final ProducerMessage<K, V> message;
    private boolean executed = false;

    private final ProduceFuture<K, V> produceFuture = new ProduceFuture<>();
    private final ProduceCallback<K, V> produceCallback;

    public ProduceJob(ProducerMessage<K, V> message, ProduceCallback<K, V> produceCallback) {
        this.message = message;
        this.produceCallback = produceCallback;
    }

    public ProduceFuture<K, V> execute(ProducerProxy<K, V> producer) {
        if (executed) {
            throw new ProduceFailedException(message.getKey(), message.getValue(), System.currentTimeMillis(), null);
        }

        executed = true;

        try {
            final long produceTime = System.currentTimeMillis();

            producer.send(message.getProducerRecord(), (metadata, exception) -> {
                if (exception != null) {
                    if (LOG.isTraceEnabled()) {
                        LOG.warn("Failed to send record with messageId {}: key={}, value={}, ", message.getMessageId(), message.getKey(), message.getValue());
                    } else {
                        LOG.warn("Failed to send record with messageId {}", message.getMessageId());
                    }
                    completeProduce(new ProduceFailedException(
                            message.getKey(), message.getValue(), produceTime, exception));
                } else {
                    LOG.trace("Successfully sent record: key={}, value={}, stream={}, partition={}, offset={}",
                            message.getKey(), message.getValue(), metadata.topic(), metadata.partition(), metadata.offset());
                    completeProduce(new GenericProducedMessage<>(message, metadata));
                }
            });
        } catch (Exception e) {
            if (LOG.isTraceEnabled()) {
                LOG.error("Error during produce to stream {}:\n  Key: {} \n  Value: {} ", message.getStream(), message.getKey(), message.getValue());
            } else {
                LOG.error("Error during produce to stream {}: ", message.getStream(), e);
            }
            completeProduce(e);
        }
        return produceFuture;
    }

    public ProduceFuture<K, V> getFuture() {
        return produceFuture;
    }

    public ProducerMessage<K, V> getMessage() {
        return message;
    }

    /**
     * Helper method to send the completion to both the future and the callback. It will be wrapped
     * in a {@link ExecutionException}
     *
     * @param cause the {@link Throwable} which caused the message produce to fail
     */
    public void completeProduce(final Throwable cause) {
        completeProduce(cause.getMessage(), cause);
    }

    /**
     * Helper method to send the completion to both the future and the callback. It will be wrapped
     * in a {@link ExecutionException}
     *
     * @param message the message which will be used for the wrapped exception
     * @param cause   the {@link Throwable} which caused the message produce to fail
     */
    public void completeProduce(final String message, final Throwable cause) {
        ExecutionException executionException = new ExecutionException(message, cause);
        if (produceCallback != null) {
            produceCallback.onError(this.message, executionException);
        }
        produceFuture.complete(executionException);
    }

    /**
     * Helper method to send the completion to both the future and the callback
     *
     * @param producedMessage the {@link ProducedMessage} containing the metadata of the produced
     *                        message
     */
    private void completeProduce(final ProducedMessage<K, V> producedMessage) {
        if (produceCallback != null) {
            produceCallback.onComplete(producedMessage);
        }
        produceFuture.complete(producedMessage);
    }
}
