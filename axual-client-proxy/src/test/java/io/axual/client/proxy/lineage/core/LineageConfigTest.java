package io.axual.client.proxy.lineage.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import io.axual.common.config.CommonConfig;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LineageConfigTest {
    private static final String APPLICATION_ID = "application.id";
    private static final String APPLICATION_VERSION = "application.version";
    private static final String SYSTEM = "system";
    private static final String TENANT = "tenant";
    private static final String INSTANCE = "instance";
    private static final String ENVIRONMENT = "environment";
    private static final String CLUSTER = "cluster";

    private Map<String, Object> lineageConfigMap = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        lineageConfigMap.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
        lineageConfigMap.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);
        lineageConfigMap.put(CommonConfig.SYSTEM_PROPERTY, SYSTEM);
        lineageConfigMap.put(CommonConfig.TENANT, TENANT);
        lineageConfigMap.put(CommonConfig.INSTANCE, INSTANCE);
        lineageConfigMap.put(CommonConfig.ENVIRONMENT, ENVIRONMENT);
        lineageConfigMap.put(CommonConfig.CLUSTER_PROPERTY, CLUSTER);
    }

    @Test
    public void getApplicationId() {
        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertEquals(APPLICATION_ID, config.getApplicationId());
    }

    @Test
    public void getApplicationVersion() {
        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertEquals(APPLICATION_VERSION, config.getApplicationVersion());
    }

    @Test
    public void getSystem() {
        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertEquals(SYSTEM, config.getSystem());
    }

    @Test
    public void getInstance() {
        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertEquals(INSTANCE, config.getInstance());
    }

    @Test
    public void getCluster() {
        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertEquals(CLUSTER, config.getCluster());
    }

    @Test
    public void getTenant() {
        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertEquals(TENANT, config.getTenant());
    }

    @Test
    public void getEnvironment() {
        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertEquals(ENVIRONMENT, config.getEnvironment());
    }

    @Test
    public void isSystemProduceEnabledUnset() {
        lineageConfigMap.remove(LineageConfig.ENABLE_SYSTEM_PRODUCE);
        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertFalse(config.isSystemProduceEnabled());
    }

    @Test
    public void isSystemProduceEnabledTrue() {
        lineageConfigMap.put(LineageConfig.ENABLE_SYSTEM_PRODUCE, Boolean.TRUE);

        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertTrue(config.isSystemProduceEnabled());
    }

    @Test
    public void isSystemProduceEnabledFalse() {
        lineageConfigMap.put(LineageConfig.ENABLE_SYSTEM_PRODUCE, Boolean.FALSE);

        LineageConfig config = new LineageConfig(lineageConfigMap);
        assertFalse(config.isSystemProduceEnabled());
    }
}
