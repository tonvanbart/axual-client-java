package io.axual.client.proxy.generic.client;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.generic.proxy.BaseProxy;
import io.axual.common.config.CommonConfig;
import io.axual.common.exception.NotSupportedException;

public abstract class BaseClientProxy<C extends BaseClientProxyConfig> extends BaseProxy<C> implements ClientProxy {
    public BaseClientProxy(C config) {
        super(config);
    }

    protected void operationNotSupported(String operation) {
        throw new NotSupportedException("Axual " + this.getClass().getSimpleName() + " does not (yet) support " + operation);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(getClass().getSimpleName());
        result.append("[");
        if (config.getConfigs().containsKey(CommonClientConfigs.CLIENT_ID_CONFIG)) {
            result.append("clientId=");
            result.append(config.getConfigs().get(CommonClientConfigs.CLIENT_ID_CONFIG));
        } else if (config.getConfigs().containsKey(CommonConfig.APPLICATION_ID)) {
            result.append("applicationId=");
            result.append(config.getConfigs().get(CommonConfig.APPLICATION_ID));
        } else if (config.getConfigs().containsKey(ConsumerConfig.GROUP_ID_CONFIG)) {
            result.append("groupId=");
            result.append(config.getConfigs().get(ConsumerConfig.GROUP_ID_CONFIG));
        } else {
            result.append("id=unknown");
        }
        result.append("]");
        return result.toString();
    }
}
