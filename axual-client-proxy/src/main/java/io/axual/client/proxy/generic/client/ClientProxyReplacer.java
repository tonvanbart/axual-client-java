package io.axual.client.proxy.generic.client;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.client.proxy.generic.config.BaseClientProxyConfig;

/**
 * Interface for classes that are able to replace a proxy.
 * @param <T> the type of proxy to replace.
 * @param <C> the config type for the proxy type.
 */
public interface ClientProxyReplacer<T extends ClientProxy, C extends BaseClientProxyConfig> {

    /**
     * Indicate if we need to do the replace.
     * @return true if the proxy should be replaced.
     */
    boolean needToReplace();

    /**
     * Replace the old proxy with a configured new instance of the same proxy type.
     * @param oldProxy the old proxy to be replaced.
     * @param config the proxy configuration.
     * @return a new, configured instance of the proxy type.
     */
    T replace(T oldProxy, C config);

    /**
     * Called if this instance will no longer be called to replace proxies. Use this to shut down
     * any resources that an implementation of this interface is using.
     */
    void close();
}
