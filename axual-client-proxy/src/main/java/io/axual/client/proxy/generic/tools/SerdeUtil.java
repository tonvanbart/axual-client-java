package io.axual.client.proxy.generic.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.wrapped.serde.WrappedDeserializerInstance;
import io.axual.client.proxy.wrapped.serde.WrappedSerializerInstance;

public class SerdeUtil {
    private SerdeUtil() {
    }

    public static Map<String, Object> addDeserializersToConfigs(Map<String, Object> configs, Deserializer<?> keyDeserializer, Deserializer<?> valueDeserializer) {
        Map<String, Object> newConfigs = new HashMap<>(configs);
        if (keyDeserializer != null) {
            newConfigs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, WrappedDeserializerInstance.class);
            newConfigs.put(WrappedDeserializerInstance.KEY_DESERIALIZER_INSTANCE_CONFIG, keyDeserializer);
        }

        if (valueDeserializer != null) {
            newConfigs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, WrappedDeserializerInstance.class);
            newConfigs.put(WrappedDeserializerInstance.VALUE_DESERIALIZER_INSTANCE_CONFIG, valueDeserializer);
        }

        return newConfigs;
    }

    public static Map<String, Object> addSerializersToConfigs(Map<String, Object> configs, Serializer<?> keySerializer, Serializer<?> valueSerializer) {
        Map<String, Object> newConfigs = new HashMap<>(configs);
        if (keySerializer != null) {
            newConfigs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, WrappedSerializerInstance.class);
            newConfigs.put(WrappedSerializerInstance.KEY_SERIALIZER_INSTANCE_CONFIG, keySerializer);
        }

        if (valueSerializer != null) {
            newConfigs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, WrappedSerializerInstance.class);
            newConfigs.put(WrappedSerializerInstance.VALUE_SERIALIZER_INSTANCE_CONFIG, valueSerializer);
        }

        return newConfigs;
    }
}
