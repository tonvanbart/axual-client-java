package io.axual.streams.config;

/*-
 * ========================LICENSE_START=================================
 * axual-streams
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.proxy.generic.factory.UncaughtExceptionHandlerFactory;

import static org.junit.Assert.assertEquals;


public class StreamRunnerConfigTest {

    private final TopologyFactory dummyTopologyFactory = builder -> null;
    private final UncaughtExceptionHandlerFactory dummyUncaughtExceptionHandlerFactory = streams -> null;

    @Test(expected = IllegalStateException.class)
    public void testBuildStreamRunnerConfig_noDeliveryStrategy() {
        StreamRunnerConfig.builder()
                .setDeliveryStrategy(null)
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuildStreamRunnerConfig_noTopologyFactory() {
        StreamRunnerConfig.builder()
                .setTopologyFactory(null)
                .build();
    }

    @Test
    public void testBuildStreamRunnerConfig_ShouldReturnDefaultOptions() {
        StreamRunnerConfig streamRunnerConfig = StreamRunnerConfig.builder()
                .setTopologyFactory(dummyTopologyFactory)
                .build();

        assertEquals(0L, streamRunnerConfig.getLingerMs());
        assertEquals(262144, (int) streamRunnerConfig.getBatchSize());
        assertEquals(40000, (int) streamRunnerConfig.getRequestTimeoutMs());
    }

    @Test
    public void testBuildStreamRunnerConfig_ShouldReturnSetOptions() {
        final long dummyLingerMs = 10L;
        final int dummyBatchSize = 131072;
        final int dummyRequestTimeoutMs = 2000;
        final String dummyTopology = "dummy-topology";
        final String dummyHandler = "DummyHandlerFactory";

        StreamRunnerConfig streamRunnerConfig = StreamRunnerConfig.builder()
                .setTopologyFactory(dummyTopologyFactory)
                .setUncaughtExceptionHandler(dummyUncaughtExceptionHandlerFactory)
                .setBatchSize(dummyBatchSize)
                .setRequestTimeoutMs(dummyRequestTimeoutMs)
                .setLingerMs(dummyLingerMs)
                .build();

        assertEquals(dummyLingerMs, streamRunnerConfig.getLingerMs());
        assertEquals(dummyBatchSize, (int) streamRunnerConfig.getBatchSize());
        assertEquals(dummyRequestTimeoutMs, (int) streamRunnerConfig.getRequestTimeoutMs());
        assertEquals(dummyTopologyFactory, streamRunnerConfig.getTopologyFactory());
        assertEquals(dummyUncaughtExceptionHandlerFactory, streamRunnerConfig.getUncaughtExceptionHandlerFactory());
    }
}
