package io.axual.client.proxy.wrapped.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.apache.kafka.common.serialization.Serializer;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;

import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.client.proxy.wrapped.generic.WrappedClientProxy;
import io.axual.client.proxy.wrapped.generic.WrappedClientProxyConfig;
import io.axual.common.tools.MapUtil;

public class WrappedProducer<K, V> extends WrappedClientProxy<ProducerProxy<K, V>, WrappedClientProxyConfig<ProducerProxy<K, V>>> implements ProducerProxy<K, V> {
    private final Producer<K, V> producer;

    public WrappedProducer(Producer<K, V> producer, Map<String, Object> configs) {
        super(new WrappedClientProxyConfig<>(configs));
        this.producer = producer;
    }

    public WrappedProducer(Map<String, Object> configs) {
        this(new KafkaProducer<>(configs), configs);
    }

    public WrappedProducer(Map<String, Object> configs, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(SerdeUtil.addSerializersToConfigs(configs, keySerializer, valueSerializer));
    }

    public WrappedProducer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public WrappedProducer(Properties properties, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(MapUtil.objectToStringMap(properties), keySerializer, valueSerializer);
    }

    @Override
    public void initTransactions() {
        producer.initTransactions();
    }

    @Override
    public void beginTransaction() {
        producer.beginTransaction();
    }

    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> map, String s) {
        producer.sendOffsetsToTransaction(map, s);
    }

    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> offsets,
        ConsumerGroupMetadata groupMetadata) throws ProducerFencedException {
        producer.sendOffsetsToTransaction(offsets, groupMetadata);
    }

    @Override
    public void commitTransaction() {
        producer.commitTransaction();
    }

    @Override
    public void abortTransaction() {
        producer.abortTransaction();
    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> producerRecord) {
        return producer.send(producerRecord);
    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> producerRecord, Callback callback) {
        return producer.send(producerRecord, callback);
    }

    @Override
    public void flush() {
        producer.flush();
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic) {
        return producer.partitionsFor(topic);
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return producer.metrics();
    }

    @Override
    public void close(Duration timeout) {
        producer.close(timeout);
    }
}
