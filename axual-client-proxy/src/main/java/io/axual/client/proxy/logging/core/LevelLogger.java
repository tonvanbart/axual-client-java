package io.axual.client.proxy.logging.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Marker;

public interface LevelLogger {
    boolean isEnabled();

    void log(String var1);

    void log(String var1, Object var2);

    void log(String var1, Object var2, Object var3);

    void log(String var1, Object... var2);

    void log(String var1, Throwable var2);

    boolean isEnabled(Marker var1);

    void log(Marker var1, String var2);

    void log(Marker var1, String var2, Object var3);

    void log(Marker var1, String var2, Object var3, Object var4);

    void log(Marker var1, String var2, Object... var3);

    void log(Marker var1, String var2, Throwable var3);
}
