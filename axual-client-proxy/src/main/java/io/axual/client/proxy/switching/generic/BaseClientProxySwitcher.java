package io.axual.client.proxy.switching.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Objects;

import io.axual.client.proxy.generic.config.DynamicClientProxyConfig;
import io.axual.client.proxy.generic.client.ClientProxy;
import io.axual.common.tools.MapUtil;
import io.axual.common.tools.SleepUtil;
import io.axual.discovery.client.DiscoveryResult;

import static io.axual.client.proxy.switching.generic.DistributorConfigs.DISTRIBUTOR_DISTANCE_CONFIG;
import static io.axual.client.proxy.switching.generic.DistributorConfigs.DISTRIBUTOR_TIMEOUT_CONFIG;

/**
 * Base class for a client proxy switcher.
 * @param <T> the type of proxy to switch.
 * @param <C> the configuration type.
 */
public abstract class BaseClientProxySwitcher<T extends ClientProxy, C extends DynamicClientProxyConfig> implements ClientProxySwitcher<T, C> {
    private static final Logger LOG = LoggerFactory.getLogger(BaseClientProxySwitcher.class);
    private static final long DEFAULT_DISTRIBUTOR_DISTANCE = 1L; //
    private static final long DEFAULT_DISTRIBUTOR_TIMEOUT = 60000L; // 60s in ms

    /**
     * {@inheritDoc}
     */
    @Override
    public T switchProxy(T oldProxy, C config, DiscoveryResult oldResult, DiscoveryResult newResult) {
        // This method switches the old proxy for a new one. The procedure is as follows:
        // 1. Close the old proxy object gracefully.
        // 2. Wait for Distributor to finish its job server-side (only if necessary).
        // 3. Initialize the new proxy and return it.

        // Gracefully close the old proxied object
        if (oldProxy != null) {
            oldProxy.close();
        }

        // Validate if we can create a new proxy based on the newly received DiscoveryResult
        if (newResult == null || newResult.getCluster() == null) {
            // No active cluster returned by DiscoveryAPI. Log a warning, as this situation should
            // only occur in rare circumstances.
            LOG.warn("Can not create a new proxy based on discovery result: {}", newResult);
            // Return null for new proxy. This will effectively cause exceptions in regular client
            // applications, which need to handle unavailability of a target cluster themselves.
            return null;
        }

        // Check to see if we are really switching to another cluster. If we're not really switching
        // to a different cluster, we can just reinitialize the proxied object without additional
        // waiting. If we do switch clusters, and oldProxy was initialized, then sleep until it's
        // safe to switch over to the new cluster.
        if (oldResult != null
                && !Objects.equals(oldResult.getCluster(), newResult.getCluster())
                && oldProxy != null) {
            // Fetch the requested time to wait for the cluster switch
            Duration switchTimeout = getSwitchTimeout(config, oldResult, newResult);
            if (switchTimeout.toMillis() > 0) {
                // Wait switchTimeout before switching to the new cluster
                LOG.info("Sleeping before switching {}, switch timeout = {}", oldProxy.getClass().getSimpleName(), switchTimeout);
                SleepUtil.sleep(switchTimeout);
                LOG.info("Sleeping of {} done", this.getClass().getSimpleName());
            }
        }

        // Create the new backing proxy
        LOG.info("Creating new backing {} with Discovery API result: {}", config.getProxyType(), newResult);
        T result = createProxyObject(config, newResult);
        LOG.info("Created new backing {}", config.getProxyType());

        return result;
    }

    protected abstract T createProxyObject(C config, DiscoveryResult discoveryResult);

    protected Duration getSwitchTimeout(C config, DiscoveryResult oldResult, DiscoveryResult newResult) {
        long timePassedSinceDiscovery = Math.max(System.currentTimeMillis() - newResult.getTimestamp(), 0);
        return Duration.ofMillis(getDistributorTimeout(newResult) * getDistributorDistance(newResult) - timePassedSinceDiscovery);
    }

    protected long getDistributorTimeout(DiscoveryResult discoveryResult) {
        String dto = MapUtil.stringValue(discoveryResult.getConfigs(), DISTRIBUTOR_TIMEOUT_CONFIG);
        return dto != null ? Long.parseLong(dto) : DEFAULT_DISTRIBUTOR_TIMEOUT;
    }

    private long getDistributorDistance(DiscoveryResult discoveryResult) {
        String ddst = MapUtil.stringValue(discoveryResult.getConfigs(), DISTRIBUTOR_DISTANCE_CONFIG);
        return ddst != null ? Long.parseLong(ddst) : DEFAULT_DISTRIBUTOR_DISTANCE;
    }
}
