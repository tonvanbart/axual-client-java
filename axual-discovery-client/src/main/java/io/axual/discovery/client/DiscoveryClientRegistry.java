package io.axual.discovery.client;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

import io.axual.discovery.client.exception.DiscoveryClientRegistrationException;
import io.axual.discovery.client.fetcher.DiscoveryLoader;

public class DiscoveryClientRegistry {
    private static final Logger LOG = LoggerFactory.getLogger(DiscoveryClientRegistry.class);
    // This map contains the registered clients, ordered by DiscoveryConfig. All access should be
    // serialized by first acquiring the lock below it.
    private static final Map<DiscoveryConfig, DiscoveryClientsList> clients = new HashMap<>();
    private static final ReentrantLock lock = new ReentrantLock();

    private static class DiscoveryClientsList {
        private final DiscoveryLoader loader;
        private final Set<DiscoveryClient> clients = new HashSet<>();

        DiscoveryClientsList(DiscoveryLoader loader) {
            this.loader = loader;
        }
    }

    private DiscoveryClientRegistry() {
    }

    public static void register(DiscoveryConfig config, DiscoveryClient client) throws DiscoveryClientRegistrationException {
        lock.lock();
        try {
            if (!clients.containsKey(config)) {
                DiscoveryLoader loader = new DiscoveryLoader(config);
                clients.put(config, new DiscoveryClientsList(loader));
            }

            DiscoveryClientsList reg = clients.get(config);
            reg.clients.add(client);
            client.onDiscoveryPropertiesChange(reg.loader.getDiscoveryResult());
        } catch (Exception e) {
            throw new DiscoveryClientRegistrationException("An exception occurred while trying to register the DiscoveryClient", e);
        } finally {
            lock.unlock();
        }
    }

    public static void invalidate(DiscoveryConfig config) {
        lock.lock();
        try {
            DiscoveryClientsList reg = clients.get(config);
            if (reg != null) {
                reg.loader.invalidate();
            }
        } finally {
            lock.unlock();
        }
    }

    public static void unregister(DiscoveryConfig config, DiscoveryClient client) {
        lock.lock();
        try {
            DiscoveryClientsList reg = clients.get(config);
            if (reg != null) {
                reg.clients.remove(client);

                // if no clients are left in the registration, close the loader and remove the registration
                if (reg.clients.isEmpty()) {
                    try {
                        reg.loader.close();
                    } catch (Exception e) {
                        LOG.warn("Exception occurred while closing the DiscoveryLoader", e);
                    } finally {
                        clients.remove(config);
                    }
                }
            }
        } finally {
            lock.unlock();
        }
    }

    public static void checkProperties(DiscoveryConfig config) {
        // Try to get the lock
        if (lock.tryLock()) {
            try {
                DiscoveryClientsList reg = clients.get(config);
                if (reg != null && reg.loader.discoveryChanged()) {
                    LOG.info("Reloading discovery: appId={}, endpoint={}", config.getApplicationId(), config.getEndpoint());
                    // See if the provider indicates a change
                    DiscoveryResult discoveryResult = reg.loader.getDiscoveryResult();
                    for (DiscoveryClient proxy : reg.clients) {
                        proxy.onDiscoveryPropertiesChange(discoveryResult);
                    }
                }
            } finally {
                // Release the lock
                lock.unlock();
            }
        }
    }

    public static void cleanUp() {
        lock.lock();
        try {
            for (DiscoveryClientsList clientList : clients.values()) {
                try {
                    clientList.loader.close();
                } catch (Exception e) {
                    LOG.warn("Could not close Discovery Loader", e);
                }
            }
            clients.clear();
        } finally {
            lock.unlock();
        }
    }
}
