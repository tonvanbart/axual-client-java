package io.axual.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * These class tests if the properties which should not be changed, are still the same this test
 * should never need to be modified.
 */
public class TestConstants {

    @Test
    public void testAxualSerdeConstants() {
        // Test offsets
        Assert.assertEquals(0, SerdeConstants.MAGIC_OFFSET);
        Assert.assertEquals(2, SerdeConstants.VERSION_OFFSET);
        Assert.assertEquals(4, SerdeConstants.EXTENDED_HEADER_SIZE_OFFSET);

        // Test header size values
        Assert.assertEquals(8, SerdeConstants.HEADER_SIZE);
        Assert.assertEquals(25, SerdeConstants.EXTENDED_HEADER_SIZE);

        // Test fixed contents
        Assert.assertEquals(0x0BEB, SerdeConstants.MAGIC_INT);
    }
}
