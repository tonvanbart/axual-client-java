package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.annotation.InterfaceStability;

/**
 * This configuration object can be used by all clients to configure a consumer.
 * Use the {@link #builder()} to create a builder instance.
 *
 * @param <K> the type of the key that will be consumed. A compatible key deserializer must be
 *            provided to deserialize the objects
 * @param <V> the type of the value that will be consumed. A compatible value deserializer must be
 *            provided to deserialize the objects
 */
@InterfaceStability.Evolving
public class ConsumerConfig<K, V> extends BaseConsumerConfig<K, V> {
    /**
     * Instantiates a new Consumer config.
     *
     * @param builder the builder
     */
    private ConsumerConfig(Builder<K, V, ?> builder) {
        super(builder);
    }

    /**
     * Create a new Builder instance to create a {@link ConsumerConfig}.
     *
     * @param <K> the type of the key that will be consumed. A compatible key deserializer must be
     *           provided to deserialize the objects
     * @param <V> the type of the value that will be consumed. A compatible value deserializer must be
     *           provided to serialize the objects
     * @return A new Builder instance
     */
    public static <K, V> Builder<K, V, ?> builder() {
        return new Builder<>();
    }

    /**
     * The Builder implementation for the {@link ConsumerConfig}.
     *
     * @param <K> the type of the key that will be consumed. A compatible key deserializer must be
     *           provided to deserialize the objects
     * @param <V> the type of the value that will be consumed. A compatible value deserializer must be
     *           provided to deserialize the objects
     * @param <T> the type of the Builder that is to be used.
     */
    public static class Builder<K, V, T extends Builder<K, V, T>> extends BaseConsumerConfig.Builder<K, V, T> {
        @Override
        public ConsumerConfig<K, V> build() {
            validate();
            return new ConsumerConfig<>(this);
        }
    }
}
