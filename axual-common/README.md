# Axual Common

This module contains the code is shared between modules.
If more than one module have a use for a method or class, then it should be moved to this module.