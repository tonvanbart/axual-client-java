package io.axual.common.resolver;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.TopicPartition;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static io.axual.common.resolver.TopicPatternResolver.TOPIC_PATTERN_CONFIG;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TopicPatternResolverTest {

    private static final String STREAM1 = "general-applicationlog";
    private static final String STREAM2 = "general-applicationlog2";
    private static final String TENANT_PROPERTY = "tnt";
    private static final String ENVIRONMENT_PROPERTY = "envt";
    private static final String PATTERN = "{" + TENANT_PROPERTY + "}-{" + ENVIRONMENT_PROPERTY + "}-{topic}";
    private static final String ENVIRONMENT = "unit";
    private static final String TENANT = "axual";
    private static final String UNRESOLVED_STREAM1 = TENANT + "-" + ENVIRONMENT + "-" + STREAM1;
    private static final String UNRESOLVED_STREAM2 = TENANT + "-" + ENVIRONMENT + "-" + STREAM2;

    private static final String TOPIC_RESOLVER_CONFIG = "topic.resolver";

    private TopicPatternResolver resolver;

    @Before
    public void init() {
        resolver = new TopicPatternResolver();
        Map<String, Object> properties = new HashMap<>();
        properties.put(TENANT_PROPERTY, TENANT);
        properties.put(ENVIRONMENT_PROPERTY, ENVIRONMENT);
        properties.put(TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
        properties.put(TOPIC_PATTERN_CONFIG, PATTERN);

        resolver.configure(properties);
    }

    @Test
    public void test_resolveTopic() {
        assertEquals(TENANT + "-" + ENVIRONMENT + "-" + STREAM1, resolver.resolveTopic(STREAM1));
    }

    @Test
    public void test_resolveTopic_null() {
        assertNull(resolver.resolveTopic((TopicPartition) null));
    }

    @Test
    public void test_resolveTopics() {
        final Set<String> topics = resolver.resolveTopics(Arrays.asList(STREAM1, STREAM2));
        assertTrue(topics.contains(UNRESOLVED_STREAM1));
        assertTrue(topics.contains(UNRESOLVED_STREAM2));
    }

    @Test
    public void test_resolveTopics_null() {
        assertEquals(0, resolver.resolveTopics((List) null).size());
    }

    @Test
    public void test_resolveTopicPartitions() {
        final Set<TopicPartition> topicPartitions = resolver.resolveTopicPartitions(
                Arrays.asList(
                        new TopicPartition(STREAM1, 0),
                        new TopicPartition(STREAM2, 0)
                )
        );
        assertTrue(topicPartitions.contains(new TopicPartition(UNRESOLVED_STREAM1, 0)));
        assertTrue(topicPartitions.contains(new TopicPartition(UNRESOLVED_STREAM2, 0)));
    }

    @Test
    public void test_resolveTopicsPartitions_null() {
        assertEquals(0, resolver.resolveTopicPartitions(null).size());
    }

    @Test
    public void test_unresolveTopics() {
        final Set<String> topics = resolver.unresolveTopics(Arrays.asList(UNRESOLVED_STREAM1, UNRESOLVED_STREAM2));
        assertTrue(topics.contains(STREAM1));
        assertTrue(topics.contains(STREAM2));
    }

    @Test
    public void test_unresolveTopics_null() {
        assertEquals(0, resolver.unresolveTopics((List) null).size());
    }

    @Test
    public void test_unresolveTopicPartitions() {
        final Set<TopicPartition> topicPartitions = resolver.unresolveTopicPartitions(
                Arrays.asList(
                        new TopicPartition(UNRESOLVED_STREAM1, 0),
                        new TopicPartition(UNRESOLVED_STREAM2, 0)
                )
        );
        assertTrue(topicPartitions.contains(new TopicPartition(STREAM1, 0)));
        assertTrue(topicPartitions.contains(new TopicPartition(STREAM2, 0)));
    }

    @Test
    public void test_unresolveTopicsPartitions_null() {
        assertEquals(0, resolver.unresolveTopicPartitions(null).size());
    }

}
