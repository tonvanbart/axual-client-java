package io.axual.client.proxy.resolving.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.Map;

import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.resolving.generic.ResolvingClientProxyConfig;
import io.axual.common.resolver.GroupResolver;

public class ResolvingConsumerConfig<K, V> extends ResolvingClientProxyConfig<ConsumerProxy<K, V>> {
    public static final String BACKING_FACTORY_CONFIG = "resolvingconsumer.backing.factory";
    public static final String GROUP_ID_RESOLVER_CONFIG = "group.id.resolver";

    private final GroupResolver groupResolver;

    ResolvingConsumerConfig(Map<String, Object> configs) {
        super(configs, BACKING_FACTORY_CONFIG);
        filterDownstream(GROUP_ID_RESOLVER_CONFIG);

        // Parse group resolver
        groupResolver = getConfiguredInstance(GROUP_ID_RESOLVER_CONFIG, GroupResolver.class);
        groupResolver.configure(configs);

        // Apply group id to downstream consumer
        String groupId = parseAndFilterStringConfig(ConsumerConfig.GROUP_ID_CONFIG, true, null);
        putDownstream(ConsumerConfig.GROUP_ID_CONFIG, groupResolver.resolveGroup(groupId));

        if (configs.containsKey(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG)) {
            putDownstream(ResolvingConsumerPartitionAssignorConfig.BACKING_ASSIGNOR_CONFIG, configs.get(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG));
            putDownstream(ResolvingConsumerPartitionAssignorConfig.TOPIC_RESOLVER_CONFIG, this.getTopicResolver());
            putDownstream(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, ResolvingConsumerPartitionAssignor.class.getName());
        }
    }

    public GroupResolver getGroupResolver() {
        return groupResolver;
    }
}
