package io.axual.client.proxy.generic.registry;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.exception.ProxyTypeNotRegistered;
import io.axual.client.proxy.generic.admin.AdminProxy;
import io.axual.client.proxy.generic.admin.AdminProxyFactory;
import io.axual.client.proxy.generic.client.ClientProxy;
import io.axual.client.proxy.generic.client.ClientProxyFactory;
import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.generic.consumer.ConsumerProxyFactory;
import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.client.proxy.generic.producer.ProducerProxyFactory;
import io.axual.client.proxy.generic.serde.DeserializerProxy;
import io.axual.client.proxy.generic.serde.DeserializerProxyFactory;
import io.axual.client.proxy.generic.serde.SerializerProxy;
import io.axual.client.proxy.generic.serde.SerializerProxyFactory;
import io.axual.client.proxy.wrapped.admin.WrappedAdminClientFactory;
import io.axual.client.proxy.wrapped.consumer.WrappedConsumerFactory;
import io.axual.client.proxy.wrapped.producer.WrappedProducerFactory;
import io.axual.client.proxy.wrapped.serde.WrappedDeserializerFactory;
import io.axual.client.proxy.wrapped.serde.WrappedSerializerFactory;
import io.axual.common.exception.PropertyNotSetException;
import io.axual.common.tools.FactoryUtil;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.ProxyType.ADMIN;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.ProxyType.CONSUMER;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.ProxyType.DESERIALIZER;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.ProxyType.PRODUCER;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.ProxyType.SERIALIZER;

public class ProxyChainUtil {
    private ProxyChainUtil() {
    }

    private static class ConfigInjectingClientProxyFactory<T extends ClientProxy> implements ClientProxyFactory<T> {
        private final Map<String, Object> configs;
        private final ClientProxyFactory<T> factory;

        private ConfigInjectingClientProxyFactory(Map<String, Object> configs, ClientProxyFactory<T> factory) {
            this.configs = configs;
            this.factory = factory;
        }

        @Override
        public T create(Map<String, Object> configs) {
            Map<String, Object> newConfigs = new HashMap<>(configs);
            if (this.configs != null) {
                newConfigs.putAll(this.configs);
            }
            return factory.create(newConfigs);
        }
    }

    private static class ConfigInjectingDeserializerProxyFactory<T> implements DeserializerProxyFactory<T> {
        private final Map<String, Object> configs;
        private final DeserializerProxyFactory<T> factory;

        private ConfigInjectingDeserializerProxyFactory(Map<String, Object> configs, DeserializerProxyFactory<T> factory) {
            this.configs = configs;
            this.factory = factory;
        }

        @Override
        public DeserializerProxy<T> create(Map<String, Object> configs, boolean isKey) {
            Map<String, Object> newConfigs = new HashMap<>(configs);
            if (this.configs != null) {
                newConfigs.putAll(this.configs);
            }
            return factory.create(newConfigs, isKey);
        }
    }

    private static class ConfigInjectingSerializerProxyFactory<T> implements SerializerProxyFactory<T> {
        private final Map<String, Object> configs;
        private final SerializerProxyFactory<T> factory;

        private ConfigInjectingSerializerProxyFactory(Map<String, Object> configs, SerializerProxyFactory<T> factory) {
            this.configs = configs;
            this.factory = factory;
        }

        @Override
        public SerializerProxy<T> create(Map<String, Object> configs, boolean isKey) {
            Map<String, Object> newConfigs = new HashMap<>(configs);
            if (this.configs != null) {
                newConfigs.putAll(this.configs);
            }
            return factory.create(newConfigs, isKey);
        }
    }

    private static <T extends ClientProxy, F extends ClientProxyFactory<T>> ClientProxyFactory<T> prependClientProxyFactory(String factoryClassName, Class<F> factoryClass, Map<String, Object> injectedConfig, String backingFactoryPropertyName, ClientProxyFactory<T> lastFactory) {
        ClientProxyFactory<T> prependedFactory = FactoryUtil.create(factoryClassName, factoryClass);

        // Set backing property
        injectedConfig = injectedConfig != null ? new HashMap<>(injectedConfig) : new HashMap<>();
        injectedConfig.put(backingFactoryPropertyName, lastFactory);

        // Set up the next part of the chain
        return new ConfigInjectingClientProxyFactory<>(injectedConfig, prependedFactory);
    }

    private static <T, F extends DeserializerProxyFactory<T>> DeserializerProxyFactory<T> prependDeserializerProxyFactory(String factoryClassName, Class<F> factoryClass, Map<String, Object> injectedConfig, String backingFactoryPropertyName, DeserializerProxyFactory<T> lastFactory) {
        DeserializerProxyFactory<T> prependedFactory = FactoryUtil.create(factoryClassName, factoryClass);

        // Set backing property
        injectedConfig = injectedConfig != null ? new HashMap<>(injectedConfig) : new HashMap<>();
        injectedConfig.put(backingFactoryPropertyName, lastFactory);

        // Set up the next part of the chain
        return new ConfigInjectingDeserializerProxyFactory<>(injectedConfig, prependedFactory);
    }

    private static <T, F extends SerializerProxyFactory<T>> SerializerProxyFactory<T> prependSerdeProxyFactory(String factoryClassName, Class<F> factoryClass, Map<String, Object> injectedConfig, String backingFactoryPropertyName, SerializerProxyFactory<T> lastFactory) {
        SerializerProxyFactory<T> prependedFactory = FactoryUtil.create(factoryClassName, factoryClass);

        // Set backing property
        injectedConfig = injectedConfig != null ? new HashMap<>(injectedConfig) : new HashMap<>();
        injectedConfig.put(backingFactoryPropertyName, lastFactory);

        // Set up the next part of the chain
        return new ConfigInjectingSerializerProxyFactory<>(injectedConfig, prependedFactory);
    }

    public static ProxyChain parseProxyChain(Map<String, ?> configs, String chainPropertyName) {
        Object chainConfig = configs.get(chainPropertyName);
        if (chainConfig instanceof String) {
            return ProxyChain.parse((String) chainConfig);
        }
        if (chainConfig instanceof ProxyChain) {
            return (ProxyChain) chainConfig;
        }

        throw new PropertyNotSetException(chainPropertyName);
    }

    public static ClientProxyFactory<AdminProxy> setupAdminFactoryChain(ProxyChain chain) {
        return setupAdminFactoryChain(chain, new WrappedAdminClientFactory());
    }

    public static ClientProxyFactory<AdminProxy> setupAdminFactoryChain(ProxyChain chain, ClientProxyFactory<AdminProxy> lastFactory) {
        for (int index = chain.getElements().size() - 1; index >= 0; index--) {
            lastFactory = prependAdminFactoryForElement(lastFactory, chain.getElements().get(index));
        }

        return lastFactory;
    }

    public static ClientProxyFactory<AdminProxy> prependAdminFactoryForElement(ClientProxyFactory<AdminProxy> lastFactory, ProxyChainElement element) {
        ProxyTypeRegistry.ProxyTypeRegistration reg = ProxyTypeRegistry.getProxyRegistration(ADMIN, element.getProxyId());
        if (reg == null) {
            throw new ProxyTypeNotRegistered("admin", element.getProxyId());
        }

        return prependClientProxyFactory(
                reg.getBackingClassName(),
                AdminProxyFactory.class,
                element.getConfigs(),
                reg.getBackingPropertyName(),
                lastFactory);
    }

    public static <K, V> ClientProxyFactory<ConsumerProxy<K, V>> setupConsumerFactoryChain(ProxyChain chain) {
        return setupConsumerFactoryChain(chain, new WrappedConsumerFactory<>());
    }

    public static <K, V> ClientProxyFactory<ConsumerProxy<K, V>> setupConsumerFactoryChain(ProxyChain chain, ClientProxyFactory<ConsumerProxy<K, V>> lastFactory) {
        for (int index = chain.getElements().size() - 1; index >= 0; index--) {
            lastFactory = prependConsumerFactoryForElement(lastFactory, chain.getElements().get(index));
        }

        return lastFactory;
    }

    @SuppressWarnings("unchecked")
    public static <K, V> ClientProxyFactory<ConsumerProxy<K, V>> prependConsumerFactoryForElement(ClientProxyFactory<ConsumerProxy<K, V>> lastFactory, ProxyChainElement element) {
        ProxyTypeRegistry.ProxyTypeRegistration reg = ProxyTypeRegistry.getProxyRegistration(CONSUMER, element.getProxyId());
        if (reg == null) {
            throw new ProxyTypeNotRegistered("consumer", element.getProxyId());
        }

        return prependClientProxyFactory(
                reg.getBackingClassName(),
                ConsumerProxyFactory.class,
                element.getConfigs(),
                reg.getBackingPropertyName(),
                lastFactory);
    }

    public static <K, V> ClientProxyFactory<ProducerProxy<K, V>> setupProducerFactoryChain(ProxyChain chain) {
        return setupProducerFactoryChain(chain, new WrappedProducerFactory<>());
    }

    public static <K, V> ClientProxyFactory<ProducerProxy<K, V>> setupProducerFactoryChain(ProxyChain chain, ClientProxyFactory<ProducerProxy<K, V>> lastFactory) {
        for (int index = chain.getElements().size() - 1; index >= 0; index--) {
            lastFactory = prependProducerFactoryForElement(lastFactory, chain.getElements().get(index));
        }

        return lastFactory;
    }

    @SuppressWarnings("unchecked")
    public static <K, V> ClientProxyFactory<ProducerProxy<K, V>> prependProducerFactoryForElement(ClientProxyFactory<ProducerProxy<K, V>> lastFactory, ProxyChainElement element) {
        ProxyTypeRegistry.ProxyTypeRegistration reg = ProxyTypeRegistry.getProxyRegistration(PRODUCER, element.getProxyId());
        if (reg == null) {
            throw new ProxyTypeNotRegistered("producer", element.getProxyId());
        }

        return prependClientProxyFactory(
                reg.getBackingClassName(),
                ProducerProxyFactory.class,
                element.getConfigs(),
                reg.getBackingPropertyName(),
                lastFactory);
    }

    public static <T> DeserializerProxyFactory<T> setupDeserializerFactoryChain(ProxyChain chain) {
        DeserializerProxyFactory<T> lastFactory = new WrappedDeserializerFactory<>();

        for (int index = chain.getElements().size() - 1; index >= 0; index--) {
            lastFactory = prependDeserializerForElement(lastFactory, chain.getElements().get(index));
        }

        return lastFactory;
    }

    @SuppressWarnings("unchecked")
    public static <T> DeserializerProxyFactory<T> prependDeserializerForElement(DeserializerProxyFactory<T> lastFactory, ProxyChainElement element) {
        ProxyTypeRegistry.ProxyTypeRegistration reg = ProxyTypeRegistry.getProxyRegistration(DESERIALIZER, element.getProxyId());
        if (reg == null) {
            throw new ProxyTypeNotRegistered("deserializer", element.getProxyId());
        }

        return prependDeserializerProxyFactory(
                reg.getBackingClassName(),
                DeserializerProxyFactory.class,
                element.getConfigs(),
                reg.getBackingPropertyName(),
                lastFactory);
    }

    public static <T> SerializerProxyFactory<T> setupSerializerFactoryChain(ProxyChain chain) {
        SerializerProxyFactory<T> lastFactory = new WrappedSerializerFactory<>();

        for (int index = chain.getElements().size() - 1; index >= 0; index--) {
            lastFactory = prependSerializerForElement(lastFactory, chain.getElements().get(index));
        }

        return lastFactory;
    }

    @SuppressWarnings("unchecked")
    public static <T> SerializerProxyFactory<T> prependSerializerForElement(SerializerProxyFactory<T> lastFactory, ProxyChainElement element) {
        ProxyTypeRegistry.ProxyTypeRegistration reg = ProxyTypeRegistry.getProxyRegistration(SERIALIZER, element.getProxyId());
        if (reg == null) {
            throw new ProxyTypeNotRegistered("serializer", element.getProxyId());
        }

        return prependSerdeProxyFactory(
                reg.getBackingClassName(),
                SerializerProxyFactory.class,
                element.getConfigs(),
                reg.getBackingPropertyName(),
                lastFactory);
    }
}
