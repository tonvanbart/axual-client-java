package io.axual.client.proxy.axual.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import io.axual.client.proxy.generic.client.ClientProxy;
import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyChainUtil;
import io.axual.common.config.CommonConfig;

public abstract class AxualProxyConfig<T extends ClientProxy> extends BaseClientProxyConfig<T> {
    private static final ConcurrentHashMap<String, AtomicInteger> clientIdCounters = new ConcurrentHashMap<>();

    private final String applicationId;
    private final ProxyChain proxyChain;
    private final String proxyType;

    public AxualProxyConfig(Map<String, Object> configs, String proxyType, String backingFactoryConfigKey, String chainConfigKey) {
        super(configs, backingFactoryConfigKey);

        filterDownstream(chainConfigKey);

        this.proxyType = proxyType;

        // Parse the application id and version
        applicationId = parseStringConfig(CommonConfig.APPLICATION_ID, true, null);

        // Parse the chain of proxies to instantiate, passed as either String or ProxyChain object
        proxyChain = ProxyChainUtil.parseProxyChain(configs, chainConfigKey);

        // Add the Kafka Client ID, used for JMX
        putDownstream(ProducerConfig.CLIENT_ID_CONFIG, generateNewClientId());
    }

    protected static Map<String, Object> addDefaultFactory(Map<String, Object> configs, String backingFactoryConfigKey, Object defaultValue) {
        Map<String, Object> result = new HashMap<>(configs);
        result.putIfAbsent(backingFactoryConfigKey, defaultValue);
        return result;
    }

    public ProxyChain getProxyChain() {
        return proxyChain;
    }

    private String generateNewClientId() {
        // Initialize client id
        String clientName = proxyType + "-" + applicationId;
        clientIdCounters.putIfAbsent(clientName, new AtomicInteger(0));
        AtomicInteger counter = clientIdCounters.get(clientName);
        return clientName + "-" + counter.incrementAndGet();
    }

    public String getApplicationId() {
        return applicationId;
    }
}
