## Axual Platform Test Standalone
The standalone is a simple server based on the Axual Platform, providing a testbed for developers.
With it you can set up a single or multicluster instance.

### Features
- Single and multicluster support
- Possible to use own keystores for servers
- Fully compatible with Axual Client
- Supports advanced ACL formatting
- Avro Support (Avro JAR must be added to classpath when started)
- Define new application IDs using the Rest API
- Define new stream definitions using the Rest API
- View cluster assignments using the Rest API
- Assign application IDs to different cluster using the Rest API
- Change TTL, Distributor Timeout and Distance using the Rest API

### Limitations
- Discovery API is mocked
- Distributor distance and timeout is static for all applications
- TTL is static
- Schema Registry is mocked, only basic SerDe calls needed for runtime supported
- No ACLs support (for ACL, add them using AdminClient)

### Run Standalone (JAR)
1. Download the Axual Platform Standalone fat JAR from the repository
2. Create an application.yml
3. Run with **_java -Dserver.path=8080 -jar <standalone jar path>_** for normal running.
4. To run with extra classpath entries, run with **_java -Dserver.path=8080 -Dloader.path <extra classpaths, comma separated> -jar <standalone jar path>_**

## Example configurations

The following configuration examples are for single cluster run and multicluster run.
It uses the following settings for the single cluster setup:
It uses the following settings for the single cluster setup:
- Tenant: axual
- Instance:platform-standalone
- Default environment: local
- Discovery API TTL: 5000 milliseconds
- Discovery API Distributor Distance: 2
- Discovery API Distributor Timeout: 20000 milliseconds
- Bind address: 0.0.0.0
- Advertised address: localhost
- Rest API port: 8080
- Discovery API port: 8081
- Cluster
   - Name: clusterA
   - Schema Registry port: 8082
   - Zookeeper port: 8083
   - Broker port: 8084
   - Use Advanced ACL: true
   - Topic Pattern: {tenant}-{instance}-{environment}-{topic}
   - Group Id Pattern: {tenant}-{instance}-{environment}-{group}
- Application 1 
   - Application ID: io.axual.test-1
   - Environment: local
- Application 2
   - Application ID: io.axual.test-2
   - Environment: local2
- Stream 1
   - Name: general-test
   - Environment: local
   - Is raw topic: false
   - Avro Class Key: io.axual.client.test.Random
   - Avro Class Value: io.axual.client.test.Random
- Stream 2
   - Name: general-test
   - Environment: local2
   - Is raw topic: false
   - Avro Class Key: io.axual.client.test.Random
   - Avro Class Value: io.axual.client.test.Random

It uses the following settings for the dual cluster setup:
- Tenant: axual
- Instance:platform-standalone
- Default environment: local
- Discovery API TTL: 5000 milliseconds
- Discovery API Distributor Distance: 2
- Discovery API Distributor Timeout: 20000 milliseconds
- Bind address: 0.0.0.0
- Advertised address: localhost
- Rest API port: 8080
- Discovery API port: 8081
- Cluster 1
   - Name: clusterA
   - Schema Registry port: 8082
   - Zookeeper port: 8083
   - Broker port: 8084
   - Use Advanced ACL: true
   - Topic Pattern: {tenant}-{instance}-{environment}-{topic}
   - Group Id Pattern: {tenant}-{instance}-{environment}-{group}
- Cluster 2
   - Name: clusterB
   - Schema Registry port: 8092
   - Zookeeper port: 8093
   - Broker port: 8094
   - Use Advanced ACL: true
   - Topic Pattern: {tenant}-{instance}-{environment}-{topic}
   - Group Id Pattern: {tenant}-{instance}-{environment}-{group}
- Application 1 
   - Application ID: io.axual.test-1
   - Environment: local
- Application 2
   - Application ID: io.axual.test-2
   - Environment: local2
- Stream 1
   - Name: general-test
   - Environment: local
   - Is raw topic: false
   - Avro Class Key: io.axual.client.test.Random
   - Avro Class Value: io.axual.client.test.Random
- Stream 2
   - Name: general-test
   - Environment: local2
   - Is raw topic: false
   - Avro Class Key: io.axual.client.test.Random
   - Avro Class Value: io.axual.client.test.Random

### Standalone YAML Example (single cluster)
```yaml
standalone:
  type: single
  tenant: axual
  instance: platform-standalone
  environment: local
  ttl: 5000
  distributorTimeout: 20000
  distributorDistance: 2
  bindAddress: 0.0.0.0
  advertisedAddress: localhost
  clusterName: clusterA
  endpointPort: 8081
  schemaRegistryPort: 8082
  zookeeperPort: 8083
  brokerPort: 8084
  useAdvancedAcl: true
  topicPattern: '{tenant}-{instance}-{environment}-{topic}'
  groupIdPattern: '{tenant}-{instance}-{environment}-{group}'
  applications:
    0:
      applicationId: io.axual.test-1
      environment: local
    1:
      applicationId: io.axual.test-1
      environment: local2
  sslConfig:
    keystoreLocation: ~/standalone.server.keystore.jks
    keystorePassword: notsecret
    keyPassword: notsecret
    truststoreLocation: ~/standalone.server.truststore.jks
    truststorePassword: notsecret
    hostnameVerificationEnabled: false
  streams:
    0:
      name: general-test
      isRawTopic: false
      keyClass: io.axual.client.test.Random
      valueClass: io.axual.client.test.Random
      partitions: 2
      environment: local
    1:
      name: general-test
      isRawTopic: false
      keyClass: io.axual.client.test.Random
      valueClass: io.axual.client.test.Random
      partitions: 2
      environment: local2
```

### Standalone YAML Example (multi cluster)
```yaml
standalone:
  type: multi
  tenant: axual
  instance: platform-standalone
  environment: local
  ttl: 5000
  distributorTimeout: 20000
  distributorDistance: 2
  bindAddress: 0.0.0.0
  advertisedAddress: localhost
  endpointPort: 8081
  enableDistribution: true
  clusterSettings:
    0:
      name: clusterA
      schemaRegistryPort: 8082
      zookeeperPort: 8083
      brokerPort: 8084
      useAdvancedAcl: true
      topicPattern: '{tenant}-{instance}-{environment}-{topic}'
      groupIdPattern: '{tenant}-{instance}-{environment}-{group}'
    1:
      name: clusterB
      schemaRegistryPort: 8092
      zookeeperPort: 8093
      brokerPort: 8094
      useAdvancedAcl: false
      topicPattern: '{tenant}-{instance}-{environment}-{topic}'
      groupIdPattern: '{tenant}-{instance}-{environment}-{group}'
  applications:
    0:
      applicationId: io.axual.test-1
      environment: local
      assignedCluster: clusterA
    1:
      applicationId: io.axual.test-1
      environment: local2
      assignedCluster: clusterB
  sslConfig:
    keystoreLocation: ~/standalone.server.keystore.jks
    keystorePassword: notsecret
    keyPassword: notsecret
    truststoreLocation: ~/standalone.server.truststore.jks
    truststorePassword: notsecret
    hostnameVerificationEnabled: false
  streams:
    0:
      name: general-test
      isRawTopic: false
      keyClass: io.axual.client.test.Random
      valueClass: io.axual.client.test.Random
      partitions: 2
      environment: local
    1:
      name: general-test
      isRawTopic: false
      keyClass: io.axual.client.test.Random
      valueClass: io.axual.client.test.Random
      partitions: 2
      environment: local2
```

## Run Single Cluster Standalone (Docker)
```shell script
docker run -ti --rm --name axual-standalone \
-p 8080-8081:8080-8081 \
-p 8082-8084:8082-8084 \
-e SERVER_PORT=8080 \
-e STANDALONE_TYPE=single \
-e STANDALONE_TENANT=axual \
-e STANDALONE_INSTANCE=platform-standalone \
-e STANDALONE_ENVIRONMENT=local \
-e STANDALONE_TTL=1000 \
-e STANDALONE_DISTRIBUTOR_DISTANCE=2 \
-e STANDALONE_DISTRIBUTOR_TIMEOUT=20000 \
-e STANDALONE_BIND_ADDRESS=0.0.0.0 \
-e STANDALONE_ADVERTISED_ADDRESS=localhost \
-e STANDALONE_CLUSTER_NAME=clusterA \
-e STANDALONE_ENDPOINT_PORT=8081 \
-e STANDALONE_SCHEMA_REGISTRY_PORT=8082 \
-e STANDALONE_ZOOKEEPER_PORT=8083 \
-e STANDALONE_BROKER_PORT=8084 \
-e STANDALONE_USE_ADVANCED_ACL=true \
-e STANDALONE_TOPIC_PATTERN='{tenant}-{instance}-{environment}-{topic}' \
-e STANDALONE_GROUP_ID_PATTERN='{tenant}-{instance}-{environment}-{group}' \
-e STANDALONE_APPLICATIONS_0_APPLICATION_ID=io.axual.test-1 \
-e STANDALONE_APPLICATIONS_0_ENVIRONMENT=local \
-e STANDALONE_APPLICATIONS_1_APPLICATION_ID=io.axual.test-2 \
-e STANDALONE_APPLICATIONS_1_ENVIRONMENT=local2 \
-e STANDALONE_STREAMS_0_NAME=general-test \
-e STANDALONE_STREAMS_0_IS_RAW_TOPIC=false \
-e STANDALONE_STREAMS_0_KEY_CLASS=io.axual.client.test.Random \
-e STANDALONE_STREAMS_0_VALUE_CLASS=io.axual.client.test.Random \
-e STANDALONE_STREAMS_0_PARTITIONS=2 \
-e STANDALONE_STREAMS_0_ENVIRONMENT=local \
-e STANDALONE_STREAMS_1_NAME=general-test \
-e STANDALONE_STREAMS_1_IS_RAW_TOPIC=false \
-e STANDALONE_STREAMS_1_KEY_CLASS=io.axual.client.test.Random \
-e STANDALONE_STREAMS_1_VALUE_CLASS=io.axual.client.test.Random \
-e STANDALONE_STREAMS_1_PARTITIONS=2 \
-e STANDALONE_STREAMS_1_ENVIRONMENT=local2 \
docker.axual.io/axaul/platform-test-standalone:5.4.1
```


## Run Multi Cluster Standalone (Docker)
```shell script
docker run -ti --rm --name axual-standalone \
-p 8080-8081:8080-8081 \
-p 8082-8084:8082-8084 \
-p 8092-8094:8092-8094 \
-e SERVER_PORT=8080 \
-e STANDALONE_TYPE=multi \
-e STANDALONE_TENANT=axual \
-e STANDALONE_INSTANCE=platform-standalone \
-e STANDALONE_ENVIRONMENT=local \
-e STANDALONE_TTL=1000 \
-e STANDALONE_DISTRIBUTOR_DISTANCE=2 \
-e STANDALONE_DISTRIBUTOR_TIMEOUT=20000 \
-e STANDALONE_BIND_ADDRESS=0.0.0.0 \
-e STANDALONE_ADVERTISED_ADDRESS=localhost \
-e STANDALONE_ENDPOINT_PORT=8081 \
-e STANDALONE_USE_ADVANCED_ACL=true \
-e STANDALONE_ENABLE_DISTRIBUTION=true \
-e STANDALONE_CLUSTERS_0_NAME=clusterA \
-e STANDALONE_CLUSTERS_0_SCHEMA_REGISTRY_PORT=8082 \
-e STANDALONE_CLUSTERS_0_ZOOKEEPER_PORT=8083 \
-e STANDALONE_CLUSTERS_0_BROKER_PORT=8084 \
-e STANDALONE_CLUSTERS_0_TOPIC_PATTERN='{tenant}-{instance}-{environment}-{topic}' \
-e STANDALONE_CLUSTERS_0_GROUP_ID_PATTERN='{tenant}-{instance}-{environment}-{group}' \
-e STANDALONE_CLUSTERS_1_NAME=clusterB \
-e STANDALONE_CLUSTERS_1_SCHEMA_REGISTRY_PORT=8092 \
-e STANDALONE_CLUSTERS_1_ZOOKEEPER_PORT=8093 \
-e STANDALONE_CLUSTERS_1_BROKER_PORT=8094 \
-e STANDALONE_CLUSTERS_1_TOPIC_PATTERN='{tenant}-{instance}-{environment}-{topic}' \
-e STANDALONE_CLUSTERS_1_GROUP_ID_PATTERN='{tenant}-{instance}-{environment}-{group}' \
-e STANDALONE_APPLICATIONS_0_APPLICATION_ID=io.axual.test-1 \
-e STANDALONE_APPLICATIONS_0_ENVIRONMENT=local \
-e STANDALONE_APPLICATIONS_0_ASSIGNED_CLUSTER=clusterA \
-e STANDALONE_APPLICATIONS_1_APPLICATION_ID=io.axual.test-2 \
-e STANDALONE_APPLICATIONS_1_ENVIRONMENT=local2 \
-e STANDALONE_APPLICATIONS_1_ASSIGNED_CLUSTER=clusterB \
-e STANDALONE_STREAMS_0_NAME=general-test \
-e STANDALONE_STREAMS_0_IS_RAW_TOPIC=false \
-e STANDALONE_STREAMS_0_KEY_CLASS=io.axual.client.test.Random \
-e STANDALONE_STREAMS_0_VALUE_CLASS=io.axual.client.test.Random \
-e STANDALONE_STREAMS_0_PARTITIONS=2 \
-e STANDALONE_STREAMS_0_ENVIRONMENT=local \
-e STANDALONE_STREAMS_1_NAME=general-test \
-e STANDALONE_STREAMS_1_IS_RAW_TOPIC=false \
-e STANDALONE_STREAMS_1_KEY_CLASS=io.axual.client.test.Random \
-e STANDALONE_STREAMS_1_VALUE_CLASS=io.axual.client.test.Random \
-e STANDALONE_STREAMS_1_PARTITIONS=2 \
-e STANDALONE_STREAMS_1_ENVIRONMENT=local2 \
docker.axual.io/axaul/platform-test-standalone:5.4.1
```

## Standalone Rest API
The Standalone Rest API can be used to view and alter the Standalone configurations.
The port where the API listens is defined by the environment variable SERVER_PORT or Java argument server.port.
All operations are HTTP GET operations in order for developers to use a web browser for the calls.

### Instance operations
Instance operations control the system at an instance level, for all registered applications

#### Get instance configuration
URL: **/standalone/instance** <br>
Verb: **GET** <br>
Returns the Instance configuration

#### Get TTL
URL: **/standalone/ttl** <br>
Verb: **GET** <br>
Returns the TTL for this instance

#### Set TTL
URL: **/standalone/ttl/set?_value_=XXX** <br>
Verb: **GET** <br>
Input: **_value_** - The TTL in milliseconds <br>
Sets the TTL to a new value for all applications in the instance

#### Get Distributor Distance
URL: **/standalone/distributor/distance** <br>
Verb: **GET** <br>
Returns the distributor distance for this instance

#### Set Distributor Distance
URL: **/standalone/distributor/distance/set?_value_=XXX** <br>
Verb: **GET** <br>
Input: **_value_** - The new distributor distance in steps <br>
Sets the distributor distance to a new value for all applications in the instance

#### Get Distributor Timeout
URL: **/standalone/distributor/timeout** <br>
Verb: **GET** <br>
Returns the distributor timeout for this instance

#### Set Distributor Timeout
URL: **/standalone/distributor/timeout/set?_value_=XXX** <br>
Verb: **GET** <br>
Input: **_value_** - The new distributor timeout in steps <br>
Sets the distributor timeout to a new value for all applications in the instance


### Cluster operations
Cluster operations control the standalone platform on a cluster level.

#### Get available cluster names
URL: **/standalone/cluster** <br>
Verb: **GET** <br>
Returns a list of cluster names

#### Get cluster configuration
URL: **/standalone/cluster/_{clusterName}_** <br>
Verb: **GET** <br>
Input: **_clustername_** - The name of the cluster <br>
Returns the cluster configuration


### Application operations
The application operations will give you information about application registrations within the standalone platform

#### Get all applications
URL: **/standalone/application** <br>
Verb: **GET** <br>
Returns a list of application configurations

#### Create an application
URL: **/standalone/application/create/_{applicationId}_?_environment_=XXX&_clusterName_=YYY** <br>
Verb: **GET** <br>
Input: **_applicationId_** - The application ID of this new application registration <br>
Input: **_environment_** - Specifies for which environment the application registration is. If not set then the environment set in the instance configuration will be used. <br>
Input: **_clusterName_** - Specifies to which cluster this application registration should be assigned. If not set then the first available cluster definition will be used. <br>
Returns the application registration data


### Stream operations
The stream operations will give you information about stream registrations within the standalone platform

#### Get all streams
URL: **/standalone/stream** <br>
Verb: **GET** <br>
Returns a Map of stream names and the corresponding stream registration.

#### Create a stream
URL: **/standalone/application/stream/_{name}_?_isRaw_=true/&_partitions_=5&_keyClass_=io.axual.client.test.Random&_valueClass_=io.axual.client.test.Random** <br>
Verb: **GET** <br>
Input: **_name_** - the stream name <br>
Input: **_isRaw_** - a boolean flag (true/false), no name resolution will be used when true, creating a topic with the specified name. When false or not set the topic pattern will be used to create the topic  <br>
Input: **_environment_** - Specifies for which environment the application registration is. If not set then the environment set in the instance configuration will be used. <br>
Input: **_partitions_** - Specifies to number of partitions to create for this stream. Default value is 3 <br>
Input: **_keyClass_** - The name of the Avro Schema Compiled Classname to use for the key. Can be left unset if Avro is not used. <br>
Input: **_valueClass_** - The name of the Avro Schema Compiled Classname to use for the value. Can be left unset if Avro is not used. <br>
Returns the stream registration data


### Assignment operations
Controls the assignments of applications to clusters.

#### Get all assignments
URL: **/standalone/assignment** <br>
Verb: **GET** <br>
Returns a Map of Cluster Names with a list of application registrations assigned to that cluster.

#### Get all assignments for a cluster
URL: **/standalone/assignment/_{clusterName}_** <br>
Verb: **GET** <br>
Input: **_clustername_** - The name of the cluster <br>
Returns List of application registrations assigned to the cluster. <br>

#### Assign an application to a cluster
URL: **/standalone/assignment/_{clusterName}_/set/_{applicationId}_?_environment_=XXX** <br>
Verb: **GET** <br>
Input: **_clustername_** - The name of the cluster <br>
Input: **_applicationId_** - The id of the application registration to assign to the cluster <br>
Input: **_environment_** - Specifies for which environment the application registration is. If not set then the environment set in the instance configuration will be used. <br>
Returns the new application registration.

## How to test application switching between clusters 
- Define the `applications` and `streams` in the [standalone.yml](#standalone-yaml-example-multi-cluster) file.
- Run the [standalone application](#run-standalone-jar).
- Run the `Producer` and `Consumer` application.
- Check on which [cluster Producer & Consumer applications are registered](#get-all-assignments).
- Try to [switch one of the application (producer/consumer) to another cluster](#assign-an-application-to-a-cluster).
- Open the switching application logs and check app is switched to new cluster and working. <br/>
**Logs**:
```
[ProducerWorker0] INFO io.axual.discovery.client.fetcher.DiscoveryLoader - Change in discovery properties, new properties: DiscoveryResult: {group.id.pattern={tenant}-{instance}-{environment}-{group}, acl.principal.builder=io.axual.security.principal.BasicAclPrincipalBuilder, cluster=clusterB, instance=example, topic.pattern={tenant}-{instance}-{environment}-{topic}, bootstrap.servers=localhost:8094, topic.resolver=io.axual.common.resolver.TopicPatternResolver, ttl=5000, distributor.distance=2, enable.value.headers=true, schema.registry.url=http://localhost:8092, environment=local, distributor.timeout=20000, system=platform-test-standalone, group.id.resolver=io.axual.common.resolver.GroupPatternResolver, tenant=axual}
[ProducerWorker0] INFO io.axual.discovery.client.DiscoveryClientRegistry - Reloading discovery: appId=io.axual.example.client.avro.producer, endpoint=http://127.0.0.1:8081/v2
[ProducerWorker0] INFO io.axual.client.proxy.switching.discovery.DiscoverySubscriber - Received new DiscoveryResult for SwitchingProducer: DiscoveryResult: {group.id.pattern={tenant}-{instance}-{environment}-{group}, acl.principal.builder=io.axual.security.principal.BasicAclPrincipalBuilder, cluster=clusterB, instance=example, topic.pattern={tenant}-{instance}-{environment}-{topic}, bootstrap.servers=localhost:8094, topic.resolver=io.axual.common.resolver.TopicPatternResolver, ttl=5000, distributor.distance=2, enable.value.headers=true, schema.registry.url=http://localhost:8092, environment=local, distributor.timeout=20000, system=platform-test-standalone, group.id.resolver=io.axual.common.resolver.GroupPatternResolver, tenant=axual}
[ProducerWorker0] INFO io.axual.client.proxy.switching.discovery.DiscoverySubscriber - Switching SwitchingProducer from clusterA to clusterB
[ProducerWorker0] INFO io.axual.client.proxy.generic.client.DynamicClientProxy - Something changed, replacing backing producer
[ProducerWorker0] INFO org.apache.kafka.clients.producer.KafkaProducer - [Producer clientId=producer-io.axual.example.client.avro.producer-1] Closing the Kafka producer with timeoutMillis = 86399913600000 ms.
[ProducerWorker0] INFO io.axual.client.proxy.switching.generic.BaseClientProxySwitcher - Sleeping before switching ResolvingProducer, switch timeout = PT39.993S
[ProducerWorker0] INFO io.axual.client.proxy.switching.generic.BaseClientProxySwitcher - Sleeping of ProducerSwitcher done
[ProducerWorker0] INFO io.axual.client.proxy.switching.generic.BaseClientProxySwitcher - Creating new backing producer with Discovery API result: DiscoveryResult: {group.id.pattern={tenant}-{instance}-{environment}-{group}, acl.principal.builder=io.axual.security.principal.BasicAclPrincipalBuilder, cluster=clusterB, instance=example, topic.pattern={tenant}-{instance}-{environment}-{topic}, bootstrap.servers=localhost:8094, topic.resolver=io.axual.common.resolver.TopicPatternResolver, ttl=5000, distributor.distance=2, enable.value.headers=true, schema.registry.url=http://localhost:8092, environment=local, distributor.timeout=20000, system=platform-test-standalone, group.id.resolver=io.axual.common.resolver.GroupPatternResolver, tenant=axual}
[ProducerWorker0] INFO io.axual.client.proxy.switching.producer.ProducerSwitcher - Creating a new producer with properties: {acl.principal.builder=io.axual.security.principal.BasicAclPrincipalBuilder, cluster=clusterB, instance=example, reconnect.backoff.ms=1000, topic.pattern={tenant}-{instance}-{environment}-{topic}, bootstrap.servers=localhost:8094, topic.resolver=io.axual.common.resolver.TopicPatternResolver, retry.backoff.ms=1000, key.serializer=io.axual.serde.avro.SpecificAvroSerializer, ssl.keystore.type=JKS, schema.registry.url=http://localhost:8092, endpoint=http://127.0.0.1:8081, ssl.key.password=[hidden], ssl.truststore.password=[hidden], max.in.flight.requests.per.connection=1, tenant=axual, ssl.endpoint.identification.algorithm=, client.id=producer-io.axual.example.client.avro.producer-1, group.id.pattern={tenant}-{instance}-{environment}-{group}, ssl.protocol=TLS, ssl.enabled.protocols=TLSv1.2,TLSv1.1, acks=-1, batch.size=1, ssl.keystore.location=/var/folders/nd/nm71bpwd4cd7_3n23jh76v600000gn/T/btf-5988138826534296344.tmp, app.id=io.axual.example.client.avro.producer, ttl=5000, distributor.distance=2, ssl.truststore.type=JKS, enable.value.headers=true, app.version=0.0.1, security.protocol=SSL, retries=0, environment=local, ssl.truststore.location=/var/folders/nd/nm71bpwd4cd7_3n23jh76v600000gn/T/btf-8856604717570117145.tmp, value.serializer=io.axual.serde.avro.SpecificAvroSerializer, distributor.timeout=20000, system=platform-test-standalone, ssl.keystore.password=[hidden], group.id.resolver=io.axual.common.resolver.GroupPatternResolver, linger.ms=0}
[ProducerWorker0] INFO org.apache.kafka.clients.producer.ProducerConfig - ProducerConfig values: 
	...

[ProducerWorker0] INFO io.confluent.kafka.serializers.KafkaAvroSerializerConfig - KafkaAvroSerializerConfig values: 
	...

[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'acl.principal.builder' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'cluster' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'instance' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'topic.pattern' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'schema.registry.url' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'endpoint' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'tenant' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'group.id.pattern' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'app.id' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'ttl' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'distributor.distance' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'enable.value.headers' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'app.version' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'environment' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'distributor.timeout' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'system' was supplied but isn't a known config.
[ProducerWorker0] WARN org.apache.kafka.clients.producer.ProducerConfig - The configuration 'group.id.resolver' was supplied but isn't a known config.
[ProducerWorker0] INFO org.apache.kafka.common.utils.AppInfoParser - Kafka version: 2.3.1
[ProducerWorker0] INFO org.apache.kafka.common.utils.AppInfoParser - Kafka commitId: 18a913733fb71c01
[ProducerWorker0] INFO org.apache.kafka.common.utils.AppInfoParser - Kafka startTimeMs: 1598544138995
[ProducerWorker0] INFO io.axual.client.proxy.switching.generic.BaseClientProxySwitcher - Created new backing producer
```