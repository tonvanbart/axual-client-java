package io.axual.client.converter;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.io.JsonEncoder;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificRecord;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import io.axual.client.exception.SchemaResolveException;
import io.axual.client.exception.SerializationException;

public class AvroJsonConverter implements AvroConverter<String> {
    private final Schema schema;

    public AvroJsonConverter(Schema schema) {
        this.schema = schema;
    }

    @Override
    public String convertFrom(GenericRecord record) {
        GenericDatumWriter<Object> writer = new GenericDatumWriter<>(record.getSchema());

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            JsonEncoder jsonEncoder = EncoderFactory.get().jsonEncoder(record.getSchema(), out);
            writer.write(record, jsonEncoder);
            jsonEncoder.flush();

            return out.toString("UTF-8");
        } catch (IOException e) {
            throw new SerializationException("Error converting to JSON", e);
        }
    }

    @Override
    public GenericRecord convertTo(String json) {
        try {
            return new GenericDatumReader<GenericRecord>(schema)
                    .read(null, DecoderFactory.get().jsonDecoder(schema, json));
        } catch (SchemaResolveException e) {
            throw new SerializationException("Error getting schema for stream, SchemaRegistry might be down, or stream" +
                    " is not configured", e);
        } catch (IOException e) {
            throw new SerializationException("Error deserializing given JSON with the latest Schema from the stream", e);
        }
    }

    @Override
    public <S extends SpecificRecord> S toSpecific(String serializedRecord, Class<S> recordClass) {
        try {
            final Schema classSchema = (Schema) recordClass.getField("SCHEMA$").get(null);

            return new SpecificDatumReader<S>(classSchema)
                    .read(null, DecoderFactory.get().jsonDecoder(classSchema, serializedRecord));
        } catch (IOException e) {
            throw new SerializationException("Error deserializing given JSON with the Schema from the class", e);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new SerializationException("Error getting Schema from provided class", e);
        }
    }
}
