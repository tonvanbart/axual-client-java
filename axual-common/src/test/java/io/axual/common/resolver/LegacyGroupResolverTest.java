package io.axual.common.resolver;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import io.axual.common.config.CommonConfig;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class LegacyGroupResolverTest {
    private static final Logger LOG = LoggerFactory.getLogger(LegacyGroupResolver.class);

    @Test
    public void testValidPatterns() {
        Map<String, Object> context = new HashMap<>();
        context.put(CommonConfig.TENANT, "acme");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.TENANT, "acme");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.INSTANCE, "inst");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.ENVIRONMENT, "env");

        for (String group : LegacyGroups.VALID_GROUPS) {
            final LegacyGroupResolver resolver = new LegacyGroupResolver();
            resolver.configure(context);
            Map<String, String> parsed = resolver.unresolveContext(group);
            assertNotNull("Group " + group + " could not be parsed", parsed);

            TreeMap<String, String> sortedElements = new TreeMap<>(parsed);
            String fields = "";
            for (Map.Entry<String, String> entry : sortedElements.entrySet()) {
                if (fields.length() > 0) {
                    fields += ", ";
                }

                fields += entry.getKey() + "=" + entry.getValue();
            }
            LOG.info(group + " ===> " + fields);
        }
    }

    @Test
    public void testInvalidPatterns() {
        Map<String, Object> context = new HashMap<>();
        context.put(CommonConfig.TENANT, "acme");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.TENANT, "acme");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.INSTANCE, "inst");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.ENVIRONMENT, "env");

        for (String group : LegacyGroups.INVALID_GROUPS) {
            final LegacyGroupResolver resolver = new LegacyGroupResolver();
            resolver.configure(context);
            Map<String, String> parsed = resolver.unresolveContext(group);
            assertNull("Group " + group + " is not expected to be parsed", parsed);
        }
    }

    @Test
    public void testValidPatternsToLegacyPatternResolver() {
        Map<String, Object> context = new HashMap<>();
        context.put(CommonConfig.TENANT, "acme");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.TENANT, "acme");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.INSTANCE, "inst");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.ENVIRONMENT, "env");

        for (String group : LegacyGroups.VALID_GROUPS) {
            final LegacyGroupResolver sourceResolver = new LegacyGroupResolver();
            sourceResolver.configure(context);
            Map<String, String> parsed = sourceResolver.unresolveContext(group);
            assertNotNull("Group " + group + " could not be parsed", parsed);

            final GroupResolver targetResolver = new LegacyGroupResolver();
            targetResolver.configure(new HashMap<String, Object>(parsed));
            String targetGroupName = targetResolver.resolve(sourceResolver.getNameFromContext(parsed));

            assertEquals(group, targetGroupName);
        }
    }

    @Test
    public void testValidPatternsToGroupPatternResolver() {
        Map<String, Object> context = new HashMap<>();
        context.put(CommonConfig.TENANT, "acme");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.TENANT, "acme");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.INSTANCE, "inst");
        context.put(Resolver.DEFAULT_PREFIX + CommonConfig.ENVIRONMENT, "env");

        for (String group : LegacyGroups.VALID_GROUPS) {
            final LegacyGroupResolver sourceResolver = new LegacyGroupResolver();
            sourceResolver.configure(context);
            Map<String, String> parsed = sourceResolver.unresolveContext(group);
            assertNotNull("Group " + group + " could not be parsed", parsed);

            final GroupResolver targetResolver = new GroupPatternResolver();
            parsed.put(LegacyGroupResolver.GROUP_ID_PATTERN_CONFIG, "{tenant}-{instance}-{environment}-{group}");
            targetResolver.configure(new HashMap<String, Object>(parsed));
            String resolvedGroup = targetResolver.resolve(sourceResolver.getNameFromContext(parsed));

            assertNotNull("Group should always be resolvable using default arguments passed to unresolver", resolvedGroup);
        }
    }

    @Test
    public void testConversions() {
        final String legacyPattern = "{tenant}-{instance}-{environment}-{group}";
        final String normalPattern = "{tenant}-{instance}-{environment}-{group}";
        Map<String, Object> emptyContext = new HashMap<>();
        Map<String, Object> contextWithDefaults = new HashMap<>();
        contextWithDefaults.put(CommonConfig.TENANT, "tnt");
        contextWithDefaults.put(Resolver.DEFAULT_PREFIX + CommonConfig.TENANT, "tnt");
        contextWithDefaults.put(Resolver.DEFAULT_PREFIX + CommonConfig.INSTANCE, "inst");
        contextWithDefaults.put(Resolver.DEFAULT_PREFIX + CommonConfig.ENVIRONMENT, "env");

        GroupPatternResolver normalUnresolver = new GroupPatternResolver();
        LegacyGroupResolver legacyUnresolver = new LegacyGroupResolver();
        GroupPatternResolver normalResolver = new GroupPatternResolver();
        LegacyGroupResolver legacyResolver = new LegacyGroupResolver();

        // Returns null because tenant name not set in context
        assertNull(convert("aaa", legacyUnresolver, legacyPattern, normalResolver, normalPattern, emptyContext));
        // Returns null because aaa does not contain tnt
        assertNull(convert("aaa", legacyUnresolver, legacyPattern, normalResolver, normalPattern, contextWithDefaults));
        // Returns null because tenant not not found in group
        assertNull(convert("aaa", legacyUnresolver, legacyPattern, normalResolver, normalPattern, contextWithDefaults));
        // Returns resolved topic
        assertEquals("tnt-inst-env-grp", convert("grp-tnt-env", legacyUnresolver, legacyPattern, normalResolver, normalPattern, contextWithDefaults));

        // TODO: extern with more patterns as we find new cases
    }

    private String convert(String group, GroupResolver unresolver, String unresolverPattern, GroupResolver resolver, String resolverPattern, Map<String, Object> context) {
        Map<String, Object> unresolverConfig = new HashMap<>(context);
        unresolverConfig.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, unresolverPattern);
        unresolver.configure(unresolverConfig);

        Map<String, String> unresolvedContext = unresolver.unresolveContext(group);
        String unresolvedGroup = unresolver.getNameFromContext(unresolvedContext);

        if (unresolvedGroup == null) {
            return null;
        }

        unresolvedContext.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, resolverPattern);
        resolver.configure(new HashMap<>(unresolvedContext));
        return resolver.resolveGroup(unresolvedGroup);
    }
}
