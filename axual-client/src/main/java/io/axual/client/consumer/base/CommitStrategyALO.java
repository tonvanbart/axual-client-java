package io.axual.client.consumer.base;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.List;

import io.axual.client.exception.RetriableException;
import io.axual.client.exception.SkippableException;

public class CommitStrategyALO<K, V> implements CommitStrategy<K, V> {
    private final Committer<K, V> committer;

    public CommitStrategyALO(Committer<K, V> committer) {
        this.committer = committer;
    }

    @Override
    public void close() {
        // Do nothing
    }

    @Override
    public void onAfterFetchBatch(List<BaseMessage<K, V>> messages) {
        // Do nothing
    }

    @Override
    public void onAfterProcessBatch() {
        // Commit offsets of all successfully processed messages. Commits asynchronous for
        // performance reasons. If for some reason the commit fails, that's okay because we're
        // allowed to see messages more than once after an application restart.
        committer.commitProcessedOffsets(false);
    }

    @Override
    public void onAfterProcessMessage(BaseMessage<K, V> message, Throwable error) {
        // If there was an error
        if (error != null) {
            // If retriable, then commit all up to the current message
            if (error instanceof RetriableException) {
                committer.commitProcessedOffsets(false);
            }
            // If skippable, then mark as processed and continue to the next message as usual
            if (error instanceof SkippableException) {
                committer.markAsProcessed(message);
            }
            // If non-retriable and non-skippable, then don't mark as processed!
        } else {
            // On successful processing and/or non-retriable error, mark as processed
            committer.markAsProcessed(message);
        }
    }
}
