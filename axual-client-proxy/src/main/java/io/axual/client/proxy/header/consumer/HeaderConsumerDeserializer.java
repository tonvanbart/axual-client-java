package io.axual.client.proxy.header.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.header.serde.HeaderDeserializer;
import io.axual.client.proxy.header.serde.HeaderDeserializerConfig;

public class HeaderConsumerDeserializer<T> extends HeaderDeserializer<T> {
    public static final String BACKING_KEY_DESERIALIZER_CONFIG = "headerconsumer.backing.key.deserializer";
    public static final String BACKING_VALUE_DESERIALIZER_CONFIG = "headerconsumer.backing.value.deserializer";

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        super.configure(modifyConfig(configs, isKey), isKey);
    }

    private static Map<String, Object> modifyConfig(Map<String, ?> configs, boolean isKey) {
        Map<String, Object> result = new HashMap<>(configs);
        // We get a backing deserializer class, class name or instance passed in. But the super class
        // expects a deserializer factory, so we wrap the backing deserializer and pass the wrapper
        //factory to the base class.
        String backingDeserializerConfigKey = isKey ? BACKING_KEY_DESERIALIZER_CONFIG : BACKING_VALUE_DESERIALIZER_CONFIG;
        Object backingDeserializer = configs.get(backingDeserializerConfigKey);
        result.put(HeaderDeserializerConfig.BACKING_DESERIALIZER_CONFIG, new HeaderConsumerDeserializerFactory<>(backingDeserializer));
        result.remove(backingDeserializerConfigKey);
        return result;
    }
}
