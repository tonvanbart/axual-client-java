package io.axual.streams.proxy.axual;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.streams.StreamsConfig;

import java.util.Map;

import io.axual.client.proxy.axual.generic.AxualProxyConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.common.annotation.InterfaceStability;
import io.axual.streams.proxy.generic.proxy.StreamsProxy;
import io.axual.streams.proxy.wrapped.WrappedStreamsFactory;

@InterfaceStability.Evolving
public class AxualStreamsConfig extends AxualProxyConfig<StreamsProxy> {
    public static final String BACKING_FACTORY_CONFIG = "axualstreams.backing.factory";
    public static final String CHAIN_CONFIG = "axualstreams.chain";

    public static final ProxyChain DEFAULT_PROXY_CHAIN = ProxyChain.newBuilder()
            .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
            .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
            .append(ProxyTypeRegistry.LINEAGE_PROXY_ID)
            .append(ProxyTypeRegistry.HEADER_PROXY_ID)
//            .append(ProxyTypeRegistry.LOGGING_PROXY_ID, config)
            .build();

    private final Object defaultKeySerde;
    private final Object defaultValueSerde;

    public AxualStreamsConfig(Map<String, Object> configs) {
        super(addDefaultFactory(configs, BACKING_FACTORY_CONFIG, WrappedStreamsFactory.class),
                "streams",
                BACKING_FACTORY_CONFIG,
                CHAIN_CONFIG);

        defaultKeySerde = configs.get(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG);
        defaultValueSerde = configs.get(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG);
    }

    public Object getDefaultKeySerde() {
        return defaultKeySerde;
    }

    public Object getDefaultValueSerde() {
        return defaultValueSerde;
    }
}
