package io.axual.discovery.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DiscoveryExceptionTest {
    protected static final String MESSAGE = "Error message for Unit Test";
    protected static final Exception CAUSE = new RuntimeException();

    @Test
    public void constructor_message() {
        DiscoveryException exception = new DiscoveryException(MESSAGE);
        assertEquals(MESSAGE, exception.getMessage());
    }

    @Test
    public void constructor_message_cause() {
        DiscoveryException exception = new DiscoveryException(MESSAGE, CAUSE);
        assertEquals(MESSAGE, exception.getMessage());
        assertEquals(CAUSE, exception.getCause());
    }

    @Test
    public void constructor_cause() {
        DiscoveryException exception = new DiscoveryException(CAUSE);
        assertEquals(CAUSE, exception.getCause());
    }

    @Test
    public void constructor_complete_suppressed_writablestack() {
        final boolean enableSuppression = true;
        final boolean writableStackTrace = true;
        DiscoveryException exception = new DiscoveryException(MESSAGE, CAUSE, enableSuppression, writableStackTrace);
        assertEquals(MESSAGE, exception.getMessage());
        assertEquals(CAUSE, exception.getCause());
    }
}
