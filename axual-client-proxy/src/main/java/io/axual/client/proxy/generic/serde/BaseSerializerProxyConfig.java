package io.axual.client.proxy.generic.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;

import io.axual.common.config.BaseConfig;

public class BaseSerializerProxyConfig<T> extends BaseConfig {
    private final String backingSerializerFactoryPropertyName;
    private final boolean isKey;

    public BaseSerializerProxyConfig(Map<String, Object> configs, boolean isKey, String backingSerializerFactoryPropertyName) {
        super(configs);
        this.backingSerializerFactoryPropertyName = backingSerializerFactoryPropertyName;
        filterDownstream(this.backingSerializerFactoryPropertyName);
        this.isKey = isKey;
    }

    @SuppressWarnings("unchecked")
    public SerializerProxy<T> getBackingSerializer() {
        if (backingSerializerFactoryPropertyName != null) {
            SerializerProxyFactory<T> factory = getConfiguredInstance(backingSerializerFactoryPropertyName, SerializerProxyFactory.class);
            return factory.create(downstreamConfigs, isKey);
        } else {
            return null;
        }
    }

    public boolean isKey() {
        return isKey;
    }
}
