package io.axual.client.proxy.switching.discovery;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

import io.axual.client.proxy.generic.client.ClientProxy;
import io.axual.client.proxy.generic.client.ClientProxyReplacer;
import io.axual.client.proxy.generic.config.BaseClientProxyConfig;
import io.axual.client.proxy.switching.generic.ClientProxySwitcher;
import io.axual.common.exception.ClientException;
import io.axual.discovery.client.DiscoveryClient;
import io.axual.discovery.client.DiscoveryClientRegistry;
import io.axual.discovery.client.DiscoveryConfig;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.discovery.client.exception.DiscoveryClientRegistrationException;

public class DiscoverySubscriber<T extends ClientProxy, C extends BaseClientProxyConfig> implements ClientProxyReplacer<T, C>, DiscoveryClient {
    private static final Logger LOG = LoggerFactory.getLogger(DiscoverySubscriber.class);

    private final DiscoveryConfig config;
    private final String clientType;
    private final ClientProxySwitcher<T, C> switcher;
    private final boolean allowNoTarget;

    private boolean needToSwitch = false;

    // The latest result we got back from the Discovery API
    private DiscoveryResult newDiscoveryResult;
    // The coordinates we're working currently
    private DiscoveryResult currentDiscoveryResult;

    public DiscoverySubscriber(DiscoveryConfig config, String clientType, ClientProxySwitcher<T, C> switcher, boolean allowNoTarget) {
        this.config = config;
        this.clientType = clientType;
        this.switcher = switcher;
        this.allowNoTarget = allowNoTarget;

        // Registration with the registry directly calls the callback below, filling currentDiscoveryResult
        try {
            DiscoveryClientRegistry.register(this.config, this);
        } catch (DiscoveryClientRegistrationException e) {
            throw new ClientException("Discovery Client Registration failed for " + clientType, e);
        }

        if (newDiscoveryResult == null) {
            throw new ClientException("Could not initialize DiscoveryResult for " + clientType + " in " + this.getClass().getName());
        }
    }

    public ClientProxySwitcher<T, C> getSwitcher() {
        return switcher;
    }

    public void checkDiscovery() {
        // if the registry finds a change, it will notify all registered DiscoveryClients, including this class
        // this means this call can lead to onDiscoveryPropertiesChange() being called.
        DiscoveryClientRegistry.checkProperties(config);
    }

    @Override
    public void close() {
        DiscoveryClientRegistry.unregister(this.config, this);
    }

    @Override
    public void onDiscoveryPropertiesChange(DiscoveryResult discoveryResult) {
        // If we need to switch, then initiate it by setting needToSwitch. If not, then update
        // the current discoveryResult with the new values (updating DISTRIBUTOR_TIMEOUT for instance)
        LOG.info("Received new DiscoveryResult for {}: {}", clientType, discoveryResult);

        if (discoveryResult == null && !allowNoTarget) {
            LOG.info("Empty target received, ignoring");
            return;
        }

        String currentCluster = currentDiscoveryResult != null ? currentDiscoveryResult.getCluster() : null;
        String newCluster = discoveryResult != null ? discoveryResult.getCluster() : null;

        if (!Objects.equals(currentCluster, newCluster)) {
            LOG.info("Switching {} from {} to {}", clientType, currentCluster, newCluster);
            newDiscoveryResult = discoveryResult;
            needToSwitch = true;
        } else {
            currentDiscoveryResult = discoveryResult;
        }
    }

    @Override
    public boolean needToReplace() {
        return needToSwitch;
    }

    @Override
    public T replace(T oldProxy, C config) {
        // Reset the flag indicating the need to switch
        needToSwitch = false;

        // Copy into another variable to use throughout the switch and lifetime of the real
        // KafkaProducer. This prevents race conditions with the onDiscoveryPropertiesChange callback,
        // should it get called again while we are still performing a switch.
        DiscoveryResult oldDiscoveryResult = currentDiscoveryResult;
        currentDiscoveryResult = newDiscoveryResult;
        newDiscoveryResult = null;

        if (currentDiscoveryResult == null && !allowNoTarget) {
            LOG.error("New DiscoveryResult for {} can not be null upon switching!", clientType);
        }

        return switcher.switchProxy(oldProxy, config, oldDiscoveryResult, currentDiscoveryResult);
    }

    public DiscoveryResult getCurrentDiscoveryResult() {
        return currentDiscoveryResult;
    }

    public String getCurrentDiscoveryProperty(String key) {
        if (currentDiscoveryResult != null) {
            Object value = currentDiscoveryResult.getConfigs().get(key);
            return value instanceof String ? (String) value : null;
        }
        return null;
    }
}
