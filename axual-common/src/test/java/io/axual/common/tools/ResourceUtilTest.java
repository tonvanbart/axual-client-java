package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.InputStream;

import static org.junit.Assert.assertNotNull;

public class ResourceUtilTest {
    @Test
    public void testExistingResource() throws Exception {
        String resourcePath = "ssl/axual.client.keystore.jks";
        try(InputStream inputStream = ResourceUtil.getResourcePath(resourcePath).openStream()) {
            assertNotNull(inputStream);
        }
    }

    @Test(expected = FileNotFoundException.class)
    public void testNonExistingResource() throws Exception {
        String resourcePath = "nonexisting.keystore.jks";
        try(InputStream path = ResourceUtil.getResourcePath(resourcePath).openStream()){

        }
    }

}
