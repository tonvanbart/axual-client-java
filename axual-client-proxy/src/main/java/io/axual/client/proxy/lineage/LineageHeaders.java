package io.axual.client.proxy.lineage;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

public class LineageHeaders {
    public static final String MESSAGE_ID_HEADER = "Axual-Message-Id";

    public static final String PRODUCER_ID_HEADER = "Axual-Producer-Id";
    public static final String PRODUCER_VERSION_HEADER = "Axual-Producer-Version";
    public static final String INTERMEDIATE_ID_HEADER="Axual-Intermediate-Id";
    public static final String INTERMEDIATE_VERSION_HEADER = "Axual-Intermediate-Version";

    public static final String SERIALIZATION_TIME_HEADER = "Axual-Serialization-Time";
    public static final String DESERIALIZATION_TIME_HEADER = "Axual-Deserialization-Time";
    public static final String COPY_FLAGS_HEADER = "Axual-Copy-Flags";

    public static final String SYSTEM_HEADER = "Axual-System";
    public static final String INSTANCE_HEADER = "Axual-Instance";
    public static final String CLUSTER_HEADER = "Axual-Cluster";
    public static final String TENANT_HEADER = "Axual-Tenant";
    public static final String ENVIRONMENT_HEADER = "Axual-Environment";

    private LineageHeaders() {
    }
}
