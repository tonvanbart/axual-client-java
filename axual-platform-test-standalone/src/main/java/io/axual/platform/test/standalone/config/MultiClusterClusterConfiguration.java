package io.axual.platform.test.standalone.config;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.ToString;

import static io.axual.platform.test.standalone.config.ClusterConstants.DEFAULT_ENABLE_DISTRIBUTION;

@Data
@ToString(callSuper = true)
@Component
@Validated
@ConfigurationProperties(value = "standalone")
@ConditionalOnProperty(name = "standalone.type", havingValue = "multi")
public class MultiClusterClusterConfiguration extends InstanceConfig {
    private Boolean enableDistribution = DEFAULT_ENABLE_DISTRIBUTION;

    @Valid
    @NestedConfigurationProperty
    @Size(min = 2, message = "At least two cluster settings are required")
    private List<ClusterSettings> clusters = new ArrayList<>();
}
