package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.serialization.Serializer;

import io.axual.common.exception.ClientException;
import io.axual.serde.avro.SpecificAvroSerializer;

/**
 * Use this ProducerConfig to produce specific, or typed, Avro objects to a stream.
 *
 * @param <K> The specific Avro Type which will be used as a key.
 * @param <V> The specific Avro Type which will be used as a value.
 */
public class SpecificAvroProducerConfig<K extends SpecificRecord, V extends SpecificRecord> extends BaseAvroProducerConfig<K, V> {
    private SpecificAvroProducerConfig(Builder<K, V, ?> builder) {
        super(builder);
    }

    /**
     * The Builder can be used to set the configuration options, except for the serializers.
     *
     * @param <K> The specific Avro Type which will be used as a key.
     * @param <V> The specific Avro Type which will be used as a value.
     * @return a new instance of builder
     */
    public static <K extends SpecificRecord, V extends SpecificRecord> Builder<K, V, ?> builder() {
        return new Builder<>();
    }

    /**
     * The Builder is used to set the configuration options.
     *
     * @param <K> The specific Avro Type which will be used as a key.
     * @param <V> The specific Avro Type which will be used as a value.
     * @param <T> the type of the Builder that is to be used.
     */
    public static class Builder<K extends SpecificRecord, V extends SpecificRecord, T extends Builder<K, V, T>> extends BaseAvroProducerConfig.Builder<K, V, T> {
        /**
         * Instantiates a new Builder.
         */
        Builder() {
            super.setKeySerializer(SpecificAvroSerializer.class.getName());
            super.setValueSerializer(SpecificAvroSerializer.class.getName());
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param keySerializerClassName The configured key serializer class name
         * @return the {@link SpecificAvroProducerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setKeySerializer(String keySerializerClassName) {
            throw new ClientException("Not allowed to override key serializer class");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param keySerializer The configured key serializer
         * @return the {@link SpecificAvroProducerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setKeySerializer(Serializer<K> keySerializer) {
            throw new ClientException("Not allowed to override key serializer class");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param valueSerializerClassName The configured value serializer
         * @return the {@link SpecificAvroProducerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setValueSerializer(String valueSerializerClassName) {
            throw new ClientException("Not allowed to override value serializer class");
        }

        /**
         * This is not supported and will always throw a {@link ClientException}
         *
         * @param valueSerializer The configured value serializer
         * @return the {@link SpecificAvroProducerConfig.Builder} object to be used for further
         * configuration
         */
        @Override
        public T setValueSerializer(Serializer<V> valueSerializer) {
            throw new ClientException("Not allowed to override value serializer class");
        }

        @Override
        public SpecificAvroProducerConfig<K, V> build() {
            validate();
            return new SpecificAvroProducerConfig<>(this);
        }
    }
}
