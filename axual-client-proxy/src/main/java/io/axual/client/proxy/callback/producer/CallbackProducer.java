package io.axual.client.proxy.callback.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.apache.kafka.common.serialization.Serializer;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;

import io.axual.client.proxy.callback.client.CallbackClientProxy;
import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.common.tools.MapUtil;

public class CallbackProducer<K, V> extends CallbackClientProxy<ProducerProxy<K, V>, CallbackProducerConfig<K, V>> implements ProducerProxy<K, V> {
    public CallbackProducer(Map<String, Object> configs) {
        super(new CallbackProducerConfig<>(configs));
    }

    public CallbackProducer(Map<String, Object> configs, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(SerdeUtil.addSerializersToConfigs(configs, keySerializer, valueSerializer));
    }

    public CallbackProducer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public CallbackProducer(Properties properties, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(MapUtil.objectToStringMap(properties), keySerializer, valueSerializer);
    }

    @Override
    public void initTransactions() {
        interceptor.execProc(this, "initTransactions", proxiedObject::initTransactions);
    }

    @Override
    public void beginTransaction() {
        interceptor.execProc(this, "beginTransaction", proxiedObject::beginTransaction);
    }

    @Override
    public void sendOffsetsToTransaction(final Map<TopicPartition, OffsetAndMetadata> offsets, final String consumerGroupId) {
        interceptor.execProc(this, "sendOffsetsToTransaction", () -> proxiedObject.sendOffsetsToTransaction(offsets, consumerGroupId), new String[]{"offsets", "consumerGroupId"}, offsets, consumerGroupId);
    }

    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> offsets,
        ConsumerGroupMetadata groupMetadata) throws ProducerFencedException {
        interceptor.execProc(this, "sendOffsetsToTransaction", () -> proxiedObject.sendOffsetsToTransaction(offsets, groupMetadata), new String[]{"offsets", "groupMetadata"}, offsets, groupMetadata);
    }

    @Override
    public void commitTransaction() {
        interceptor.execProc(this, "commitTransaction", proxiedObject::commitTransaction);
    }

    @Override
    public void abortTransaction() {
        interceptor.execProc(this, "abortTransaction", proxiedObject::abortTransaction);
    }

    @Override
    public Future<RecordMetadata> send(final ProducerRecord<K, V> producerRecord) {
        return interceptor.exec(this, "send", () -> proxiedObject.send(producerRecord), new String[]{"producerRecord"}, producerRecord);
    }

    @Override
    public Future<RecordMetadata> send(final ProducerRecord<K, V> producerRecord, final Callback callback) {
        return interceptor.exec(this, "send", () -> proxiedObject.send(producerRecord, callback), new String[]{"producerRecord", "callback"}, producerRecord, callback);
    }

    @Override
    public void flush() {
        interceptor.execProc(this, "flush", proxiedObject::flush);
    }

    @Override
    public List<PartitionInfo> partitionsFor(final String topic) {
        return interceptor.exec(this, "partitionsFor", () -> proxiedObject.partitionsFor(topic), new String[]{"topic"}, topic);
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return interceptor.exec(this, "metrics", proxiedObject::metrics);
    }
}
