package io.axual.streams;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.test.Random;

public class MessageAggregator {
    public static Map<Long, Integer> aggregate(Queue<ConsumerMessage<Random, Random>> queue) {
        final Map<Long, Integer> result = new HashMap<>();

        // Count how many times each message was consumed
        for (ConsumerMessage<Random, Random> message : queue) {
            int count = 0;
            long key = Long.valueOf(message.getKey().getRandom());
            if (result.containsKey(key)) {
                count = result.get(key);
            }
            result.put(key, count + 1);
        }

        return result;
    }
}
