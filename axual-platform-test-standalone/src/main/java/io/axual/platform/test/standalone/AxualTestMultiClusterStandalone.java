package io.axual.platform.test.standalone;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

import io.axual.platform.test.core.ClusterUnitConfig;
import io.axual.platform.test.core.InstanceUnitConfig;
import io.axual.platform.test.standalone.config.MultiClusterClusterConfiguration;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@ConditionalOnProperty(name = "standalone.type", havingValue = "multi")
public class AxualTestMultiClusterStandalone extends AxualStandalone {
    @Autowired
    public AxualTestMultiClusterStandalone(final MultiClusterClusterConfiguration config) {
        super(config);
        log.info("Using configuration {}", config);
        InstanceUnitConfig instanceUnitConfig = getInstanceUnitConfig(config);
        instanceUnitConfig.setEnableDistribution(config.getEnableDistribution());

        List<ClusterUnitConfig> clusterUnitConfigList = createClusterUnitConfigList(config, instanceUnitConfig, config.getClusters());

        createAndStartPlatformUnit(config, instanceUnitConfig, clusterUnitConfigList);
    }
}
