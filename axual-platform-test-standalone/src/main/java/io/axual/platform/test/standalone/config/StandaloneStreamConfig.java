package io.axual.platform.test.standalone.config;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-standalone
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import lombok.Data;
import lombok.ToString;

import static io.axual.platform.test.standalone.config.ClusterConstants.DEFAULT_ENVIRONMENT;

@Data
@ToString
@Validated
public class StandaloneStreamConfig {
    @NotBlank
    private String name;
    @NotBlank
    private String environment = DEFAULT_ENVIRONMENT;
    private String keyClass = null;
    private String valueClass = null;
    @Positive(message = "At least one partition is required")
    private int partitions = 1;

    private Boolean isRawTopic = Boolean.FALSE;
}
