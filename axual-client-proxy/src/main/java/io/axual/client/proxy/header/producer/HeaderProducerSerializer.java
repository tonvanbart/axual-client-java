package io.axual.client.proxy.header.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.header.serde.HeaderSerializer;
import io.axual.client.proxy.header.serde.HeaderSerializerConfig;

public class HeaderProducerSerializer<T> extends HeaderSerializer<T> {
    public static final String BACKING_KEY_SERIALIZER_CONFIG = "headerproducer.backing.key.serializer";
    public static final String BACKING_VALUE_SERIALIZER_CONFIG = "headerproducer.backing.value.serializer";

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        super.configure(modifyConfig(configs, isKey), isKey);
    }

    private static Map<String, Object> modifyConfig(Map<String, ?> configs, boolean isKey) {
        Map<String, Object> result = new HashMap<>(configs);
        // We get a backing serializer class, class name or instance passed in. But the super class
        // expects a serializer factory, so we wrap the backing serializer and pass the wrapper
        //factory to the base class.
        String backingSerializerConfigKey = isKey ? BACKING_KEY_SERIALIZER_CONFIG : BACKING_VALUE_SERIALIZER_CONFIG;
        Object backingSerializer = configs.get(backingSerializerConfigKey);
        result.put(HeaderSerializerConfig.BACKING_SERIALIZER_CONFIG, new HeaderProducerSerializerFactory<>(backingSerializer));
        result.remove(backingSerializerConfigKey);
        return result;
    }
}
