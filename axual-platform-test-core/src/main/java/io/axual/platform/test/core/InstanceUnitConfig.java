package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.ArrayList;
import java.util.List;

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.SslConfig;

@InterfaceStability.Evolving
public class InstanceUnitConfig {
    private String name;
    private String tenant;
    private String system;
    private int discoveryPort;
    private String discoveryBindAddress;
    private String discoveryAdvertisedAddress;
    private boolean enableDistribution = true;
    private SslConfig sslConfig;
    private List<String> clusters = new ArrayList<>();

    public String getName() {
        return name;
    }

    public InstanceUnitConfig setName(String name) {
        this.name = name;
        return this;
    }

    public String getTenant() {
        return tenant;
    }

    public InstanceUnitConfig setTenant(String tenant) {
        this.tenant = tenant;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public InstanceUnitConfig setSystem(String system) {
        this.system = system;
        return this;
    }

    public List<String> getClusters() {
        return clusters;
    }

    public InstanceUnitConfig addCluster(String cluster) {
        clusters.add(cluster);
        return this;
    }

    public String getDiscoveryBindAddress() {
        return discoveryBindAddress;
    }

    public InstanceUnitConfig setDiscoveryBindAddress(String discoveryBindAddress) {
        this.discoveryBindAddress = discoveryBindAddress;
        return this;
    }

    public String getDiscoveryAdvertisedAddress() {
        return discoveryAdvertisedAddress;
    }

    public InstanceUnitConfig setDiscoveryAdvertisedAddress(String discoveryAdvertisedAddress) {
        this.discoveryAdvertisedAddress = discoveryAdvertisedAddress;
        return this;
    }

    public int getDiscoveryPort() {
        return discoveryPort;
    }

    public InstanceUnitConfig setDiscoveryPort(int discoveryPort) {
        this.discoveryPort = discoveryPort;
        return this;
    }

    public boolean getEnableDistribution() {
        return enableDistribution;
    }

    public InstanceUnitConfig setEnableDistribution(boolean enableDistribution) {
        this.enableDistribution = enableDistribution;
        return this;
    }

    public SslConfig getSslConfig() {
        return sslConfig;
    }

    public InstanceUnitConfig setSslConfig(SslConfig sslConfig) {
        this.sslConfig = sslConfig;
        return this;
    }
}
