package io.axual.client.proxy.resolving.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.ExtendableListTopicsResult;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.TopicListing;
import org.apache.kafka.common.KafkaFuture;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.axual.common.resolver.TopicResolver;

public class ResolvingListTopicsResult extends ExtendableListTopicsResult {
    private final ListTopicsResult rawResult;
    private final TopicResolver resolver;

    public ResolvingListTopicsResult(ListTopicsResult rawResult, final TopicResolver resolver) {
        super(null);
        this.rawResult = rawResult;
        this.resolver = resolver;
    }

    @Override
    public KafkaFuture<Map<String, TopicListing>> namesToListings() {
        return rawResult.namesToListings().thenApply(new KafkaFuture.Function<Map<String, TopicListing>, Map<String, TopicListing>>() {
            @Override
            public Map<String, TopicListing> apply(Map<String, TopicListing> rawResult) {
                Map<String, TopicListing> result = new HashMap<>();
                for (Map.Entry<String, TopicListing> entry : rawResult.entrySet()) {
                    String unresolvedTopic;
                    if (entry.getValue().isInternal()) {
                        unresolvedTopic = entry.getKey();
                    } else {
                        unresolvedTopic = resolver.unresolveTopic(entry.getKey());
                    }
                    if (unresolvedTopic != null) {
                        result.put(unresolvedTopic, new TopicListing(unresolvedTopic, entry.getValue().isInternal()));
                    }
                }
                return result;
            }
        });
    }

    @Override
    public KafkaFuture<Collection<TopicListing>> listings() {
        return rawResult.listings().thenApply(new KafkaFuture.Function<Collection<TopicListing>, Collection<TopicListing>>() {
            @Override
            public Collection<TopicListing> apply(Collection<TopicListing> rawResult) {
                Collection<TopicListing> result = new HashSet<>();
                for (TopicListing rawListing : rawResult) {
                    if (rawListing.isInternal()) {
                        result.add(new TopicListing(rawListing.name(), rawListing.isInternal()));
                    } else {
                        String unresolved = resolver.unresolveTopic(rawListing.name());
                        if (unresolved != null) {
                            result.add(new TopicListing(unresolved, rawListing.isInternal()));
                        }
                    }
                }
                return result;
            }
        });
    }

    @Override
    public KafkaFuture<Set<String>> names() {
        return rawResult.names().thenApply(new KafkaFuture.Function<Set<String>, Set<String>>() {
            @Override
            public Set<String> apply(Set<String> rawResult) {
                return resolver.unresolveTopics(rawResult);
            }
        });
    }
}
