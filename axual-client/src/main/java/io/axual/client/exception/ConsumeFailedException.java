package io.axual.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.exception.ClientException;

/**
 * Error specifically used when there is an error trying to consume records
 */
public class ConsumeFailedException extends ClientException {

    /**
     * Creates a new ConsumeFailedException
     *
     * @param originalException the original error
     * @param info              info on which records failed to consume
     */
    public ConsumeFailedException(Throwable originalException, String info) {
        super(String.format("An error occured during message consumption: %s%nOriginal error: %s",
                info, originalException.getMessage()), originalException);
    }

    /**
     * Creates a new ConsumeFailedException
     *
     * @param originalException the original error
     */
    public ConsumeFailedException(Throwable originalException) {
        super(String.format("An error occured during message consumption: %s",
                originalException.getMessage()), originalException);
    }
}
