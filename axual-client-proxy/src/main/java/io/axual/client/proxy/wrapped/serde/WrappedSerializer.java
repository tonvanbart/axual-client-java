package io.axual.client.proxy.wrapped.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Serializer;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.generic.proxy.BaseProxy;
import io.axual.client.proxy.generic.serde.SerializerProxy;

public class WrappedSerializer<T> extends BaseProxy<WrappedSerializerConfig> implements SerializerProxy<T> {
    private Serializer<T> serializer;

    public WrappedSerializer() {
        super(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void configure(Map<String, ?> configs, boolean isKey) {
        config = new WrappedSerializerConfig(new HashMap<>(configs), isKey);
        serializer = config.getSerializer();
    }

    @Override
    public byte[] serialize(String topic, T data) {
        return serializer.serialize(topic, data);
    }

    @Override
    public byte[] serialize(String topic, Headers headers, T data) {
        return serializer.serialize(topic, headers, data);
    }

    @Override
    public void close(Duration timeout) {
        if (serializer != null) {
            serializer.close();
            serializer = null;
        }
        super.close(timeout);
    }
}
