package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class with timestamp helper methods.
 */
public class TimestampUtil {
    private TimestampUtil() {
    }

    /**
     * Format a given timestamp as a string containing the date.
     *
     * @param timestamp the timestamp
     * @return the string
     */
    public static String formatDate(long timestamp) {
        DateFormat withDate = new SimpleDateFormat("y-M-d HH:mm:ss.SSS");
        Date date = new Date(timestamp);
        return withDate.format(date);
    }

    /**
     * Format a given delta millis as a string.
     *
     * @param millis the millis
     * @return the string
     */
    public static String formatDelta(long millis) {
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;

        return String.format("%02d:%02d:%02d.%03d", hour, minute, second, millis % 1000);
    }

    /**
     * Format a given timestamp as a string (without a date).
     *
     * @param timestamp the timestamp
     * @return the string
     */
    public static String formatTime(long timestamp) {
        DateFormat withoutDate = new SimpleDateFormat("HH:mm:ss.SSS");
        Date date = new Date(timestamp);
        return withoutDate.format(date);
    }
}
